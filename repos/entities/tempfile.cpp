/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include "tempfile.h"


TempFile::TempFile( const QString &path, const QString &url )
{
    mFilePath = path;
    mUrl = url;
    QFileInfo fileInfo( path );
    mFileInfo = fileInfo;
    mFileName = fileInfo.fileName();
}

const QFileInfo &TempFile::getFileInfo() const
{
    return mFileInfo;
}

bool TempFile::getFileChecked() const
{
    return mFileChecked;
}

void TempFile::setFileChecked(bool fileChecked)
{
    mFileChecked = fileChecked;
}

const QString &TempFile::getFilePath() const
{
    return mFilePath;
}

void TempFile::setFilePath( const QString &filePath )
{
    mFilePath = filePath;
}


const QUrl &TempFile::getUrl() const
{
    return mUrl;
}

void TempFile::setUrl( const QUrl &url )
{
    mUrl = url;
}
