/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include "rocketchatchannelservice.h"

#include <CustomModels/models.h>



RocketChatChannelService::RocketChatChannelService( QObject *parent, RocketChatServerData *pServer, MessageService *pMessageService, FileService *pFileService ): QObject( parent ), mFileService( pFileService )
{
    this->mServer = pServer;
    this->mMessageService = pMessageService;
    mStorage = PersistanceLayer::instance();
    qRegisterMetaType<channelVector>( "channelVector" );
    qRegisterMetaType<userVector>( "userVector" );

}

QSharedPointer<RocketChatChannel> RocketChatChannelService::createChannelObject( const QString &pRoomId, const QString &pName, const QString &pType, bool insertIntoRepo )
{
    if ( mServer ) {
        auto ptr = QSharedPointer<RocketChatChannel>::create( mServer, mMessageService, pRoomId, pName, pType );

        if ( Q_LIKELY( mChannels ) ) {
            mChannels->add( ptr );
        } else {
            qCritical() << "Channelsrepo not available to ChannelService";
        }

        fillChannelWithMessages( ptr );

        return ptr;
    } else {
        return nullptr;
    }
}

QSharedPointer<RocketChatChannel> RocketChatChannelService::createChannelObject( const QString &pRoomId, const QString &pName, const QString &pType, const QString &pUsername, bool insertIntoRepo )
{
    if ( mServer ) {
        auto ptr = QSharedPointer<RocketChatChannel>::create( mServer, mMessageService, pRoomId, pName, pType );

        if ( pType == "d" ) {
            ptr->setUsername( pUsername );

            //TODO: place RocketChatUserObject inside Message instead...
            auto then = [ ptr ]( QSharedPointer<TempFile> tempfile, bool showInline ) {
                ptr->setAvatarImg( tempfile );
            };
            auto avatarUrl = "/avatar/" + pUsername + ".jpg";
            auto avatarRequest = QSharedPointer<FileRequest>::create( avatarUrl, "temp", then, true );
            mFileService->getFileRessource( avatarRequest );
        }

        //TODO: this should not be done here!
        if ( insertIntoRepo && Q_LIKELY( mChannels ) ) {
            mChannels->add( ptr );
        } else if ( !mChannels ) {
            qCritical() << "Channelsrepo not available to ChannelService";
        }

        fillChannelWithMessages( ptr );

        return ptr;
    } else {
        return nullptr;
    }
}


QList<QSharedPointer<RocketChatChannel> > RocketChatChannelService::processChannelData( const QJsonArray &pChannelArray, bool pJoined, bool pUpdateOnly )
{
    QList<QSharedPointer<RocketChatChannel> > vec;

    if ( mServer ) {
        std::tuple<QString, QString> openChannelTupel =  mStorage->getCurrentChannel();
        QString openChannel = std::get<0>( openChannelTupel );
        mStorage->transaction();

        for ( const auto &currentChannel : pChannelArray ) {
            QJsonObject currentChannelObject = currentChannel.toObject();

            if ( Q_LIKELY( ( currentChannelObject.contains( "rid" ) &&
                             currentChannelObject.contains( "name" ) ) || ( currentChannelObject.contains( "_id" ) &&
                                     currentChannelObject.contains( "t" ) ) ) ) {

                QString id = currentChannelObject["rid"].toString();
                QString name = currentChannelObject["name"].toString();
                QString type = currentChannelObject["t"].toString();
                QString username = "";
                QString chatPartnerId ;

                qint64 updatedAt = -1;

                //don't ask ...
                if ( currentChannelObject.contains( QStringLiteral( "_updatedAt" ) ) && !currentChannelObject.contains( QStringLiteral( "ls" ) ) ) {
                    auto updatedObj = currentChannelObject[QStringLiteral( "_updatedAt" )].toObject();
                    updatedAt = static_cast<qint64>( updatedObj[QStringLiteral( "$date" )].toDouble() );
                }

                if ( id == "" ) {
                    id = currentChannelObject["_id"].toString();
                }

                if ( type == "d" ) {
                    username = name;
                    name = currentChannelObject["fname"].toString();

                    QString ownUser = mServer->getUserId();
                    chatPartnerId = id;
                    chatPartnerId.replace( ownUser, "" );
                }

                //TODO: refactor me please!
                if ( pUpdateOnly && !mChannels->contains( id ) ) {
                    continue;
                }

                if ( currentChannelObject.contains( "open" ) && !currentChannelObject["open"].toBool() ) {
                    mChannels->remove( id );
                    mStorage->deleteChannel( id );
                    continue;
                }

                QSharedPointer<RocketChatChannel> channel = nullptr;

                if ( mChannels->contains( id ) ) {
                    channel = mChannels->get( id );
                } else {
                    channel = createChannelObject( id, name, type, username );
                }

                if ( !channel.isNull() ) {


                    if ( type == "d" ) {
                        channel->setChatPartnerId( chatPartnerId );
                    }

                    if ( !name.isEmpty() ) {
                        channel->setName( name );
                    }

                    if ( currentChannelObject.contains( "ro" ) ) {
                        channel->setReadOnly( currentChannelObject["ro"].toBool() );
                    }

                    if ( currentChannelObject.contains( "archived" ) ) {
                        channel->setArchived( currentChannelObject["archived"].toBool() );
                    }

                    if ( currentChannelObject.contains( "blocker" ) ) {
                        channel->setBlocked( currentChannelObject["blocker"].toBool() );

                        if ( currentChannelObject["blocker"].toBool() ) {
                            mMessageService->addChannelToBlockList( channel->getRoomId() );
                        }
                    }

                    if ( currentChannelObject.contains( "u" ) ) {
                        auto userObj = currentChannelObject["u"].toObject();

                        if ( userObj.contains( "username" ) && userObj.contains( "_id" ) ) {
                            channel->setOwnerName( userObj["username"].toString() );
                            channel->setOwnerId( userObj["_id"].toString() );
                        }
                    }

                    if ( currentChannelObject.contains( "muted" ) ) {
                        auto muted = currentChannelObject["muted"].toArray().toVariantList();
                        auto mutedUsers = QVariant( muted ).toStringList();
                        channel->setMuted( mutedUsers );

                        if ( currentChannelObject.contains( "username" ) && mutedUsers.contains( currentChannelObject["username"].toString() ) ) {
                            channel->setSelfMuted( true );
                        }
                    }

                    if ( id == openChannel ) {
                        channel->setUnreadMessages( 0 );
                    } else if ( currentChannelObject.contains( "unread" ) ) {
                        channel->setUnreadMessages( static_cast<unsigned int>( currentChannelObject["unread"].toInt() ) );
                    }

                    channel->setJoined( pJoined );
                    loadUsersOfChannel( channel );

                    subscribeChannel( channel );

                    channel->setUpdatedAt( updatedAt );

                    vec.append( channel );
                    persistChannel( channel );
                }

            }
        }

        mStorage->askForcommit();
    }

    return vec;
}


void RocketChatChannelService::loadJoinedChannelsFromServer( )
{
    DdpCallback subscriptionSuccess = [ = ]( QJsonObject pResponse, MeteorDDP * pDdpApi ) {
        Q_UNUSED( pDdpApi );

        if ( Q_LIKELY( pResponse.contains( "result" ) ) ) {
            QJsonArray channels = pResponse["result"].toArray();
            auto channelsParsed = processChannelData( channels, true, false );
            emit channelsLoaded( channelsParsed, true );
        }
    };
    DdpCallback roomsSuccess = [ = ]( QJsonObject pResponse, MeteorDDP * pDdpApi ) {
        Q_UNUSED( pDdpApi );

        if ( Q_LIKELY( pResponse.contains( "result" ) ) ) {
            QJsonArray channels = pResponse["result"].toArray();
            auto channelsParsed = processChannelData( channels, true, true );

            auto channelsLocal = mChannels->getElements();

            for ( auto it = channelsLocal.begin(); it != channelsLocal.end(); it++ ) {
                bool deleteFlag = true;

                for ( const auto &channelServer : channelsParsed ) {
                    if ( channelServer->getRoomId() == it.key() ) {
                        deleteFlag = false;
                    }
                }

                if ( deleteFlag ) {
                    deleteChannel( it.key() );
                }
            }

            emit channelsLoaded( channelsParsed, true );
        }
    };

    auto subscriptionsSuccess = [ = ]( QJsonObject pResponse, MeteorDDP * pDdpApi ) {
        subscriptionSuccess( pResponse, pDdpApi );
        QJsonArray empty;
        auto rooms = QSharedPointer<DDPMethodRequest>::create( "rooms/get", empty, roomsSuccess );
        mServer->sendDdprequest( rooms );
    } ;
    QJsonArray empty;
    auto subscriptions = QSharedPointer<DDPMethodRequest>::create( QStringLiteral( "subscriptions/get" ), empty, subscriptionsSuccess );
    mServer->sendDdprequest( subscriptions );
}



void RocketChatChannelService::loadJoinedChannelsFromDb()
{
    QList<QVariantHash> roomsList = mStorage->getChannels();

    if ( Q_LIKELY( !roomsList.isEmpty() ) ) {
        QList<QSharedPointer<RocketChatChannel>> channels;
        QList<QSharedPointer<RocketChatChannel>> channelsP;
        QList<QSharedPointer<RocketChatChannel>> channelsD;
        QList<QSharedPointer<RocketChatChannel>> channelsC;


        auto users = Models::getUsersModel();

        for ( const auto &roomHash : roomsList ) {
            auto channelPointer =  createChannelObject( roomHash["id"].toString(), roomHash["name"].toString(), roomHash["type"].toString(), roomHash["username"].toString(), false );

            if ( !channelPointer.isNull() ) {
                channelPointer->setMuted( roomHash["list"].toStringList() );
                channelPointer->setArchived( roomHash["archived"].toBool() );
                channelPointer->setReadOnly( roomHash["readOnly"].toBool() );
                channelPointer->setJoined( roomHash["joined"].toBool() );
                channelPointer->setChatPartnerId( roomHash["chatPartnerId"].toString() );
                channelPointer->setUpdatedAt( roomHash["updatedAt"].toLongLong() );

                if ( channelPointer->getType() == "d" ) {
                    auto user = users->getUserById( channelPointer->getChatPartnerId() );

                    if ( !user.isNull() ) {
                        channelPointer->setChatPartner( user );
                    } else {
                        auto newUser = QSharedPointer<RocketChatUser>::create( channelPointer->getChatPartnerId() );
                        newUser->setUserName( roomHash["username"].toString() );
                        channelPointer->setChatPartner( newUser );
                        auto result = QMetaObject::invokeMethod( users, "addUser", Q_ARG( User, newUser ) );
                        Q_ASSERT( result );
                    }
                }

                if ( roomHash.contains( "blocked" ) ) {
                    channelPointer->setBlocked( roomHash["blocked"].toBool() );
                }

                channels.append( channelPointer );

                if ( channelPointer->getType() == "p" ) {
                    channelsP.append( channelPointer );
                } else if ( channelPointer->getType() == "c" ) {
                    channelsC.append( channelPointer );
                } else if ( channelPointer->getType() == "d" ) {
                    channelsD.append( channelPointer );
                }
            }
        }

        mChannels->add( "p", channelsP );
        mChannels->add( "c", channelsC );
        mChannels->add( "d", channelsD );
        emit  channelsLoaded( channels, true );
    }
}

void RocketChatChannelService::persistChannel( const QSharedPointer<RocketChatChannel> &pChannel )
{
    auto id = pChannel->getRoomId();
    auto name  = pChannel->getName();
    auto type = pChannel->getType();
    auto muted = pChannel->getMuted().join( "," );
    auto username = pChannel->getUsername();
    auto joined = pChannel->getJoined();
    auto archived = pChannel->getArchived();
    auto readonly = pChannel->getReadOnly();
    auto blocked = pChannel->getBlocked();
    auto chatPartnerId = pChannel->getChatPartnerId();
    qint64 updatedAt = pChannel->getUpdatedAt();
    mStorage->addChannel( id, name, type, updatedAt, joined, readonly, muted, archived, blocked, username, chatPartnerId );
}

void RocketChatChannelService::persist()
{
    mStorage->transaction();

    for ( const auto &channel : mChannels->getElements() ) {
        persistChannel( channel );
    }

    mStorage->askForcommit();
}
/**
 * Fills the channel with users, which are loaded from the server
 *
 * @brief RocketChatChannelService::loadUsersOfChannel
 * @param channel
 */
void RocketChatChannelService::loadUsersOfChannel( const QSharedPointer<RocketChatChannel> &pChannel )
{
    if ( !pChannel.isNull() ) {
        QString channelId = pChannel->getRoomId();
        QJsonArray params = {channelId, true};
        auto request = QSharedPointer<DDPMethodRequest>::create( "getUsersOfRoom", params );
        DdpCallback success = [ = ]( QJsonObject data, MeteorDDP * ddp ) {
            Q_UNUSED( ddp );

            if ( Q_LIKELY( data.contains( "result" ) ) ) {
                QJsonObject result = data["result"].toObject();

                if ( Q_LIKELY( result.contains( "records" ) && result["records"].isArray() ) ) {
                    QJsonArray records = result["records"].toArray();
                    QVector<QSharedPointer<RocketChatUser>> userList;
                    auto userModel = Models::getUsersModel();

                    //crate new user objects and add them to the channel
                    for ( const auto current : records ) {
                        if ( current.type() == QJsonValue::Object ) {
                            QJsonObject userObject = current.toObject();
                            QString username;
                            QString userId;
                            QString status;
                            QString name;

                            if ( userObject.contains( "id" ) ) {
                                userId = userObject["id"].toString();
                            } else if ( userObject.contains( "_id" ) ) {
                                userId = userObject["_id"].toString();
                            }

                            if ( userObject.contains( "username" ) ) {
                                username = userObject["username"].toString();
                            }

                            if ( userObject.contains( "status" ) ) {
                                status = userObject["status"].toString();
                            }

                            if ( userObject.contains( "name" ) ) {
                                name = userObject["name"].toString();
                            }

                            auto user = QSharedPointer<RocketChatUser>( nullptr );


                            auto oldUser = userModel->getUserById( userId );

                            if ( !oldUser.isNull() ) {
                                oldUser->setName( name );
                                oldUser->setUserId( username );
                                oldUser->setStatus( status );
                                user = oldUser;

                            } else {
                                auto newUser = QSharedPointer<RocketChatUser>::create( userId );
                                newUser->setName( name );
                                newUser->setUserName( username );
                                newUser->setStatus( status );
                                user = newUser;
                            }

                            if ( userId == pChannel->getChatPartnerId() ) {
                                pChannel->setChatPartner( user );
                            }

                            userList.append( user );

                        } else if ( current.type() == QJsonValue::String ) {
                            //TODO: still relevant?
                            QString name = current.toString();
                            auto user = QSharedPointer<RocketChatUser>::create( name );
                            userList.append( user );
                        }
                    }

                    emit usersLoaded( channelId, userList );
                }
            }
        };
        request->setSuccess( success );
        mServer->sendDdprequest( request );
    }
}

void RocketChatChannelService::subscribeChannel( const QSharedPointer<RocketChatChannel> &pChannel )
{
    if ( !pChannel.isNull() ) {
        QString roomId = pChannel->getRoomId();
        subscribeChannel( roomId );
    }
}

void RocketChatChannelService::subscribeChannel( const QString &pChannel )
{
    auto subscription = QSharedPointer<RocketChatSubscribeChannelRequest>::create( pChannel );
    QJsonArray params = {pChannel + "/deleteMessage"};
    auto deletesub = QSharedPointer<DDPSubscriptionRequest>::create( "stream-notify-room", params );
    mServer->sendDdprequest( subscription );
    mServer->sendDdprequest( deletesub );
}



void RocketChatChannelService::fillChannelWithMessages( const QSharedPointer<RocketChatChannel> &pChannel )
{
    if ( !pChannel.isNull() ) {
        QString roomId = pChannel->getRoomId();
        auto messages = mStorage->getMessagesByRid( roomId );
        MessageList list;

        for ( const auto &currentMessage : messages ) {
            auto newMessage = mMessageService->parseMessage( currentMessage, false );

            if ( Q_LIKELY( !newMessage.isNull() ) ) {
                list.append( newMessage );
            }
        }

        pChannel->addMessages( list );
    }
}

MeteorDDP *RocketChatChannelService::getDdp() const
{
    return mDdp;
}

void RocketChatChannelService::setDdp( MeteorDDP *ddp )
{
    mDdp = ddp;
}

ChannelRepository *RocketChatChannelService::getChannels() const
{
    return mChannels;
}

void RocketChatChannelService::setChannels( ChannelRepository *pChannels )
{
    if ( pChannels ) {
        mChannels = pChannels;
    }
}

void RocketChatChannelService::leaveChannel( const QString &pId )
{
    //TODO: handle response, as soon as RC is fixed to do so
    if ( mChannels->contains( pId ) ) {
        auto request = QSharedPointer<RocketChatLeaveRoomRequest>::create( pId );
        auto ddp = getDdp();
        ddp->sendRequest( request );
        auto request2 = QSharedPointer<RocketChatHideRoomRequest>::create( pId );
        getDdp()->sendRequest( request2 );
        deleteChannel( pId );
    }
}

void RocketChatChannelService::deleteChannel( const QString &pId )
{
    if ( mChannels->contains( pId ) ) {
        mChannels->get( pId )->setDeleted( true );
        mChannels->remove( pId );
        mStorage->transaction();
        mStorage->deleteChannel( pId );
        mStorage->askForcommit();
    }
}

void RocketChatChannelService::hideRoom( const QString &pId )
{
    auto request = QSharedPointer<RocketChatHideRoomRequest>::create( pId );
    mDdp->sendRequest( request );
    deleteChannel( pId );
}

void RocketChatChannelService::reportContent( const QString &pMessageId, const QString &pAuthorId )
{
    QJsonArray params = {QStringLiteral( "abuse" )};
    auto request = QSharedPointer<DDPMethodRequest>::create( QStringLiteral( "createDirectMessage" ), params );
    DdpCallback success = [ = ]( QJsonObject pResponse, MeteorDDP * pDdp ) {
        if ( Q_LIKELY( pResponse.contains( QStringLiteral( "result" ) ) ) ) {
            QJsonObject result = pResponse[QStringLiteral( "result" )].toObject();

            if ( Q_LIKELY( result.contains( QStringLiteral( "rid" ) ) ) ) {
                QString rid = result[QStringLiteral( "rid" )].toString();
                auto subRoom = QSharedPointer<DDPSubscriptionRequest>::create( QStringLiteral( "room" ), QJsonArray( {"d" + QStringLiteral( "abuse" )} ) );
                auto subMessages = QSharedPointer<RocketChatSubscribeChannelRequest>::create( rid );
                auto channel = createChannelObject( rid, QStringLiteral( "abuse" ), "d" );

                if ( !channel.isNull() ) {

                    emit channelsLoaded( {channel}, true );
                    pDdp->sendRequest( subRoom );
                    pDdp->sendRequest( subMessages );
                    emit directChannelReady( rid, "d" );
                    QString message = QStringLiteral( "Report message with id:" ) + pMessageId + QStringLiteral( " from user with id:" ) + pAuthorId;
                    auto reportMessage = QSharedPointer<RocketChatMessage>::create( std::move( rid ), std::move( message ) );
                    mMessageService->sendMessage( reportMessage );
                }
            }
        }

        pDdp->unsetResponseBinding( request->getFrame() );

    };
    request->setSuccess( success );
    mServer->sendDdprequest( request );
}


void RocketChatChannelService::searchRoom( const QString &pTerm, const QString &pType )
{
    auto roomsModel = Models::getRoomSearchModel();
    RestRequestCallback success = [ = ]( QNetworkReply * reply, QJsonObject data, RestApi * ) {
        Q_UNUSED( reply )
        qDebug() << data;
        channelVector channelList;
        QHash<QString, bool> ids;
        QHash<QString, bool> names;

        if ( pType != "d" ) {
            if ( data.contains( "rooms" ) ) {
                QJsonArray rooms = data["rooms"].toArray();

                for ( auto room : rooms ) {
                    QJsonObject roomObj = room.toObject();

                    if ( roomObj.contains( "_id" ) && roomObj.contains( "name" ) && roomObj.contains( "t" ) ) {
                        QString name = roomObj["name"].toString();
                        QString id = roomObj["_id"].toString();
                        QString type = roomObj["t"].toString();
                        auto ptr = QSharedPointer<RocketChatChannel>::create( mServer, mMessageService, id, name, type );
                        channelList.append( ptr );
                        ids[id] = true;
                        names[name] = true;
                    }
                }
            }
        }

        if ( pType == "d" || pType.isEmpty() ) {
            if ( data.contains( "users" ) ) {
                QJsonArray rooms = data["users"].toArray();

                for ( auto room : rooms ) {
                    QJsonObject roomObj = room.toObject();

                    if ( roomObj.contains( "_id" ) && roomObj.contains( "name" ) && roomObj.contains( "username" ) ) {
                        QString name = roomObj["name"].toString();
                        QString id = roomObj["_id"].toString();
                        QString type = "d";
                        QString username = roomObj["username"].toString();
                        auto ptr = QSharedPointer<RocketChatChannel>::create( mServer, mMessageService, id, name, type );

                        ptr->setUsername( username );
                        channelList.append( ptr );
                        ids[id] = true;
                        names[name] = true;
                    }
                }
            }
        }

        channelList << searchRoomLocal( pTerm, ids, pType, names );

        if ( !channelList.isEmpty() ) {
            std::sort( channelList.begin(), channelList.end(), []( const QSharedPointer<RocketChatChannel> &v1, const QSharedPointer<RocketChatChannel> &v2 )->bool{
                return v1->getName().toLower() < v2->getName().toLower();
            } );
            auto result = QMetaObject::invokeMethod( roomsModel, "addChannels", Q_ARG( channelVector, channelList ) );
            Q_ASSERT( result );
        }
    };

    auto request = QSharedPointer<RestSpotlightRequest>::create( pTerm, "" );
    request->setSuccess( success );

    mServer->sendApiRequest( request );
}

channelVector RocketChatChannelService::searchRoomLocal( const QString &pTerm, const QHash<QString, bool>  &pFilter, const QString &pType, const QHash<QString, bool> &pNames )
{
    channelVector list;
    QString term = pTerm.toLower();

    for ( const auto &channel : mChannels->getElements() ) {
        QString id = channel->getRoomId();
        QString type = channel->getType();
        QString name = channel->getName().toLower();
        QString username = channel->getUsername().toLower();

        if ( !pFilter.contains( id ) && !pNames.contains( name ) && !pNames.contains( username ) && ( pType.isEmpty() || pType == type ) ) {

            if ( name.indexOf( term, Qt::CaseInsensitive ) != -1 || username.indexOf( term, Qt::CaseInsensitive ) != -1 ) {
                list.append( channel );
            }
        }
    }

    return list;
}

//TODO: fix
void RocketChatChannelService::openPrivateChannelWith( const QString &pUsername )
{
    QJsonArray params = {pUsername};
    auto request = QSharedPointer<DDPMethodRequest>::create( QStringLiteral( "createDirectMessage" ), params );
    DdpCallback success = [ = ]( QJsonObject pResponse, MeteorDDP * pDdp ) {
        if ( Q_LIKELY( pResponse.contains( QStringLiteral( "result" ) ) ) ) {
            QJsonObject result = pResponse[QStringLiteral( "result" )].toObject();

            if ( Q_LIKELY( result.contains( QStringLiteral( "rid" ) ) ) ) {
                QString rid = result[QStringLiteral( "rid" )].toString();

                QSharedPointer<RocketChatChannel> channel;
                auto subRoom = QSharedPointer<DDPSubscriptionRequest>::create( QStringLiteral( "room" ), QJsonArray( {"d" + pUsername} ) );
                auto subMessages = QSharedPointer<RocketChatSubscribeChannelRequest>::create( rid );

                //TODO: fix
                if ( !mChannels->contains( rid ) ) {
                    DdpCallback success = [ = ]( QJsonObject pResponse, MeteorDDP * pDdp ) {
                        Q_UNUSED( pDdp )

                        if ( pResponse.contains( "result" ) ) {
                            QString name = pResponse["fname"].toString();
                            QString username = pResponse["name"].toString();
                            QString id = pResponse["_id"].toString();
                            QSharedPointer<RocketChatChannel> channel = createChannelObject( id, name, "d", username );

                            mServer->loadRecentHistory( rid );

                            if ( !channel.isNull() ) {
                                emit channelsLoaded( {channel}, true );
                                mServer->sendDdprequest( subRoom );
                                mServer->sendDdprequest( subMessages );
                                emit directChannelReady( channel->getName(), "d" );
                            }
                        }
                    };
                    auto detailsRequest = QSharedPointer<RocketChatGetRoomByNameAndType>::create( pUsername, "d" );
                    detailsRequest->setSuccess( success );
                    mServer->sendDdprequest( detailsRequest );
                } else {
                    channel = mChannels->get( rid );

                    mServer->loadRecentHistory( rid );

                    if ( !channel.isNull() ) {
                        emit channelsLoaded( {channel}, true );
                        mServer->sendDdprequest( subRoom );
                        mServer->sendDdprequest( subMessages );
                        emit directChannelReady( channel->getName(), "d" );
                    }
                }
            }
        }

        pDdp->unsetResponseBinding( request->getFrame() );

    };
    request->setSuccess( success );
    mServer->sendDdprequest( request );
}
