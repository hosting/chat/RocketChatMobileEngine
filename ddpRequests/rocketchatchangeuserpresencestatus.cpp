/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#include "rocketchatchangeuserpresencestatus.h"
#include "repos/entities/rocketchatuser.h"

RocketChatChangeUserPresenceStatus::RocketChatChangeUserPresenceStatus( const RocketChatUser::status &pStatus )
{
    QJsonArray params;

    QString statusText;

    if ( pStatus == RocketChatUser::status::AWAY ) {
        statusText = QStringLiteral( "away" );
    } else if ( pStatus == RocketChatUser::status::ONLINE ) {
        statusText = QStringLiteral( "online" );
    } else if ( pStatus == RocketChatUser::status::OFFLINE ) {
        statusText = QStringLiteral( "offline" );
    } else if ( pStatus == RocketChatUser::status::BUSY ) {
        statusText = QStringLiteral( "busy" );
    }

    params.append( QJsonValue::Null );

    buildRequest( QStringLiteral( "UserPresence:" ) + statusText, params );
}
