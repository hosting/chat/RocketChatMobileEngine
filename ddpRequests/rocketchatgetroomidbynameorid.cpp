#include "rocketchatgetroomidbynameorid.h"

RocketChatGetRoomIdByNameOrId::RocketChatGetRoomIdByNameOrId( const QString &pName )
{
    QJsonArray params = {pName};
    buildRequest( QStringLiteral( "getRoomIdByNameOrId" ), params );
}

RocketChatGetRoomIdByNameOrId::RocketChatGetRoomIdByNameOrId( const QString &pName, const DdpCallback &success ): RocketChatGetRoomIdByNameOrId( pName )
{
    setSuccess( success );
}
