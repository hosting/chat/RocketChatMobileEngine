#include "rocketchatsubscriberoomschanged.h"

RocketChatSubscribeRoomsChanged::RocketChatSubscribeRoomsChanged( const QString &pUserId )
{
    QJsonArray params = {pUserId + QStringLiteral( "/rooms-changed" ), false};
    buildRequest( QStringLiteral( "stream-notify-user" ), params );
}
