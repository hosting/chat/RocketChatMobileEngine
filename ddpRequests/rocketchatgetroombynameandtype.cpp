#include "rocketchatgetroombynameandtype.h"


RocketChatGetRoomByNameAndType::RocketChatGetRoomByNameAndType(const QString &pName, const QString &pType):RocketChatGetRoomByNameAndType(pName, pType, nullptr)
{

}

RocketChatGetRoomByNameAndType::RocketChatGetRoomByNameAndType(const QString &pName, const QString &pType, const DdpCallback &pSuccess)
{
    QJsonArray params = { pType, pName };
    buildRequest( QStringLiteral( "getRoomByTypeAndName" ), params );
    setSuccess( pSuccess );
}
