/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include "emojiservice.h"
#include <QSharedPointer>
#include <QJsonArray>
#include <QJsonObject>
#include "rocketchatserver.h"
#include "utils.h"



EmojiService::EmojiService( RocketChatServerData *server,  FileService *pFileService )
    : server( server ), storage( PersistanceLayer::instance() ), mFileService( pFileService )
{
    mParsedCustomEmojisTemp.reserve( 50 );
}
/**
 * Loads the custom emojis from the server
 *
 *
 * @brief EmojiService::loadCustomEmojis
 */
void EmojiService::loadCustomEmojis( std::function<void ( QList<QSharedPointer<Emoji>> )> pSuccess )
{
    QJsonArray params;
    DdpCallback success = [ = ]( const QJsonObject & pResponse, MeteorDDP * pDdp ) {
        Q_UNUSED( pDdp );
        QJsonArray result = pResponse[QStringLiteral( "result" )].toArray();
        auto data = new EmojiData( result );
        data->success = std::move( pSuccess );
        handleCustomEmojisReceived( data );
    };
    auto request = QSharedPointer<DDPMethodRequest>::create( QStringLiteral( "listEmojiCustom" ), params, success );
    if(server){
        server->sendDdprequest( request );
    }
}
/**
 * Loads the Emoji Data from the databse and processes it to emoji Objects
 *
 * @brief EmojiService::loadEmojisFromDb
 * @return
 */
QList<QSharedPointer<Emoji>> EmojiService::loadEmojisFromDb()
{
    auto emojiData = storage->getCustomEmojis();
    QList<QSharedPointer<Emoji>> emojiList;
    emojiList.reserve( 3000 );

    for ( auto &current : emojiData ) {
        auto parsedEmojis = parseEmoji( current );
        emojiList.append( std::move( parsedEmojis ) );
    }

    return emojiList;
}

void EmojiService::persistEmoji( const QSharedPointer<Emoji> &pEmoji )
{
    storage->addCustomEmoji( pEmoji->getIdentifier(), pEmoji->getFilePath(), pEmoji->getHtml(), QStringLiteral( "custom" ), QStringLiteral( "dontcare" ) );
}


void EmojiService::handleCustomEmojisReceived( EmojiData *data )
{
    if ( data ) {
        QJsonArray jsonArray = data->getEmojiData();

        if ( jsonArray.isEmpty() ) {
            data->success( mParsedCustomEmojisTemp );
        } else {
            for ( const auto currentEmoji : jsonArray ) {
                if ( currentEmoji.isObject() ) {
                    auto parsedEmoji = parseEmoji( currentEmoji.toObject() );

                    if ( !parsedEmoji.isNull() ) {
                        mParsedCustomEmojisTemp.append( parsedEmoji );
                        qDebug() << "emoji parsed: " << parsedEmoji->getIdentifier();
                        QString url = QStringLiteral( "/emoji-custom/" ) + parsedEmoji->getName() + '.' + parsedEmoji->getExtension();
                        std::function<void ( QSharedPointer<TempFile>, bool )> then = [ = ]( const QSharedPointer<TempFile> &file, bool ) {
                            if ( Q_UNLIKELY( file.isNull() ) ) {
                                qWarning() << "file is null" << parsedEmoji->getIdentifier();
                                return;
                            }

                            QString path = Utils::getPathPrefix() + file->getFilePath();
                            parsedEmoji->setCategory( QStringLiteral( "custom" ) );
                            parsedEmoji->setFilePath( path );
                            parsedEmoji->setHtml( QStringLiteral( "<img height='20' width='20' src='" ) + path + QStringLiteral( "' />" ) );
                            data->emojisProcessed++;

                            if ( data->emojisProcessed == data->emojiCounter ) {
                                data->success( mParsedCustomEmojisTemp );
                            }
                        };
                        auto fileRequest = QSharedPointer<FileRequest>::create( url, QStringLiteral( "emoji" ), then, true );
                        mFileService->getFileRessource( fileRequest );
                    }
                }
            }
        }
    }

}
/**
 * Creates an emoij object from a QHash
 *
 * Meant to be used with data from the database
 *
 * Needed keys:
 *  [name,extension,html,category, file]
 *
 * @brief EmojiService::parseEmoji
 * @param pEmojiData
 * @return
 */
QSharedPointer<Emoji> EmojiService::parseEmoji( const QHash<QString, QString> &pEmojiData )
{
    QString name = pEmojiData[QStringLiteral( "id" )];
    QString html = pEmojiData[QStringLiteral( "html" )];
    QString catgeory = pEmojiData[QStringLiteral( "category" )];
    QString filePath = pEmojiData[QStringLiteral( "file" )];
    QString unicode = pEmojiData[QStringLiteral( "unicode" )];
    int order = pEmojiData[QStringLiteral( "sort_order" )].toInt();

    return QSharedPointer<Emoji>::create( std::move( name ), std::move( catgeory ), std::move( filePath ), std::move( html ), std::move( unicode ), order );
}

QSharedPointer<Emoji> EmojiService::parseEmoji( const QJsonObject &pEmojiData )
{
    QString name = pEmojiData[QStringLiteral( "name" )].toString();
    QString extension = pEmojiData[QStringLiteral( "extension" )].toString();
    QString category = pEmojiData[QStringLiteral( "category" )].toString();

    auto emoji = QSharedPointer<Emoji>::create( std::move( name ),  std::move( extension ),  std::move( category ) );
    return emoji;
}

/**
 * Constructor of Json Array
 * @brief EmojiData::EmojiData
 * @param data
 */
EmojiData::EmojiData( const QJsonArray &data )
{
    this->emojiData = data;
    this->emojiCounter = data.count();
}

const QJsonArray &EmojiData::getEmojiData() const
{
    return emojiData;
}
