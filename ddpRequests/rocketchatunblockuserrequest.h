#ifndef ROCKETCHATUNBLOCKUSERREQUEST_H
#define ROCKETCHATUNBLOCKUSERREQUEST_H

#include "ddpmethodrequest.h"

class RocketChatUnblockUserRequest : public DDPMethodRequest
{
    public:
        RocketChatUnblockUserRequest( const QString &pChannelId, const QString &pUserId );
};

#endif // ROCKETCHATUNBLOCKUSERREQUEST_H
