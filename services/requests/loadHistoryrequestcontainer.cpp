/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include "loadHistoryrequestcontainer.h"

LoadHistoryRequestContainer::LoadHistoryRequestContainer( const QList<QSharedPointer<LoadHistoryServiceRequest>> &pRequests,
        const std::function<void ( QMultiMap<QString, QSharedPointer<RocketChatMessage>>* )> &pSuccess ): mRequests( pRequests ), mSuccess( pSuccess )
{
}

QList<QSharedPointer<LoadHistoryServiceRequest> > LoadHistoryRequestContainer::getRequests() const
{
    return mRequests;
}

std::function<void ( QMultiMap<QString, QSharedPointer<RocketChatMessage> > * )> LoadHistoryRequestContainer::getSuccess() const
{
    return mSuccess;
}

void LoadHistoryRequestContainer::setSuccess( const std::function<void ( QMultiMap<QString, QSharedPointer<RocketChatMessage> > * )> &pValue )
{
    mSuccess = pValue;
}

