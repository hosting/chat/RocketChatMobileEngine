/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#include "messagerepository.h"

MessageRepository::MessageRepository( QObject *parent ): QObject( parent )
{
    mElements.reserve( 100 );
}

ChatMessage MessageRepository::youngest()
{

    ChatMessage obj;

    if ( Q_LIKELY( mTimestampIndex.size() ) ) {
        obj = mTimestampIndex.last();
    }

    return obj;
}

const ChatMessage &MessageRepository::youngest() const
{
    if ( !mTimestampIndex.isEmpty() ) {
        return mTimestampIndex.last();
    }

    const auto obj = new ChatMessage();
    return *obj;

}

ChatMessage MessageRepository::oldest()
{
    ChatMessage obj;

    if ( Q_LIKELY( mTimestampIndex.size() ) ) {
        obj = mTimestampIndex.first();
    }

    return obj;
}

const ChatMessage &MessageRepository::oldest() const
{
    if ( !mTimestampIndex.isEmpty() ) {
        return mTimestampIndex.first();
    }

    const auto obj = new ChatMessage();
    return *obj;

}

void MessageRepository::remove(QString id)
{
    auto timestamp = mElements[id]->getTimestamp();
    AbstractBaseRepository::remove(id);
    mTimestampIndex.remove(timestamp);
}


QVector<QSharedPointer<RocketChatMessage> > MessageRepository::messagesAsObjects()
{
    QVector<QSharedPointer<RocketChatMessage>> list;

    for ( const auto &message : mTimestampIndex ) {
        if ( !message.isNull() ) {
            auto sortable = message;
            list.append( sortable );
        }
    }

    return list;
}

const QMap<qint64, QSharedPointer<RocketChatMessage> > &MessageRepository::timestampIndex() const
{
    return mTimestampIndex;
}

void MessageRepository::setTimestampIndex( const QMap<qint64, QSharedPointer<RocketChatMessage> > &timestampIndex )
{
    mTimestampIndex = timestampIndex;
}

bool MessageRepository::add( const QString &pId, const QSharedPointer<RocketChatMessage> &pMsg )
{
    if ( !pMsg.isNull() ) {
        if ( !mElements.contains( pId ) ) {
            mTimestampIndex[pMsg->getTimestamp()] = pMsg;
            return AbstractBaseRepository::add( pId, pMsg );
        }
    }

    return false;

}
