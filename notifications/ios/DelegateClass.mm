#include "iosdevicetokenstorage.h"
#include "iosnotificationreceiver.h"
#import <Foundation/Foundation.h>
#import "DelegateClass.h"


@implementation DelegateObject

-(id) initWithCPPInstance{
    if(self){
        m_instance = IosDeviceTokenStorage::getInstance();
        mNotificationsInstance = IosNotificationReceiver::getInstance();
    }
    return self;
}

-(void) setToken:(NSString*) token{
    m_instance->setToken([token UTF8String]);
}

- (void) sendMessage:(NSString *)pServer : (NSString *)pRid : (NSString *)pName : (NSString *)pType{
    if(pServer!=NULL && pRid != NULL && pName !=NULL && pType!=NULL){
        QString server([pServer UTF8String]);
        QString rid([pRid UTF8String]);
        QString name([pName UTF8String]);
        QString type([pType UTF8String]);
        mNotificationsInstance->sendMessageReceived(server,rid,name,type);
    }
}

- (void) openChannelByName:(NSString *)channelName{
    if(channelName != NULL){
        QString channelNameString([channelName UTF8String]);
        mNotificationsInstance->sendSwitchChannelByName(channelNameString);
        
    }
}


@end
