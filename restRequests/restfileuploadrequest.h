/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#ifndef RESTFILEUPLOADREQUEST_H
#define RESTFILEUPLOADREQUEST_H
#include <QVector>
#include <QDebug>

#include "restRequests/postrequest.h"
class RestFileUploadRequest : public PostRequest
{
    public:
        RestFileUploadRequest( const QString &pPath, const QString &pToken, const QByteArray &pData );
        double getProgress() const;
        void setProgress( double pValue );

    protected:
        QString mToken;
        double mProgress = 0.0;
        bool mOneChunk = true;

};

#endif // RESTFILEUPLOADREQUEST_H
