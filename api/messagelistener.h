#ifndef MESSAGELISTENER_H
#define MESSAGELISTENER_H

#include <QJsonObject>
#include <QMutexLocker>
#include <QObject>


class MessageListener: public QObject
{
        Q_OBJECT
    public:
        MessageListener( QObject *parent = nullptr ): QObject( parent ) {}
        virtual bool handlesMessage( const QJsonObject &object ) = 0;
};

#endif // MESSAGELISTENER_H
