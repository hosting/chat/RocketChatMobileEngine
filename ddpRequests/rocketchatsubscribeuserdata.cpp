#include "rocketchatsubscribeuserdata.h"

#include <QJsonArray>

RocketChatSubscribeUserData::RocketChatSubscribeUserData()
{
    QJsonArray params = {QJsonValue::Null};
    buildRequest( QStringLiteral( "userData" ), params );
}
