/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#ifndef ROCKETCHATMESSAGE_H
#define ROCKETCHATMESSAGE_H

#include <QJsonObject>
#include <QList>
#include <QSharedPointer>
#include "repos/entities/rocketchatattachment.h"
#include "repos/emojirepo.h"
#include "utils.h"
#include "rocketchatattachment.h"
#include "rocketchatuser.h"

class EmojiRepo;

class RocketChatMessage: public QObject
{
        Q_OBJECT
    public:
        enum class Type {
            textmessage,
            replymessage,
            none
        };
        RocketChatMessage( const QJsonObject &data, unsigned long timestamp, RocketChatMessage::Type messageType = Type::textmessage );
        explicit RocketChatMessage( const QJsonObject &data );
        RocketChatMessage( const QString &roomId, const QString &message );
        bool operator > ( const RocketChatMessage &compMessage ) const
        {
            return ( timestamp > compMessage.getTimestamp() );
        }
        bool operator < ( const RocketChatMessage &compMessage ) const
        {
            return ( timestamp < compMessage.getTimestamp() );
        }

        qint64 getTimestamp() const;
        bool isEmpty() const;
        const QJsonObject &getData() const;

        const QString &getRoomId() const;
        void setRoomId( const QString &value );

        const QString &getId() const;

        const QString &getAuthor() const;
        void setAuthor( const QString &value );

        const QString &getFormattedDate() const;
        void setFormattedDate( const QString &value );

        const QString &getFormattedTime() const;
        void setFormattedTime( const QString &value );

        const QString &getMessageString() const;
        void setMessageString( const QString &value );

        const QString &getMessageType();
        void setMessageType( const QString &value );
        bool getOwnMessage() const;
        void setOwnMessage( bool value );

        const QList<QSharedPointer<RocketChatAttachment> > &getAttachments() const;
        void setAttachments( const QList<QSharedPointer<RocketChatAttachment> > &value );

        qint64 getEmojiHash() const;
        void setEmojiHash( const qint64 &pValue );

        const QString &getAuthorId() const;
        void setAuthorId( const QString &value );

        const QString &getServer() const;
        void setServer( const QString &value );

        const QString &getRoomeTyp() const;
        void setRoomeTyp( const QString &value );
        bool inTimeIntervall( const qint64 newt, const qint64 oldt );
        QSharedPointer<TempFile> getAvatarImg() const;
        void setAvatarImg( const QSharedPointer<TempFile> &avatarImg );

    protected:
        bool mIntialized = false;
        QSharedPointer<TempFile> mAvatarImg;
        QSharedPointer<RocketChatUser> mAuthor;
        bool empty = false;
        qint64 timestamp = 0;
        QJsonObject data;
        QString roomId;
        QString server;
        Type type;
        bool ownMessage = false;
        QString originalMessage;
        QString parsedMessage;
        QString formattedTime;
        QString formattedDate;
        QString messageString;
        QString id;
        QString author;
        QString authorId;
        QString messageType;
        QString roomeTyp;
        QList<QSharedPointer<RocketChatAttachment>> attachments;
        qint64 mEmojiHash = 0;
    signals:
        void dataChanged( const QString &id, const QString &property );
};

#endif // ROCKETCHATMESSAGE_H
