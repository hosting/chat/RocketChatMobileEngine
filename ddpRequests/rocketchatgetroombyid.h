#ifndef ROCKETCHATGETROOMBYID_H
#define ROCKETCHATGETROOMBYID_H

#include "ddpmethodrequest.h"

class RocketChatGetRoomById: public DDPMethodRequest
{
    public:
        explicit RocketChatGetRoomById( const QString &pId );
        RocketChatGetRoomById( const QString &pId, const DdpCallback &pSuccess );

};

#endif // ROCKETCHATGETROOMBYID_H
