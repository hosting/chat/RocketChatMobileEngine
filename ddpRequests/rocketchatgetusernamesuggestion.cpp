#include "rocketchatgetusernamesuggestion.h"

RocketChatGetUsernameSuggestion::RocketChatGetUsernameSuggestion( const DdpCallback &pSuccess )
{
    setSuccess( pSuccess );
    QJsonArray params;
    buildRequest( QStringLiteral( "getUsernameSuggestion" ), params );
}
