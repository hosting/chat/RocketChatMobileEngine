/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#include "channelmodel.h"

int ChannelModel::rowCount( const QModelIndex &parent ) const
{
    Q_UNUSED( parent )
    int count = channelList.count();
    return count;
}

QVariant ChannelModel::data( const QModelIndex &index, int role ) const
{
    int row = index.row();

    if ( channelList.isEmpty() || row > channelList.count() ) {
        return QVariant();
    }

    auto currentChannel = channelList.at( row );

    if ( !currentChannel.isNull() ) {
        auto roleType = static_cast<ChannelRoles>( role );

        switch ( roleType ) {
            case ChannelRoles::name:
                return currentChannel->getName();

            case ChannelRoles::roomId:
                return currentChannel->getRoomId();

            case ChannelRoles::lastMessageText: {
                    auto channel = channelList.at( row );
                    auto message = channel->getYoungestMessage();

                    if ( message.isNull() ) {
                        return "";
                    }

                    if ( message->getMessageType() == QStringLiteral( "file" ) || message->getMessageType() == QStringLiteral( "image" ) || message->getMessageType() == QStringLiteral( "audio" ) ) {
                        return tr( QByteArrayLiteral( "file upload" ) );
                    }

                    auto messageText = message->getMessageString();
                    messageText = messageText.replace( "<img height='20' width='20'", "<img height='10' width='10'" );
                    return messageText;
                }

            case ChannelRoles::channelType:
                return currentChannel->getType();

            case ChannelRoles::unreadMessages:
                return currentChannel->getUnreadMessages();

            case ChannelRoles::readonly:
                return currentChannel->getReadOnly() || currentChannel->getArchived() || currentChannel->getBlocked();

            case ChannelRoles::ownerId:
                return currentChannel->getOwnerId();

            case ChannelRoles::archived:
                return currentChannel->getArchived();

            case ChannelRoles::blocked:
                return currentChannel->getBlocked();

            case ChannelRoles::username:
                return currentChannel->getUsername();

            case ChannelRoles::userStatus: {
                    int status = 0;
                    auto partner = currentChannel->getChatPartner();

                    if ( !partner.isNull() ) {
                        switch ( partner->getStatus() ) {
                            case RocketChatUser::status::ONLINE :
                                status = 0;
                                break;

                            case RocketChatUser::status::AWAY :
                                status = 1;
                                break;

                            case RocketChatUser::status::OFFLINE:
                                status = 2;
                                break;

                            case RocketChatUser::status::BUSY:
                                status = 3;
                                break;
                        }

                    }

                    return status;
                }

            case ChannelRoles::deleted:
                return currentChannel->getDeleted();

            case ChannelRoles::ownerName:
                return currentChannel->getOwnerName();

            case ChannelRoles::avatarImg: {
                    auto avatarImg = currentChannel->getAvatarImg();

                    if ( !avatarImg.isNull() ) {
                        auto path = Utils::getPathPrefix() + avatarImg->getFilePath();

                        if ( path.endsWith( "svg" ) ) {
                            return "qrc:res/user-identity.svg";
                        } else {
                            return path;
                        }
                    } else {
                        return "";
                    }
                }

        }
    }

    return QVariant();

}

bool ChannelModel::addChannel( const QSharedPointer<RocketChatChannel> &channel )
{
    if ( !channel.isNull() && channel->getRoomId() != "" && !duplicateCheck.contains( channel->getRoomId() ) ) {
        connect( channel.data(), &RocketChatChannel::messageAdded, this, &ChannelModel::onNewerMessage, Qt::UniqueConnection );
        connect( channel.data(), &RocketChatChannel::unreadMessagesChanged, this, &ChannelModel::onUnreadMessageChanged, Qt::UniqueConnection );
        connect( channel.data(), &RocketChatChannel::dataChanged, this, &ChannelModel::onDataChanged, Qt::UniqueConnection );
        connect( channel.data(), &RocketChatChannel::channelDeleted, this, &ChannelModel::onDeleted, Qt::UniqueConnection );
        connect( channel.data(), &RocketChatChannel::updatedChanged, this, &ChannelModel::onChannelOrderChanged );

        duplicateCheck.insert( channel->getRoomId() );
        int pos = channelList.findPosition( channel );

        if ( pos < 0 ) {
            pos = 0;
        }

        beginInsertRows( QModelIndex(), pos, pos );
        channelList.insertSort( channel );
        endInsertRows();
        sortChanged();
        return true;
    }

    return false;

}

bool ChannelModel::addChannelsSlot( const QList<QSharedPointer<RocketChatChannel> > &pChannels )
{
    //  beginResetModel();
    int from = -1;
    int to = -1;

    beginResetModel();

    for ( const auto &channel : pChannels ) {
        if ( !channel.isNull() && !channel->getRoomId().isEmpty() && !duplicateCheck.contains( channel->getRoomId() ) ) {
            //connect( channel.data(), &RocketChatChannel::messageAdded, this, &ChannelModel::onNewerMessage, Qt::UniqueConnection );
            connect( channel.data(), &RocketChatChannel::unreadMessagesChanged, this, &ChannelModel::onUnreadMessageChanged, Qt::UniqueConnection );
            connect( channel.data(), &RocketChatChannel::dataChanged, this, &ChannelModel::onDataChanged, Qt::UniqueConnection );
            connect( channel.data(), &RocketChatChannel::channelDeleted, this, &ChannelModel::onDeleted, Qt::UniqueConnection );
            connect( channel.data(), &RocketChatChannel::updatedChanged, this, &ChannelModel::onChannelOrderChanged );
            duplicateCheck.insert( channel->getRoomId() );
            auto index = channelList.insertSort( channel );

            if ( from == -1 ) {
                from = index;
                to = index;
            }

            if ( index < from ) {
                from = index;
            } else if ( index > to ) {
                to = index;
            }
        }
    }

    //beginInsertRows( QModelIndex(), from, to );
    //endInsertRows();
    endResetModel();
    return true;
}

QHash<int, QByteArray> ChannelModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[static_cast<int>( ChannelRoles::name )] = QByteArrayLiteral( "name" );
    roles[static_cast<int>( ChannelRoles::roomId )] =  QByteArrayLiteral( "roomId" );
    roles[static_cast<int>( ChannelRoles::unreadMessages )] = QByteArrayLiteral( "unreadMessages" );
    roles[static_cast<int>( ChannelRoles::channelType )] = QByteArrayLiteral( "type" );
    roles[static_cast<int>( ChannelRoles::lastMessageText )] = QByteArrayLiteral( "lastMessageText" );
    roles[static_cast<int>( ChannelRoles::readonly )] = QByteArrayLiteral( "readonly" );
    roles[static_cast<int>( ChannelRoles::archived )] = QByteArrayLiteral( "archived" );
    roles[static_cast<int>( ChannelRoles::blocked )] = QByteArrayLiteral( "blocked" );
    roles[static_cast<int>( ChannelRoles::deleted )] = QByteArrayLiteral( "deleted" );
    roles[static_cast<int>( ChannelRoles::ownerId )] = QByteArrayLiteral( "ownerId" );
    roles[static_cast<int>( ChannelRoles::ownerName )] = QByteArrayLiteral( "ownerName" );
    roles[static_cast<int>( ChannelRoles::username )] = QByteArrayLiteral( "username" );
    roles[static_cast<int>( ChannelRoles::userStatus )] = QByteArrayLiteral( "userStatus" );
    roles[static_cast<int>( ChannelRoles::avatarImg )] = QByteArrayLiteral( "avatarImg" );

    return roles;
}

void ChannelModel::onNewerMessage( const QString &id, qint64 timestamp )
{
    Q_UNUSED( timestamp )
    int pos = 0;
    bool found = false;

    for ( const auto &current : channelList ) {
        if ( current->getRoomId() == id ) {
            found = true;
            break;
        }

        pos++;
    }

    if ( found ) {
        //emit dataChanged(index(pos),index(pos),{lastMessageText});
    }

    // sortChanged();
}

void ChannelModel::onUnreadMessageChanged( const QString &id, int number )
{
    Q_UNUSED( number )
    qDebug() << QStringLiteral( "unread signal catched" );
    int pos = 0;
    bool found = false;

    for ( const auto &current : channelList ) {
        if ( current->getRoomId() == id ) {
            found = true;
            break;
        }

        pos++;
    }

    if ( found ) {
        qDebug() << "channel found";
        emit dataChanged( index( pos ), index( pos ), {static_cast<int>( ChannelRoles::unreadMessages )} );
        emit unreadMessagesChanged();
    }
}

void ChannelModel::onDataChanged( const QString &id, const QString &property )
{
    int pos = 0;
    bool found = false;

    for ( const auto &current : channelList ) {
        if ( current->getRoomId() == id ) {
            found = true;
            break;
        }

        pos++;
    }

    if ( found ) {
        auto roles = roleNames();

        for ( auto it = roles.begin(); it != roles.end(); it++ ) {
            if ( roles[it.key()] == property ) {
                emit dataChanged( index( pos ), index( pos ), {it.key()} );
            }
        }
    }
}

void ChannelModel::onDeleted( const QString &pId, bool deleted )
{
    Q_UNUSED( deleted )
    int idx = -1;

    auto iter = channelList.begin();

    for ( ; iter != channelList.end(); iter++ ) {
        if ( ( *iter )->getRoomId() == pId ) {
            break;
        }
    }

    if ( iter != channelList.end() ) {
        idx = static_cast<int>( std::distance( channelList.begin(), iter ) );
        beginRemoveRows( QModelIndex(), idx, idx );
        channelList.erase( iter );
        endRemoveRows();
    }

}

void ChannelModel::onChannelOrderChanged( const QString &pId, qint64 pTimestamp )
{
    static int counter = 0;
    counter++;
    qDebug() << counter;
    auto changed = channelList.reOrder();
    emit dataChanged( createIndex( std::get<0>( changed ), 0 ), createIndex( std::get<1>( changed ), 0 ) );
}

void ChannelModel::addChannelSlot( const QSharedPointer<RocketChatChannel> &channel )
{
    addChannel( channel );
}

QString ChannelModel::getType() const
{
    return type;
}

void ChannelModel::setType( const QString &value )
{
    type = value;
}

void ChannelModel::sortChanged()
{

    beginResetModel();
    std::sort( channelList.begin(), channelList.end(), []( const QSharedPointer<RocketChatChannel> &channel1, const QSharedPointer<RocketChatChannel> &channel2 ) {
        auto youngest1 = channel1->getYoungestMessage();
        auto youngest2 = channel2->getYoungestMessage();

        if ( !youngest1.isNull() && !youngest2.isNull() ) {
            auto timestamp1 = youngest1->getTimestamp();
            auto timestamp2 = youngest2->getTimestamp();
            return timestamp2 < timestamp1;
        }

        if ( youngest1.isNull() && !youngest2.isNull() ) {
            return false;
        }

        if ( youngest2.isNull() && !youngest1.isNull() ) {
            return true;
        }

        return false;

    } );
    endResetModel();
    emit countChanged();
}

void ChannelModel::clear()
{
    beginResetModel();
    channelList.clear();
    duplicateCheck.clear();
    endResetModel();
}
