/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include "rocketchatupdatepushtokenrequest.h"
#include "config.h"

RocketChatUpdatePushTokenRequest::RocketChatUpdatePushTokenRequest( const QString &pToken, const QString &pUserId )
{
    QJsonArray params;
    QJsonObject options;
    QJsonObject tokenObject;
#ifdef Q_OS_LINUX
    Q_UNUSED( pToken );
#endif
#ifdef Q_OS_ANDROID
    tokenObject[QStringLiteral( "gcm" )] = pToken;
#endif
#ifdef Q_OS_IOS
    tokenObject["apn"] = pToken;
#endif

    if ( !tokenObject.isEmpty() ) {
        options[QStringLiteral( "userId" )] = pUserId;
        options[QStringLiteral( "token" )] = tokenObject;
        //TODO: handle appName
        options[QStringLiteral( "appName" )] = QStringLiteral( "qt mobile client" );
        params.append( options );
        buildRequest( QStringLiteral( "raix:push-update" ), params );
    }
}

RocketChatUpdatePushTokenRequest::RocketChatUpdatePushTokenRequest( const QString &pToken, const QString &pUserId, const DdpCallback &pSuccess ): RocketChatUpdatePushTokenRequest( pToken, pUserId )
{
    setSuccess( pSuccess );
}



