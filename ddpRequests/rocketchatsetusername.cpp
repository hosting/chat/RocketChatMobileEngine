#include "rocketchatsetusername.h"

RocketChatSetUsername::RocketChatSetUsername( const QString &pUsername, const DdpCallback &pSuccess )
{
    setSuccess( pSuccess );
    QJsonArray params;
    params.append( pUsername );
    buildRequest( QStringLiteral( "setUsername" ), params );
}
