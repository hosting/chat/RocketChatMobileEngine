/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#pragma once
#ifndef MESSAGESERVICE_H
#define MESSAGESERVICE_H

#include <QObject>
#include <functional>
#include <QSharedPointer>
#include <QMultiMap>
#include <QtAlgorithms>
#include <QDateTime>
#include <repos/messagerepository.h>
#include "repos/entities/rocketchatmessage.h"
#include "persistancelayer.h"
#include "rocketchatserver.h"
#include "repos/entities/rocketchatattachment.h"
#include "repos/emojirepo.h"
#include "services/requests/loadhistoryservicerequest.h"
#include "services/requests/loadHistoryrequestcontainer.h"
#include "services/requests/loadmissedmessageservicerequest.h"
#include "CustomModels/messagemodel.h"
#include "CustomModels/messagesearchresultsmodel.h"
#include "services/fileservice.h"

typedef QList<QSharedPointer<RocketChatMessage>> MessageList;
typedef QSharedPointer<RocketChatMessage> ChatMessage;
typedef QMultiMap<QString, QSharedPointer<RocketChatMessage>> MessageMap;
class PersistanceLayer;
class RocketChatServerData;
class FileService;

class MessageService : public QObject
{
        Q_OBJECT
    public:
        // MessageService(PersistanceLayer *pPersistanceLayer, RocketChatServerData* pServer, const QHash<QString, QString> &pEmojisMap, FileService &pFileService);
        MessageService( QObject *parent, PersistanceLayer *pPersistanceLayer, RocketChatServerData *pServer, EmojiRepo *mEmojiRepo, FileService *pFileService );
        void loadHistory( const QSharedPointer<LoadHistoryServiceRequest> & );
        void checkForMissingMessages( const QSharedPointer<LoadMissedMessageServiceRequest> &pRequest );
        void sendMessage( const QSharedPointer<RocketChatMessage> &pMessage );
        void persistMessage( const QSharedPointer<RocketChatMessage> &pMessage );
        void persistMessages( const MessageList &pMessage );
        void persist();
        QSharedPointer<RocketChatMessage> parseMessage( const QJsonObject &pMessage, bool linkify = true );
        void addUserToBlockList( const QString &pId );
        void addChannelToBlockList( const QString &pId );
        void searchMessage( const QString &pTerm, const QString &pRoom );
        void deleteMessagesNotInList( MessageMap *pMessages, QString pChannelId, bool pCheckForYounger );
    protected:
        QRegularExpression mReplyRegeEx;
        EmojiRepo *mEmojiRepo;
        MessagesModel *mMessagesModel;
        QRegularExpression mReplyReplyRegeEx;
        const static int mExpectedSize = 50;
        PersistanceLayer *mPersistanceLayer;
        RocketChatServerData *mServer;
        MessageSearchResultsModel *mSearchResults;
        FileService *mFileService;

        void loadHistoryFromServer( const QSharedPointer<LoadHistoryRequestContainer> &pContainer );
        void loadHistoryFromServer( const QSharedPointer<LoadHistoryServiceRequest> &pContainer );
        QMultiMap<QString, ChatMessage> *loadHistoryFromDb( const QSharedPointer<LoadHistoryServiceRequest> &pRequest );
        void loadHistoryFromAutoSource( const QSharedPointer<LoadHistoryServiceRequest> &pRequest );
        QList<QSharedPointer<RocketChatAttachment>> processAttachments( const QJsonObject &pMessageData );
        QSet<QString> mBlockedUsers;
};

#endif // MESSAGESERVICE_H
