/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include "fileservice.h"

FileService::FileService( RocketChatServerData *server )
{
    if ( server != nullptr ) {
        mStorage = PersistanceLayer::instance();
        mTempPath = QStandardPaths::writableLocation( QStandardPaths::TempLocation );
        mDocumentPath = QStandardPaths::writableLocation( QStandardPaths::DocumentsLocation );

        if ( !mDocumentPath.size() ) {
            mDocumentPath = mTempPath;
        }

        mImagepath = QStandardPaths::writableLocation( QStandardPaths::PicturesLocation );

        if ( !mImagepath.size() ) {
            mImagepath = mDocumentPath;
        }

        mVideoPath = QStandardPaths::writableLocation( QStandardPaths::MoviesLocation );

        if ( !mVideoPath.size() ) {
            mVideoPath = mDocumentPath;
        }

        mMusicPath = QStandardPaths::writableLocation( QStandardPaths::MusicLocation );

        if ( !mMusicPath.size() ) {
            mMusicPath = mDocumentPath;
        }

        mServer = server;

        getFilesFromDb();
    }
}


/**
 * This method tries to find a file and decides by itself to load it from disk or server
 *
 * WARNING: If the file is already in the download process,
 *
 * @brief FileService::getFileRessource
 * @param request
 * @param pThen function that is execeuted when thre is some result
 * @return bool true if the file is already downloaded, then is not executed in this case
 */
bool FileService::getFileRessource( const QSharedPointer< FileRequest > &pRequest )
{
    if ( mServer ) {
        auto repo = mServer->getFiles();

        if ( !pRequest.isNull() && pRequest->url.size() > 0 ) {
            if ( mCurrentDownloads.contains( pRequest->url ) ) {
                return true;
            }

            auto tempFile = getFileFromCache( pRequest );

            if ( !tempFile.isNull() ) {

                if ( !pRequest->showInline ) {
                    auto location = determineLocation( pRequest->url, pRequest->type, false );
                    QFile targetFile( location );

                    if ( targetFile.exists() ) {
                        auto parts = location.split( "." );
                        location.clear();

                        for ( auto i = 0 ; i < ( parts.length() - 1 ); i++ ) {
                            location += parts[i];

                            if ( i < ( parts.length() - 1 ) ) {
                                location += QStringLiteral( "." );
                            }
                        }

                        location += QStringLiteral( "_" ) + QString::number( QDateTime::currentSecsSinceEpoch() );
                        location += QStringLiteral( "." ) + parts[parts.size() - 1];

                        QFile oldFile( tempFile->getFilePath() );

                        if ( oldFile.copy( location ) ) {
                            tempFile = QSharedPointer<TempFile>::create( location, pRequest->url );
                        } else {
                            qWarning() << oldFile.errorString();
                        }

                    } else {
                        QFile oldFile( tempFile->getFilePath() );
                        QString path = location;
                        QStringList pathParts = location.split( "/" );
                        pathParts.removeLast();
                        QString dirStr = pathParts.join( "/" );
                        QDir dir;

                        if ( dir.mkpath( dirStr ) ) {
                            if ( oldFile.copy( location ) ) {
                                tempFile = QSharedPointer<TempFile>::create( location, pRequest->url );
                            } else {
                                qWarning() << oldFile.errorString();
                            }
                        } else {
                            qWarning() << "could not create: " << dirStr;
                        }
                    }

                }



                pRequest->then( tempFile, pRequest->showInline );
            } else {
                getFileFromServer( pRequest );
            }

        }
    }

    return false;
}

void FileService::getFilesFromDb()
{
    if ( mServer ) {
        auto files = mStorage->getFiles();
        auto repo = mServer->getFiles();

        for ( const auto &file : files ) {
            auto url = file["url"];
            auto path = file["path"];
            repo->add( url, QSharedPointer<TempFile>::create( path, url ) );
        }
    }
}

QSharedPointer<TempFile> FileService::getFileFromCache( const QSharedPointer< FileRequest > &pRequest )
{
    if ( !pRequest.isNull() && mServer ) {
        auto repo = mServer->getFiles();

        if ( repo == nullptr ) {
            qDebug() << "repo is null";
        }

        QString baseUrl = mServer->getRestApi()->getBaseUrl();
        QString fullUrl = baseUrl + pRequest->url ;
        qDebug() << "file from cache requested" << fullUrl;

        if ( repo->contains( fullUrl ) ) {
            auto storageEntry = repo->get( fullUrl );

            qDebug() << "cachehit:" << fullUrl << ":" << storageEntry->getFilePath();

            if ( storageEntry.isNull() ) {
                return QSharedPointer<TempFile>( nullptr );
            }


            if ( storageEntry->getFileChecked() ) {
                return storageEntry;
            } else {
                QFile cacheHit( storageEntry->getFilePath() );

                if ( cacheHit.exists() ) {
                    storageEntry->setFileChecked( true );
                    return storageEntry;
                }
            }

            mStorage->removeFileCacheEntry( pRequest->url, storageEntry->getFilePath() );
            repo->remove( fullUrl );
        }

        return QSharedPointer<TempFile>( nullptr );


    }

    return QSharedPointer<TempFile>( nullptr );

}

void FileService::getFileFromServer( const QSharedPointer< FileRequest > &pRequest )
{
    if ( Q_LIKELY( !pRequest.isNull() && mServer ) ) {
        auto baseUrl = mServer->getRestApi()->getBaseUrl();
        auto fullUrl = baseUrl + pRequest->url ;

        if ( !mCurrentDownloads.contains( fullUrl ) ) {
            RestRequestCallback success = [ = ]( QNetworkReply * pReply, const QJsonObject & pData, RestApi * api ) {
                Q_UNUSED( pData );
                Q_UNUSED( api );

                if ( pReply ) {
                    auto content = pReply->readAll();

                    QString location;

                    //special treatment for avatars as RocketChat does server avatars always as .jpg even if they are svgs
                    if ( pRequest->url.startsWith( QStringLiteral( "/avatar/" ) ) ) {
                        auto mimetype = mMimeDb.mimeTypeForData( content );
                        location = determineAvatarLocation( pReply->url(), mimetype );
                    } else {
                        location = determineLocation( pReply->url(), pRequest->type, pRequest->showInline );
                    }

                    QUrl locationUrl( location );
                    auto dirPath = locationUrl.adjusted( QUrl::RemoveFilename ).toString();
                    auto parts = location.split( '/' );

                    QDir dir( dirPath );

                    if ( !dir.exists() ) {
                        auto tempPath = parts.first();

                        for ( int i = 1; i < parts.length() - 1; i++ ) {
                            tempPath.append( '/' + parts[i] );
                            QDir temp( tempPath );

                            if ( !temp.exists() ) {
                                temp.mkdir( tempPath );
                            }
                        }
                    }

                    if ( dir.exists() ) {

                        QFile file( location );
                        QString url = pReply->url().toString();

                        bool writeContent = true;

                        if ( file.exists() ) {
                            QString fileHash = getFileHash( &file );
                            QString dataHash = QCryptographicHash::hash( content, QCryptographicHash::Md5 );

                            if ( fileHash == dataHash ) {
                                auto tempfile = QSharedPointer<TempFile>::create( location, url );
                                pRequest->then( tempfile, pRequest->showInline );

                                if ( location.size() ) {
                                    if ( pRequest->showInline ) {
                                        mStorage->addFileCacheEntry( url, location );
                                        auto repo = mServer->getFiles();
                                        repo->add( url, tempfile );
                                    }

                                    mCurrentDownloads.remove( fullUrl );
                                }

                                writeContent = false;

                                return;
                            }
                        }

                        file.close();

                        if ( writeContent && file.open( QIODevice::ReadWrite ) ) {
                            auto written = file.write( content );
                            qDebug() << "wrote byte to file" << written;
                            file.close();
                            auto tempfile = QSharedPointer<TempFile>::create( location, url );

                            if ( pRequest->showInline ) {
                                mStorage->addFileCacheEntry( url, location );
                                auto repo = mServer->getFiles();
                                repo->add( url, tempfile );
                            }

                            pRequest->then( tempfile, pRequest->showInline );

                        } else if ( writeContent ) {
                            qWarning() << "Could not write file" + location;
                            qWarning() << file.errorString();
                        }

                        mCurrentDownloads.remove( fullUrl );
                    } else {
                        qWarning() << "folder does not exist: " << dirPath;
                    }
                }
            };
            RestRequestCallback error = [ = ]( QNetworkReply * reply, const QJsonObject & pData, RestApi * api ) {
                Q_UNUSED( reply );
                Q_UNUSED( pData );
                Q_UNUSED( api );
                qDebug() << "could not download file" << pRequest->url;
            };

            mCurrentDownloads.insert( fullUrl );
            auto request = QSharedPointer<GetRequest>::create( fullUrl, success, error, true, true );
            mServer->sendApiRequest( request );
        }
    }
}

QString FileService::getFileHash( QFile *pFile )
{
    QString hashString = "";

    if ( pFile ) {
        if ( pFile->open( QFile::ReadOnly ) ) {
            QCryptographicHash hash( QCryptographicHash::Md4 );

            if ( hash.addData( pFile ) ) {
                hashString =  hash.result();
            }

            pFile->close();
        }
    }

    return hashString;
}


QString FileService::determineLocation( const QUrl &pUrl, const QString &pType, bool pShowInline )
{
    auto urlPath = QStringLiteral( "/rocketChat" ) + pUrl.path();
    QString filePath;

    if ( !pShowInline ) {
        if ( pType == "image" ) {
            filePath = mImagepath + urlPath;
        } else if ( pType == "video" ) {
            filePath = mVideoPath + urlPath;
        } else if ( pType == "music" ) {
            filePath = mMusicPath + urlPath;
        } else if ( pType == "document" ) {
            filePath = mDocumentPath + urlPath;
        } else {
            filePath = mTempPath + urlPath;
        }
    } else {
        filePath = mTempPath + urlPath;
    }

    return filePath;
}

QString FileService::determineAvatarLocation( const QUrl &pUrl, const QMimeType &pType )
{
    QString mimeTypeName = pType.name();
    QString urlString = pUrl.toString();
    QString urlPath = QStringLiteral( "/rocketChat" ) + pUrl.path();
    QString filePath;

    filePath = mTempPath + urlPath;

    if ( pType.name().contains( "svg" ) ) {
        QStringList parts = filePath.split( "." );

        //change ending to correct ending if mimetype and ending collide
        if ( !parts.isEmpty() && parts.last() == QStringLiteral( "jpg" ) ) {
            QString origSuffix = parts.last();
            QString suffix = pType.preferredSuffix();
            qDebug() << "origSuffix " << origSuffix;
            int origSuffixLength = origSuffix.length() + 1;
            filePath.replace( filePath.length() - origSuffixLength, origSuffixLength, "." + suffix );
        }
    }

    return filePath;
}

FileRequest::FileRequest( const QString &url, const QString &type, const std::function<void ( QSharedPointer<TempFile>, bool )> &then, bool showInline ):
    type( type ), url( url ), then( then ), showInline( showInline )
{
}
