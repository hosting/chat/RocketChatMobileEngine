/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#ifndef GOOGLEPUSHNOTIFICATIONS_H
#define GOOGLEPUSHNOTIFICATIONS_H
#include "notifications/notificationabstract.h"
#include "notifications/android/androidnotificationreceiver.h"
#include "config.h"

class AndroidNotificationReceiver;
class GooglePushNotifications: public NotificationAbstract
{
        Q_OBJECT
    public:
        GooglePushNotifications();

        //    QString getGcmSenderId() const;
        //    void setGcmSenderId(const QString &pValue);

        QString getGcmRegistrationId() const;
        void setGcmRegistrationId( const QString &pValue );

        void pushMessageReceived( const QString &pServer, const QString &pRid, const QString &pName, const QString &pType );

        void init();

        void registerWithService();

    private:
        //  QString mGcmSenderId = GCM_SENDER_ID;
        QString mGcmRegistrationId;
        AndroidNotificationReceiver *mReceiver;


        // NotificationAbstract interface
    public:
        void checkForPendingMessages();
};

#endif // GOOGLEPUSHNOTIFICATIONS_H
