#ifndef GETSERVERINFOREQUEST_H
#define GETSERVERINFOREQUEST_H

#include "getrequest.h"
#include "restpaths.h"

class GetServerInfoRequest: public GetRequest
{
    public:
        GetServerInfoRequest( const RestRequestCallback &pSuccess, const RestRequestCallback &pError );
};

#endif // GETSERVERINFOREQUEST_H
