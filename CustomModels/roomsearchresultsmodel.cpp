#include "roomsearchresultsmodel.h"

int RoomSearchResultsModel::rowCount( const QModelIndex &parent ) const
{
    Q_UNUSED( parent )
    return channelList.size();
}

QVariant RoomSearchResultsModel::data( const QModelIndex &index, int role ) const
{
    int row = index.row();

    if ( channelList.isEmpty() || row > channelList.count() ) {
        return QVariant();
    }

    auto currentChannel = channelList.at( row );

    auto roleType = static_cast<ChannelRoles>( role );


    switch ( roleType ) {
        case ChannelRoles::name:
            return currentChannel->getName();

        case ChannelRoles::roomId:
            return currentChannel->getRoomId();

        case ChannelRoles::channelType:
            return currentChannel->getType();

        case ChannelRoles::unreadMessages:
            return currentChannel->getUnreadMessages();

        case ChannelRoles::readonly:
            return currentChannel->getReadOnly() || currentChannel->getArchived() || currentChannel->getBlocked();

        case ChannelRoles::ownerName:
            return currentChannel->getOwnerName();

        case ChannelRoles::ownerId:
            return currentChannel->getOwnerId();

        case ChannelRoles::archived:
            return currentChannel->getArchived();

        case ChannelRoles::blocked:
            return currentChannel->getBlocked();

        case ChannelRoles::username:
            return currentChannel->getUsername();

        case ChannelRoles::deleted:
            //TODO implement or remove!
            break;
    }

    return QVariant();

}

QHash<int, QByteArray> RoomSearchResultsModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[static_cast<int>( ChannelRoles::name )] = QByteArrayLiteral( "name" );
    roles[static_cast<int>( ChannelRoles::roomId )] =  QByteArrayLiteral( "roomId" );
    roles[static_cast<int>( ChannelRoles::unreadMessages )] = QByteArrayLiteral( "unreadMessages" );
    roles[static_cast<int>( ChannelRoles::channelType )] = QByteArrayLiteral( "type" );
    roles[static_cast<int>( ChannelRoles::readonly )] = QByteArrayLiteral( "readonly" );
    roles[static_cast<int>( ChannelRoles::archived )] = QByteArrayLiteral( "archived" );
    roles[static_cast<int>( ChannelRoles::blocked )] = QByteArrayLiteral( "blocked" );
    roles[static_cast<int>( ChannelRoles::deleted )] = QByteArrayLiteral( "deleted" );
    roles[static_cast<int>( ChannelRoles::ownerId )] = QByteArrayLiteral( "ownerId" );
    roles[static_cast<int>( ChannelRoles::ownerName )] = QByteArrayLiteral( "ownerName" );
    roles[static_cast<int>( ChannelRoles::username )] = QByteArrayLiteral( "username" );

    return roles;
}

void RoomSearchResultsModel::clear()
{
    beginResetModel();
    channelList.clear();
    endResetModel();
}

void RoomSearchResultsModel::addChannels( const channelVector &pChannels )
{
    beginResetModel();
    channelList.clear();
    channelList << pChannels;
    endResetModel();
}
