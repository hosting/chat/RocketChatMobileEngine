/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#include "restapi.h"

RestApi::RestApi( QObject *parent, const QString &pBaseUrl ) : QObject( parent ),  mApiUrl( pBaseUrl + mApiPath ), mBaseUrl( pBaseUrl )
{
    qRegisterMetaType<RestApiRequest>( "RestApiRequest" );
}

void RestApi::init()
{
    mNam = new QNetworkAccessManager( this );
    mApiLogin = QStringLiteral( "/login" );
    mApiLogoff = QStringLiteral( "/logout" );
    mCookieJar = new QNetworkCookieJar;
    mNam->setCookieJar( mCookieJar );
    mNetworkReplies.reserve( 100 );
    mResponseBinding.reserve( 100 );

    mError = new QJsonParseError;

    qRegisterMetaType<RestApiRequest>( "RestApiRequest" );

    mStorage = PersistanceLayer::instance();
    connect( mNam, &QNetworkAccessManager::finished, this, &RestApi::processNetworkRequest, Qt::UniqueConnection );
    connect( this, &RestApi::sendRequestSignal, this, &RestApi::sendRequestSlot, Qt::UniqueConnection );
}

void RestApi::login( const QString &pUsername, const QString &pPassword )
{
    if ( pUsername.length() && pPassword.length() ) {
        QUrl url = QString( mApiUrl + mApiLogin );
        QByteArray data;
        data.append( "user=" + pUsername + "&" );
        data.append( "password=" + pPassword );

        this->mName = pUsername;
        this->mPass = pPassword;

        QNetworkRequest request;
        request.setHeader( QNetworkRequest::ContentTypeHeader, QString( QStringLiteral( "application/x-www-form-urlencoded" ) ) );
        request.setUrl( url );
        qDebug() << "send rest api login to " + mApiUrl + mApiLogin;
        mNetworkReplies[mNam->post( request, data )] = methods::LOGIN;
    } else {
        mLoginErrorString = QStringLiteral( "no username or password provided" );
        emit( loginError() );
    }
}




void RestApi::logout()
{
    QUrl url = QString( mApiUrl + mApiLogoff );
    QNetworkRequest request;
    request.setUrl( url );
    request.setRawHeader( QByteArray( QByteArrayLiteral( "X-Auth-Token" ) ), QByteArray( mToken.toLocal8Bit() ) );
    request.setRawHeader( QByteArray( QByteArrayLiteral( "X-User-Id" ) ), QByteArray( mUserId.toLocal8Bit() ) );


    // mNetworkReplies[mNam->get( request )] = methods::LOGOFF;

}


void RestApi::processLoginRequest( QNetworkReply *pReply )
{
    if ( pReply ) {
        QString replyString = ( QString )pReply->readAll();
        QJsonDocument replyJson = QJsonDocument::fromJson( replyString.toUtf8(), mError );
        QJsonObject replyObject = replyJson.object();

        if ( replyObject[QStringLiteral( "status" )].toString() == QStringLiteral( "success" ) && replyObject.contains( QStringLiteral( "data" ) ) ) {
            QJsonObject data = replyObject[QStringLiteral( "data" )].toObject();

            if ( data.contains( QStringLiteral( "authToken" ) ) && data.contains( QStringLiteral( "userId" ) ) ) {
                this->mToken = data[QStringLiteral( "authToken" )].toString();
                this->mUserId = data[QStringLiteral( "userId" )].toString();
                qDebug() << "REST token: " << this->mToken;
                qDebug() << "REST user: " << this->mUserId;
                emit( loggedIn() );
                initCookie();
            }
        } else {
            mLoginErrorString = QStringLiteral( "wrong username or password" );
            qDebug() << "log in error" << replyString << pReply->errorString();
            emit( loginError() );
        }

        mNetworkReplies.remove( pReply );
        pReply->deleteLater();
    }
}


void RestApi::processLogoutRequest( QNetworkReply *pReply )
{
    if ( pReply ) {
        mNetworkReplies.remove( pReply );
        pReply->deleteLater();
    }
}

void RestApi::processNetworkRequest( QNetworkReply *pReply )
{
    if ( pReply ) {
        int status = pReply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt();

        if ( mResponseBinding.contains( pReply ) && !mResponseBinding[pReply].isNull() ) {
            if ( mResponseBinding[pReply]->isRawData() ) {
                auto request =  mResponseBinding[pReply];
                auto success = request->getSuccess();
                auto error = request->getError();
                QJsonObject replyObject;

                if ( pReply->attribute( QNetworkRequest::HttpStatusCodeAttribute ) == 200 ) {
                    success( pReply, replyObject, this );
                } else {
                    error( pReply, replyObject, this );
                }
            } else {

                QString replyString = ( QString )pReply->readAll();
                // qDebug() << replyString;
                QJsonDocument replyJson = QJsonDocument::fromJson( replyString.toUtf8(), this->mError );
                QJsonObject replyObject = replyJson.object();
                auto request =  mResponseBinding[pReply];
                auto success = request->getSuccess();
                auto error = request->getError();

                if ( ( replyObject[QStringLiteral( "status" )].toString() == QStringLiteral( "success" ) || ( status >= 200 && status < 300 ) ) && success ) {
                    success( pReply, replyObject, this );
                } else if ( error ) {
                    error( pReply, replyObject, this );
                }
            }

            mResponseBinding.remove( pReply );
            return;
        }

        switch ( mNetworkReplies[pReply] ) {

            case methods::LOGIN:
                qDebug() << "login reply received";
                processLoginRequest( pReply );
                break;

            case methods::LOGOFF:
                processLogoutRequest( pReply );
                break;

            case methods::DOWNLOAD:
                processDownload( pReply );
                break;

            case methods::UPLOAD:
                if ( pReply->attribute( QNetworkRequest::HttpStatusCodeAttribute ) == 200 ) {
                    qDebug() << "upload ready" << pReply->readAll() << pReply->attribute( QNetworkRequest::HttpStatusCodeAttribute );
                    emit fileUploadReady( true );
                } else {
                    qDebug() << pReply->readAll();
                    qDebug() << "HTTP ERROR, status code:" << pReply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt();
                    emit fileUploadReady( false );
                }

                break;

            default:
                qDebug() << pReply->readAll();
                pReply->deleteLater();
        }
    }
}

void RestApi::slotError( QNetworkReply::NetworkError error )
{
    qDebug() << error;
}


QNetworkReply *RestApi::post( const QString &pUrl, const QByteArray &pData, const QString &pMimeType )
{
    QNetworkRequest request;
    request.setUrl( pUrl );
    request.setHeader( QNetworkRequest::ContentTypeHeader, pMimeType );
    request.setRawHeader( QByteArray( QByteArrayLiteral( "X-Auth-Token" ) ), QByteArray( mToken.toLocal8Bit() ) );
    request.setRawHeader( QByteArray( QByteArrayLiteral( "X-User-Id" ) ), QByteArray( mUserId.toLocal8Bit() ) );
    request.setAttribute( QNetworkRequest::HttpPipeliningAllowedAttribute, true );
    request.setAttribute( QNetworkRequest::HTTP2AllowedAttribute, true );
    request.setAttribute( QNetworkRequest::SpdyAllowedAttribute, true );
    QNetworkReply *reply = mNam->post( request, pData );
    return reply;
}

QNetworkReply *RestApi::get( const QString &pUrl, const QString &pMimeType )
{
    QNetworkRequest request;
    request.setUrl( pUrl );
    request.setRawHeader( QByteArray( QByteArrayLiteral( "X-Auth-Token" ) ), QByteArray( mToken.toLocal8Bit() ) );
    request.setRawHeader( QByteArray( QByteArrayLiteral( "X-User-Id" ) ), QByteArray( mUserId.toLocal8Bit() ) );
    request.setHeader( QNetworkRequest::ContentTypeHeader, QString( pMimeType ) );
    request.setAttribute( QNetworkRequest::HttpPipeliningAllowedAttribute, true );
    request.setAttribute( QNetworkRequest::HTTP2AllowedAttribute, true );
    request.setAttribute( QNetworkRequest::SpdyAllowedAttribute, true );

    QNetworkReply *reply = mNam->get( request );
    return reply;
}

QString RestApi::getLoginErrorString()
{
    return mLoginErrorString;
}


QString RestApi::getPass() const
{
    return mPass;
}

QString RestApi::getName() const
{
    return mName;
}

void RestApi::processDownload( QNetworkReply *pReply )
{
    if ( pReply ) {
        qDebug() << "file downloaded";
        qDebug() << "requstURI: " << pReply->url().path();

        if ( pReply->error() ) {
            qDebug() << "File received" + pReply->errorString();
        } else {
            QByteArray content = pReply->readAll();
            QMimeType type = mMimeDb.mimeTypeForData( content );
            QString suffix = type.preferredSuffix();
            QString filename = pReply->url().fileName();
            QString directory = QStandardPaths::writableLocation( QStandardPaths::TempLocation );
            QString filePath = directory + '/' + filename;
            filePath.replace( filePath.length() - 4, 4, '.' + suffix );
            QFile file( filePath );

            if ( file.open( QIODevice::ReadWrite ) ) {
                file.write( content );
                file.close();
            }

            emit( fileDownloaded( pReply->url().path(), filePath ) );
        }

        pReply->deleteLater();
    }
}

RestApi::~RestApi()
{
    logout();
    delete mError;
}

QString RestApi::getBaseUrl() const
{
    return mBaseUrl;
}



void RestApi::sendRequest( const RestApiRequest &pRequest )
{
    emit( sendRequestSignal( pRequest ) );
}

void RestApi::initCookie()
{
    QString host = mBaseUrl.split( "//" )[1];
    QNetworkCookie rc_uid;
    rc_uid.setDomain( host );
    rc_uid.setName( QByteArrayLiteral( "rc_uid" ) );
    rc_uid.setValue( mUserId.toUtf8() );

    QNetworkCookie rc_token;

    rc_token.setDomain( host );
    rc_token.setName( QByteArrayLiteral( "rc_token" ) );
    rc_token.setValue( mToken.toUtf8() );

    mCookieJar->insertCookie( rc_token );
    mCookieJar->insertCookie( rc_uid );
}

void RestApi::setToken( const QString &pToken )
{
    this->mToken = pToken;

    if ( this->mUserId.length() ) {
        initCookie();
    }
}

void RestApi::setUserId( const QString &pUserId )
{
    this->mUserId = pUserId;

    if ( this->mToken.length() ) {
        initCookie();
    }
}

QHash<QNetworkReply *, RestApiRequest> RestApi::getResponseBinding() const
{
    return mResponseBinding;
}

void RestApi::sendRequestSlot( const RestApiRequest &pRequest )
{
    if ( !pRequest.isNull() ) {
        QNetworkReply *reply = nullptr;
        QString path;

        if ( !pRequest->getAbsolutePath() ) {
            path = mApiUrl + pRequest->getPath();
        } else {
            path = pRequest->getPath();
        }

        if ( pRequest->getMethod() == RestRequest::methods::GET ) {
            reply = get( path, pRequest->getMimeType() );
        }

        if ( pRequest->getMethod() == RestRequest::methods::POST ) {
            reply = post( path, pRequest->getContent(), pRequest->getMimeType() );
        }

        connect( reply, SIGNAL( error( QNetworkReply::NetworkError ) ),
                 this, SLOT( slotError( QNetworkReply::NetworkError ) ) );

        mResponseBinding[reply] = pRequest;
    }
}
