/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include "googlepushnotifications.h"
#include "androidnotificationreceiver.h"
#include <QDebug>

GooglePushNotifications::GooglePushNotifications()
{
    mReceiver = AndroidNotificationReceiver::instance();
    mReceiver->setReceiver( this );
}

//QString GooglePushNotifications::getGcmSenderId() const
//{
//    return mGcmSenderId;
//}

//void GooglePushNotifications::setGcmSenderId(const QString &pSenderId)
//{
//    if(mGcmSenderId != pSenderId){
//        mGcmSenderId = pSenderId;
//    }
//}

QString GooglePushNotifications::getGcmRegistrationId() const
{
    return mGcmRegistrationId;
}

void GooglePushNotifications::setGcmRegistrationId( const QString &pRegistrationId )
{
    if ( !pRegistrationId.isEmpty() ) {
        if ( mGcmRegistrationId != pRegistrationId ) {
            mGcmRegistrationId = pRegistrationId;
            emit tokenReceived( pRegistrationId );
        }
    }
}

void GooglePushNotifications::pushMessageReceived( const QString &pServer, const QString &pRid, const QString &pName, const QString &pType )
{
    qDebug() << "push message received";
    emit messageReceived( pServer, pRid, pName, pType );
}

void GooglePushNotifications::init()
{
    // if(!mGcmSenderId.isEmpty()){
    if ( mGcmRegistrationId.isEmpty() ) {
        qDebug() << "init gcm register called";
        mReceiver->doRegister( this );
    } else {

    }

    // }
}

void GooglePushNotifications::registerWithService()
{
    init();
}

void GooglePushNotifications::checkForPendingMessages()
{

}
