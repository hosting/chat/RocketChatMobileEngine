#include "rocketchatgetroombyid.h"

RocketChatGetRoomById::RocketChatGetRoomById( const QString &pId )
{
    QJsonArray params = {pId};
    buildRequest( QStringLiteral( "getRoomNameById" ), params );
}

RocketChatGetRoomById::RocketChatGetRoomById( const QString &pId, const DdpCallback &pSuccess ): RocketChatGetRoomById( pId )
{
    setSuccess( pSuccess );
}
