/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#ifndef RESTAPI_H
#define RESTAPI_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QJsonDocument>
#include <QHash>
#include <QMetaObject>
#include <QNetworkReply>
#include <QNetworkCookieJar>
#include <QMimeDatabase>
#include <QMimeType>
#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QString>
#include <QDebug>
#include <QStandardPaths>
#include <QDir>
#include <QHash>
#include <QDataStream>
#include <QNetworkCookieJar>
#include <QNetworkCookie>

#include "persistancelayer.h"
#include "restRequests/restrequest.h"
typedef QSharedPointer<RestRequest> RestApiRequest;
class PersistanceLayer;
class RestApi: public QObject
{
        Q_OBJECT

    public:
        RestApi( QObject *parent, const QString &pBaseUrl );
        void login( const QString &pUsername, const QString &pPassword );
        void logout();
        QString getName() const;
        QString getPass() const;
        QNetworkReply *post( const QString &pUrl, const QByteArray &pArray, const QString &pMimeType );
        QNetworkReply *get( const QString &pUrl, const QString &pMimeType );
        QString getLoginErrorString();
        QString mName;
        QString mPass;
        ~RestApi();
        QString getBaseUrl() const;
        void sendRequest( const RestApiRequest & );
        void initCookie();

        void setToken( const QString & );
        void setUserId( const QString & );

        QHash<QNetworkReply *, RestApiRequest> getResponseBinding() const;

    private:
        enum methods {
            LOGIN,
            LOGOFF,
            DOWNLOAD,
            UPLOAD
        };


        QNetworkCookieJar *mCookieJar = nullptr;

        QNetworkAccessManager *mNam = nullptr;

        QHash<QNetworkReply *, methods> mNetworkReplies;
        QHash<QNetworkReply *, RestApiRequest> mResponseBinding;
        QMimeDatabase mMimeDb;
        QString mToken = "";
        QString mUserId = "";

        QString mApiPath = QStringLiteral( "/api/v1" );
        QString mApiUrl = "";
        QString mBaseUrl = "";
        QString mApiLogin = "";
        QString mApiLogoff = "";

        QString mLoginErrorString = "";

        QJsonParseError *mError;

        PersistanceLayer *mStorage;

        void sendRequestSlot( const RestApiRequest &pRequest );

        void processLoginRequest( QNetworkReply * );
        void processLogoutRequest( QNetworkReply * );
        void processNetworkRequest( QNetworkReply * );

    public slots:
        void slotError( QNetworkReply::NetworkError error );
        void init();
    signals:
        void loggedIn();
        void fileDownloaded( QString pUrl, QString pTempfile );
        void loginError();
        void fileUploadReady( bool pUploadSuccessfull );
        void sendRequestSignal( RestApiRequest );

    protected:
        void processDownload( QNetworkReply *pReply );
};

#endif // RESTAPI_H
