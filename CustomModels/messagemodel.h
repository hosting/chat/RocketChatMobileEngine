/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#ifndef MESSAGEMODEL_H
#define MESSAGEMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QString>
#include <QMap>

#include "repos/entities/rocketchatchannel.h"
#include "container/sortedvector.h"
class RocketChatChannel;

class MessagesModel : public QAbstractListModel
{
        Q_OBJECT
        Q_PROPERTY( QString currentChannel READ getCurrent WRITE setCurrent NOTIFY currentChannelChanged )


public:
        enum class MessageRoles : int {
            text = Qt::UserRole + 1,
            type,
            linkurl,
            author,
            date,
            time,
            ownMessage,
            width,
            height,
            formattedDate,
            formattedTime,
            messageType,
            authorId,
            blocked,
            id,
            replyMessage,
            replyAutor,
            replyDate,
            replyUrl,
            avatarImg
        };
        MessagesModel();
        QVariant data( const QModelIndex &index, int role = Qt::DisplayRole ) const;
        QString getCurrent() const;
        void setCurrent( const QString &value );
        Q_INVOKABLE int rowCount( const QModelIndex &parent = QModelIndex() ) const;

        Q_INVOKABLE int getPositionOfMessage( const QString &pId );

        void onCurrentChannelChanged();
        bool addChannel( const QSharedPointer<RocketChatChannel> & );
    public slots:
        void addBlockedUser( const QString &pUserId );

    protected:
        QString current;
        QSet<QString> duplicateCheck;

        QHash<int, QByteArray> roleNames() const;
        bool buffered = false;
        int mInsertCount = 0;
        void onNewMessage( const QSharedPointer<RocketChatChannel> &channel, qint64 pMessageId );
        SortedVector<RocketChatMessage> messagesOfCurrentChannel;
        void onBeginInsertList( const QString &pChannelId );
        void onEndInsertList( const QString &pChannelId );
        QMap<QString, QSharedPointer<RocketChatChannel>> mChannelMap;
        QMutex mutex;
        QSet<QString> mBlockedUsers;
        void onMessageDeleted(const QString channelId, const QString messageId);
        void onDataChanged( const QString &id, const QString &property );
    signals:
        void currentChannelChanged( void );
        void countChanged();
    public slots:
        void addChannelSlot( const QSharedPointer<RocketChatChannel> & );
};

#endif // MESSAGEMODEL_H
