/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#ifndef ROCKETCHATCHANNEL_H
#define ROCKETCHATCHANNEL_H

#include <QObject>
#include <QJsonObject>
#include <QString>
#include <QMap>
#include <QList>
#include <QJsonDocument>
#include <QDebug>
#include <QJsonArray>
#include <QLinkedList>
#include <QtConcurrent>

#include "repos/messagerepository.h"
#include "repos/channelrepository.h"
#include "repos/entities/rocketchatuser.h"
#include "repos/entities/rocketchatmessage.h"

#include "api/restapi.h"
#include "api/meteorddp.h"
#include "restRequests/postrequest.h"
#include "restRequests/restsendmessagerequest.h"
#include "ddpRequests/ddpmethodrequest.h"
#include "ddpRequests/ddpsubscriptionrequest.h"
#include "ddpRequests/rocketchatsubscribechannelrequest.h"

#include "utils.h"
#include "persistancelayer.h"
#include "rocketchatserver.h"
#include "services/messageservice.h"

typedef QSharedPointer<RestRequest> RestApiRequest;

class PersistanceLayer;
class RocketChatServerData;

class RocketChatChannel : public QObject
{
        Q_OBJECT
    public:
        RocketChatChannel( RocketChatServerData *pServer, MessageService *pMessageService, const QString &pRoomId, const QString &pName, const QString &pType );
        RocketChatChannel( );

        ~RocketChatChannel();

        bool addMessage( const ChatMessage &, bool nosiginal = false );
        void setRoomId( const QString &pValue );
        void setName( const QString &pValue );
        void setUsers( const QMap<QString, QSharedPointer<RocketChatUser> > &pValue );
        void setUsers( const QList<QSharedPointer<RocketChatUser>> &pValue );
        void setJoined( bool pValue );
        void setUnreadMessages( unsigned int pValue );
        bool addUser( const QSharedPointer<RocketChatUser> &pUser );
        QList<QSharedPointer<RocketChatMessage>> addMessages( const QList<QSharedPointer<RocketChatMessage>> &pMessagesList );

        const QHash<QString, QSharedPointer< RocketChatMessage >> &getMessages() const;
        const QString &getRoomId() const;
        const QString &getName() const;
        const QMap<QString, QSharedPointer<RocketChatUser> > &getUsers() const;
        ChatMessage getYoungestMessage();
        const ChatMessage &getYoungestMessage() const;

        ChatMessage getOldestMessage();
        const ChatMessage &getOldestMessage() const;

        MessageRepository *getMessageRepo( void );
        bool getJoined() const;
        unsigned int getUnreadMessages() const;

        const QString &getType() const;
        void setType( const QString &pType );

        bool getReadOnly() const;
        void setReadOnly( bool pValue );

        const QStringList &getMuted() const;
        void setMuted( const QStringList &pValue );

        bool getArchived() const;
        void setArchived( bool pValue );

        void sendUnsentMessages( void );

        bool getSelfMuted() const;
        void setSelfMuted( bool pValue );

        void setParentRepo( ChannelRepository *pParentRepo );
        int operator >( const RocketChatChannel &channel ) const;
        int operator <( const RocketChatChannel &channel ) const;


        bool getBlocked() const;
        void setBlocked( bool blocked );

        bool getDeleted() const;
        void setDeleted( bool deleted );

        const QString &getOwnerName() const;
        void setOwnerName( const QString &pOwner );

        const QString &getOwnerId() const;
        void setOwnerId( const QString &ownerId );

        const QString &getUsername() const;
        void setUsername( const QString &username );

        const QSharedPointer<RocketChatUser> &getChatPartner() const;
        void setChatPartner( const QSharedPointer<RocketChatUser> &chatPartner );

        const QString &getChatPartnerId() const;
        void setChatPartnerId( const QString &chatPartnerId );

        void deleteMessage(const QString id);
        QStringList whiteList(const QList<QString> ids, const qint64 newest,const qint64 oldest);
        QSharedPointer<TempFile> getAvatarImg() const;
        void setAvatarImg( const QSharedPointer<TempFile> &pAvatar );

        qint64 getUpdatedAt() const;
        void setUpdatedAt( const qint64 &updatedAt );

        qint64 getCreatedAt() const;
        void setCreatedAt( const qint64 &createdAt );

    private:
        QSharedPointer<TempFile> mAvatarImg;
        QString mOwnerName;
        QString mOwnerId;
        bool mDeleted;
        bool mReadOnly = false;
        bool mArchived = false;
        bool mBlocked = false;
        QStringList mMuted;
        QString mRoomId;
        QString mName;
        QString mUsername;
        QString mChatPartnerId;
        MessageRepository mMessages;
        QMap<QString, QSharedPointer<RocketChatUser>> mUsers;
        QSharedPointer<RocketChatUser> mChatPartner;
        bool mJoined = false;
        QString mType;
        unsigned int mUnreadMessages = 0;
        bool mSelfMuted = false;
        qint64 mUpdatedAt = -1;
        qint64 mCreatedAt = -1;
    signals:
        void messageListReceived( const QString &pChannelId, const QList<ChatMessage> &pMessages );
        QList<QSharedPointer<RocketChatMessage>> messageAdded( const QString &id, qint64 timestamp );
        void unreadMessagesChanged( const QString &id, int number );
        void channelDeleted( const QString &pId, bool deleted );
        void dataChanged( const QString &id, const QString &property );
        void chatPartnerStatusChanged();
        void messageDeleted(const QString, const QString);
        void updatedChanged(const QString, qint64 updatedAt);

};

#endif // ROCKETCHATCHANNEL_H
