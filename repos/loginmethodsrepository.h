/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#ifndef LOGINMETHODSREPOSITORY_H
#define LOGINMETHODSREPOSITORY_H

#include <QObject>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include "abstractbaserepository.h"
#include "entities/loginmethod.h"
#include "utils.h"

class LoginMethodsRepository : public QObject
{
        Q_OBJECT
    public:
        explicit LoginMethodsRepository( QObject *parent = nullptr );

        void addLoginMethod( const QJsonObject &pObject, const QString &pBaseUrl );
        void addLoginMethod( const LoginMethod &pMethod );

        LoginMethod get( const QString &pMethodName );
        LoginMethod operator[]( const QString &pString );

        bool contains( const QString &pTerm );

        void clear();

    private:
        QHash<QString, LoginMethod> mData;

    signals:
        void newLoginMethod( QHash<QString, QVariant> );

    public slots:
};

#endif // LOGINMETHODSREPOSITORY_H
