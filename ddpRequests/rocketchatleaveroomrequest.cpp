#include "rocketchatleaveroomrequest.h"

RocketChatLeaveRoomRequest::RocketChatLeaveRoomRequest( const QString &pChanneId )
{
    QJsonArray params;
    params.append( pChanneId );
    buildRequest( QStringLiteral( "leaveRoom" ), params );
}
