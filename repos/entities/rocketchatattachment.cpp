/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#include "rocketchatattachment.h"

RocketChatAttachment::RocketChatAttachment( const QString &pFileUrl, const QString &pType, const QString &pTitle )
{
    this->mUrl   =    pFileUrl;
    this->mType  =    pType;
    this->mTitle =    pTitle;
}

const QString &RocketChatAttachment::getUrl() const
{
    return mUrl;
}

const QString &RocketChatAttachment::getType() const
{
    return mType;
}

const QString &RocketChatAttachment::getTitle() const
{
    return mTitle;
}

int RocketChatAttachment::getWidth() const
{
    return mWidth;
}

void RocketChatAttachment::setWidth( int pValue )
{
    mWidth = pValue;
}

int RocketChatAttachment::getHeight() const
{
    return mHeight;
}

void RocketChatAttachment::setHeight( int pValue )
{
    mHeight = pValue;
}
