#include "getserverinforequest.h"

GetServerInfoRequest::GetServerInfoRequest( const RestRequestCallback &pSuccess, const RestRequestCallback &pError ): GetRequest( RestPaths::apiServerInfo, pSuccess, pError )
{

}
