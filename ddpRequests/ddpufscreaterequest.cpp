/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include "ddpufscreaterequest.h"


DDPUfsCreateRequest::DDPUfsCreateRequest( const QString &pChannelId, const QString &pFilename, int pSize, const QString &pMime, const DdpCallback &pSuccess ): DDPUfsCreateRequest( pChannelId, pFilename, pSize, pMime )
{
    setSuccess( pSuccess );
}

DDPUfsCreateRequest::DDPUfsCreateRequest( const QString &pChannelId, const QString &pFilename, int pSize, const QString &pMime )
{
    QJsonArray params;
    QJsonObject paramObj;
    paramObj[QStringLiteral( "name" )] = pFilename;
    paramObj[QStringLiteral( "size" )] = pSize;
    paramObj[QStringLiteral( "type" )] = pMime;
    paramObj[QStringLiteral( "rid" )] = pChannelId;
    QString store = QStringLiteral( "rocketchat_uploads" );

    int major = std::get<0>( RocketChatServerConfig::serverVersion );
    int minor = std::get<1>( RocketChatServerConfig::serverVersion );
    int patch = std::get<2>( RocketChatServerConfig::serverVersion );

    if ( major != -1 && minor != -1 && patch != -1 ) {
        if ( major >= 0 && minor > 60 ) {
            store = QStringLiteral( "Uploads" );
        }
    }
    paramObj[QStringLiteral( "store" )] = store;
    params.append( paramObj );
    buildRequest( QStringLiteral( "ufsCreate" ), params );
}
