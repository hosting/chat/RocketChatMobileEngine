/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#ifndef TEMPFILE_H
#define TEMPFILE_H
#include <QString>
#include <QUrl>
#include <QFileInfo>
#include <QMimeDatabase>
#include <QMimeType>

class TempFile
{
    public:

        TempFile( const QString &path, const QString &url );

        const QString &getFilePath() const;
        void setFilePath( const QString &filePath );

        const QString &getType() const;
        void setType( const QString &value );

        const QUrl &getUrl() const;
        void setUrl( const QUrl &url );

        const QFileInfo &getFileInfo() const;

        bool getFileChecked() const;
        void setFileChecked( bool fileChecked );

    protected:
        TempFile() = default;
        bool mFileChecked = false;
        QUrl mUrl;
        QString mFileName;
        QString mFilePath;
        QFileInfo mFileInfo;
};

#endif // TEMPFILE_H
