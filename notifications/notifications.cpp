/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include "notifications.h"
#ifdef Q_OS_ANDROID
#include "notifications/android/googlepushnotifications.h"
#endif
#ifdef Q_OS_IOS
#include "notifications/ios/applepushnotifications.h"
#endif
#include "QDebug"

Notifications::Notifications()
{
#ifdef Q_OS_ANDROID
    mNotificationInstance = new GooglePushNotifications;
#endif
#ifdef Q_OS_IOS
    mNotificationInstance = new ApplePushNotifications;
#endif

    if ( mNotificationInstance != nullptr ) {
        connect( mNotificationInstance, &Notifications::tokenReceived, this, &Notifications::tokenReceived );
        connect( mNotificationInstance, &Notifications::messageReceived, this, &Notifications::messageReceived );
        connect( mNotificationInstance, &Notifications::switchChannelByName, this, &Notifications::switchChannelByName );
    }
}

void Notifications::registerWithService()
{
    if ( mNotificationInstance ) {
        qDebug() << "register with service Notifications called";
        mNotificationInstance->registerWithService();
    }
}

void Notifications::checkForPendingMessages()
{
    mNotificationInstance->checkForPendingMessages();
}
