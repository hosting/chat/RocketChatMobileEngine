/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#ifndef FILEUPLOADER_H
#define FILEUPLOADER_H

#include <QObject>
#include <QString>
#include <QByteArray>
#include <QMutexLocker>

#include "api/meteorddp.h"
#include "api/restapi.h"
#include "restRequests/restfileuploadrequest.h"
#include "ddpRequests/ddpmethodrequest.h"
#include "ddpRequests/ddpufscreaterequest.h"
#include "rocketchatserverconfig.h"

class RocketChatServerData;
class FileUploader : public QObject
{
        Q_OBJECT
    public:
        FileUploader( QObject *parent, RocketChatServerData *pServer, const QUrl &pUri, const QString &pChannelId );
        void upload( const std::function<void( void )> & );
        void cancel( void );
        QString getFileId() const;

    protected:
        void onUfsCreated( const QJsonObject &pResponse );
        void onUploadCompleted( const QJsonObject &pResponse );
        void nextRequest( void );

        QMutex mLock;
        QUrl mUri;
        MeteorDDP *mDdpApi;
        RestApi *mRestApi;
        QString mChannelId;
        QByteArray mDump;
        QMimeType mType;
        QString mToken;
        QString mFileId;
        int mSize;
        QVector<QSharedPointer<RestFileUploadRequest>> mPartialRequests;
        std::function<void( void )> mCleanUp;
        RocketChatServerData *mServer;
        bool mCanceled = 0;
        bool mErrorStatus = 0;
        double progress = 0;
        uint lastUpdate = 0;
    signals:
        void ufsCreated( const QString &mFileId );
        void progressChanged( double progress );
        void fileuploadCompleted( void );
        void error( const QString &erorrString );
    public slots:

};

#endif // FILEUPLOADER_H
