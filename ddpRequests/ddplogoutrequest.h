#ifndef DDPLOGOUTREQUEST_H
#define DDPLOGOUTREQUEST_H
#include <QJsonArray>
#include "ddpRequests/ddpmethodrequest.h"


class DdpLogoutRequest: public DDPMethodRequest
{
public:
    DdpLogoutRequest();
};

#endif // DDPLOGOUTREQUEST_H
