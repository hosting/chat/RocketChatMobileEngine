#ifndef PERSISTANCELAYER_H
#define PERSISTANCELAYER_H

#include <QSettings>
#include <QObject>
#include <QMetaObject>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QFile>
#include <QMimeDatabase>
#include <QMimeType>
#include <QJsonDocument>
#include <QMap>
#include <QJsonObject>

#include "repos/entities/rocketchatchannel.h"
#include "config.h"

class RocketChatChannel;
class RestApi;
class MeteorDDP;
class PersistanceLayer : public QObject
{
        Q_OBJECT

        Q_PROPERTY( QString username READ getUserName WRITE setUserName NOTIFY userDataChanged )
        Q_PROPERTY( QString password READ getPassword WRITE setPassword NOTIFY userDataChanged )

    public:

        PersistanceLayer( const PersistanceLayer & ) = delete;

        static PersistanceLayer *persistanceLayer;
        static PersistanceLayer *instance();

    public slots:
        void setUserData( const QString &pUser, const QString &pPass );
        void setCurrentChannel( const QString &pChannelId, const QString &pName );
        void setSetting( const QString &pProperty, const QString &pValue );
        void setUserName( const QString & );
        void setPassword( const QString & );
        void setToken( const QString &, uint );
        void addChannel( const QString &pId, const QString &pName, const QString &pType, qint64 pUpdatedAt, bool pJoined, bool pReadOnly, const QString &pMuted, bool pArchived, bool pBlocked, const QString &pUsername, const QString &pMchatPartnerId );
        void deleteChannel( const QString &pId );
        void addMessage( const QString &pId, const QString &pRid, const QString &pAuthor, qint64 pTs, const QString &pJson, const QString &pUserId );
        void addFileCacheEntry( const QString &pUrl, const QString &pPath );
        void addCustomEmoji( const QString &pTag, const QString &pPath, const QString &pHtml, const QString &pCategory );
        void addCustomEmoji( const QString &pTag, const QString &pPath, const QString &pHtml, const QString &pCategory, const QString &pUnicode );
        void addCustomEmoji( const QString &pTag, const QString &pPath, const QString &pHtml, const QString &pCategory, const QString &pUnicode, int pOrder );
        void addUserToBlockList( const QString &pUserId, const QString &pUsername );
        void close();

    public:

        void setUserId( const QString & );

        QString getUserName();
        std::tuple<QString, QString> getCurrentChannel( void );
        QString getPassword();
        QPair<QString, uint> getToken();
        QString getUserId();
        QList<QVariantHash> getChannels( void );
        QHash<QString, QString> getMessageByid( const QString &pId );
        QList<QJsonObject> getMessagesByRid( const QString &pRid );
        QList<QJsonObject> getMessagesByRid( const QString &pRid, qint64 pFrom, qint64 pTo );
        QList<QJsonObject> getMessagesByRid( const QString &pRid, qint64 pFrom, int pLimit );
        QList<QPair<QString, QString >> getListOfBlockedUsers();

        QList<QHash<QString, QString > > getCustomEmojis();
        Q_INVOKABLE QString getNewVideoPath( void );
        Q_INVOKABLE QString getNewImagePath( void );

        QString getFileCacheEntry( const QString &pUrl );
        QList< QHash<QString, QString>> getFiles();

        Q_INVOKABLE QString getSetting( const QString &pProperty );

        void removeFileCacheEntry( const QString &pUrl );
        void removeFileCacheEntry( const QString &pUrl, const QString &pPath );

        void transaction( void );
        void askForcommit( void );
        void wipeDb( void );

        bool transactionActive = false;

        ~PersistanceLayer();

        void init();
        bool mDbReady = false;
        void deleteMessage(const QString id);

    private:
        void commit( void );
        PersistanceLayer() {}
        QSqlDatabase mDb{ QSqlDatabase::addDatabase( "QSQLITE" )};
        QMimeDatabase mMimeDb;
        QTimer commitTimer;
        QMutex commitMutex;
        void initShema();
        void initQueries();
        void upgradeSchema();

        int mCommitCounter = 0;
        int mTransactionCounter = 0;


        QSqlQuery querySetUsername;
        QSqlQuery querySetPassword;
        QSqlQuery querySetToken;
        QSqlQuery querySetUser;
        QSqlQuery querySetUserId;
        QSqlQuery querySetChannel;
        QSqlQuery querySetSetting;
        QSqlQuery queryAddChannel;
        QSqlQuery queryDeleteChannel;
        QSqlQuery queryDeleteMessagesFromChannel;
        QSqlQuery queryAddMessage;
        QSqlQuery queryAddCacheEntry;
        QSqlQuery queryAddEmoji;
        QSqlQuery queryAddUser;
        QSqlQuery queryGetMessageByRid;
        QSqlQuery queryGetName;
        QSqlQuery queryGetCurrentChannel;
        QSqlQuery queryGetPass;
        QSqlQuery queryGetToken;
        QSqlQuery queryGetUserId;
        QSqlQuery queryGetMessagesByRid;
        QSqlQuery queryGetMessageByRidAndRange;
        QSqlQuery queryGetMessagesByRidLimit;
        QSqlQuery queryGetBlockedUsers;
        QSqlQuery queryGetChannels;
        QSqlQuery queryGetEmojies;
        QSqlQuery queryGetFileCacheEntry;
        QSqlQuery queryGetSetting;
        QSqlQuery queryRemoveFileCacheEntry;



    signals:
        void userDataChanged();
        void ready();
};

#endif // PERSISTANCELAYER_H
