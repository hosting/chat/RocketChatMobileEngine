#ifndef EMOJISMODEL_H
#define EMOJISMODEL_H

#include <QAbstractListModel>
#include <QSet>
#include <QMap>
#include <QSharedPointer>

#include <repos/entities/emoji.h>



class EmojisModel: public QAbstractListModel
{
        Q_OBJECT
        Q_PROPERTY( QString currentCategory READ currentCategory WRITE setCurrentCategory NOTIFY currentCategoryChanged )
    public:
        enum class EmojiRoles : int {
            emojiText = Qt::UserRole,
            image,
            category,
            order,
            custom
        };

        EmojisModel();
        int rowCount( const QModelIndex &parent = QModelIndex() ) const;
        QVariant data( const QModelIndex &index, int role = Qt::DisplayRole ) const;
        QString currentCategory() const;
        void setCurrentCategory( const QString &currentCategory );

    public slots:
        void addEmoji( const QSharedPointer<Emoji> &pEmoji );
        void addEmojisByCategory( const QString &pCategory, const QList<QSharedPointer<Emoji>> &pList );
        void clear();
    protected:
        QHash<int, QByteArray> roleNames() const;
        QString mCurrentCategory;
        QSet<QString> mDuplicateCheck;
        QMap<QString, QList<QSharedPointer<Emoji>>> mData;
    signals:
        void currentCategoryChanged( void );

};

#endif // EMOJISMODEL_H
