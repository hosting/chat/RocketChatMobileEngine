/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#ifndef METEORDDP_H
#define METEORDDP_H

#include <QString>
#include <QHash>
#include <QUrl>
#include <QHash>
#include <QtWebSockets/QtWebSockets>
#include "ddpRequests/ddprequest.h"
#include "messagelistener.h"


/**
 * @brief The MeteorDDP class
 *
 *
 */
class MeteorDDP : public QObject
{
        Q_OBJECT
    public:
        MeteorDDP( QObject *parent, const QString &pUri, bool pUnsecure = false );
        void connectWithServer();
        // ~MeteorDDP();
        QString getToken() const;
        void setToken( const QString &pValue );
        QString getFrameCount();
        bool getWebsocketConnectionEstablished() const;
        void sendRequest( const QSharedPointer<DDPRequest> &pRequest );
        void resume( void );
        void unsetResponseBinding( const QString &pId );
        bool websocketIsValid( void );
        uint diffToLastMessage( void );

        void disconnectFromServer( void );

        void setResumeToken( const QString &pToken );
        void setUserId( const QString &pUserId );

        void registerMessageHandler( MessageListener * );
        void registerMessageHandler( MessageListener & );
        void init( const QString &pServer );


        bool getUnsecure() const;

    private:
        QJsonParseError *mError;

        void pong();
        void genUri();
        void loadLocale();
        void userPresenceOnline();
        void sendJson( const QJsonObject & );
        bool unsecure;

        int frameCount = 0;

        QString mServer;
        QUrl wsUri;
        QString mUid;
        QString mToken;

        void onTextMessageReceived( const QString & );
        void onConnected();
        void loginServiceConfiguration();

        QString mWebsocketUri;
        bool mWebsocketConnectionEstablished = false;
        QHash<QString, QSharedPointer<DDPRequest>> mResponseBinding;
        uint mLastPing = 0;
        uint mMaxTimeout = 0;
        QSet<QString> mReceivedEvents;

        QWebSocket mWebsocket;


    signals:
        void websocketConnected();
        void ddpConnected();
        void ddpDisconnected();
        void ddpLoginConfigured();
        void messageReceived( QJsonObject pMessage );
        void sendMessageSignal( QJsonObject );
};

#endif // METEORDDP_H
