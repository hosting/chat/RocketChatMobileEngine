/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include "ddpsubscriptionrequest.h"
#include "../rocketchatserverconfig.h"

DDPSubscriptionRequest::DDPSubscriptionRequest( const QString &pCollection, const QJsonArray &pParams )
{
    buildRequest( pCollection, pParams );
}

DDPSubscriptionRequest::DDPSubscriptionRequest( const QString &pCollection, const QJsonArray &pParams, const DdpCallback &pSuccess ): DDPSubscriptionRequest( pCollection, pParams )
{
    setSuccess( pSuccess );
}

void DDPSubscriptionRequest::buildRequest( const QString &pCollection, const QJsonArray &pParams )
{
    QJsonArray params = pParams;
    QJsonArray args;
    QJsonObject obj;
    obj[QStringLiteral( "useCollection" )] = false;
    obj[QStringLiteral( "args" )] = args;
    int major = std::get<0>( RocketChatServerConfig::serverVersion );
    int minor = std::get<1>( RocketChatServerConfig::serverVersion );
    int patch = std::get<2>( RocketChatServerConfig::serverVersion );

    if ( major != -1 && minor != -1 && patch != -1 ) {
        if ( major >= 0 && minor > 60 ) {
            params.append( obj );
        }
    }

    QJsonObject request;
    request[QStringLiteral( "name" )] = pCollection;
    request[QStringLiteral( "msg" )] = QStringLiteral( "sub" );
    request[QStringLiteral( "params" )] = params;
    mRawRequest = request;
}
