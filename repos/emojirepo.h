/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#ifndef EMOJIREPO_H
#define EMOJIREPO_H

#include <QObject>
#include <QMap>
#include "abstractbaserepository.h"
#include "repos/entities/emoji.h"

#include "utils.h"

class EmojiRepo : public QObject, public AbstractBaseRepository<Emoji>
{
    public:
        explicit EmojiRepo( QObject *parent = nullptr );
        bool add( const QSharedPointer<Emoji> &pEmoji );
        bool add( const QString &pId, const QSharedPointer<Emoji> &pEmoji );
    private:
        int mHighestOrder = 0;
};

#endif // EMOJIREPO_H
