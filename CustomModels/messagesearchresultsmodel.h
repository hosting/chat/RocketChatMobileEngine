#ifndef MESSAGESEARCHRESULTSMODEL_H
#define MESSAGESEARCHRESULTSMODEL_H

#include <QAbstractListModel>
#include <QSharedPointer>

#include "repos/entities/rocketchatchannel.h"

typedef QList<QSharedPointer<RocketChatMessage>> MessageList;

class MessageSearchResultsModel : public QAbstractListModel
{
        Q_OBJECT
    public:
        enum MessageRoles : int {
            text = Qt::UserRole + 1,
            type,
            linkurl,
            author,
            date,
            time,
            ownMessage,
            width,
            height,
            formattedDate,
            formattedTime,
            messageType,
            authorId,
            blocked,
            id,
            rid,
            timestamp
        };
        MessageSearchResultsModel() = default;

        Q_INVOKABLE void clear();

        int rowCount( const QModelIndex &parent ) const;
        QVariant data( const QModelIndex &index, int role ) const;

        QHash<int, QByteArray> roleNames() const;

        Q_INVOKABLE void setSearchResults( const MessageList &pResults );

        Q_INVOKABLE int getPositionOfMessage( const QString &pId );

    private:
        QList<QSharedPointer<RocketChatMessage>> mElements;
};

#endif // MESSAGESEARCHRESULTSMODEL_H
