/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#pragma once

#ifndef ROCKETCHATCHANNELFACTORY_H
#define ROCKETCHATCHANNELFACTORY_H
#include <QVector>
#include <QSharedPointer>
#include <QObject>
#include <QSqlQuery>
#include <QSqlRecord>

#include "api/meteorddp.h"
#include "api/restapi.h"
#include "restRequests/restgetjoinedroomsrequest.h"
#include "restRequests/restgetjoinedgroupsrequest.h"
#include "restRequests/restrequest.h"
#include "rocketchatserver.h"
#include "ddpRequests/rocketchatgetroomsrequest.h"
#include "ddpRequests/rocketchatleaveroomrequest.h"
#include "ddpRequests/rocketchathideroomrequest.h"
#include "restRequests/restspotlightrequest.h"
#include "services/messageservice.h"
#include "repos/channelrepository.h"
#include "fileservice.h"

#include "CustomModels/models.h"

typedef QList<QSharedPointer<RocketChatChannel>> channelVector;

typedef QVector<QSharedPointer<RocketChatUser>> userVector;

class PersistanceLayer;
class RocketChatServerData;
class MessageService;
class ChannelRepository;
class RocketChatChannelService : public QObject
{
        Q_OBJECT
    public:
        RocketChatChannelService( QObject *parent, RocketChatServerData *pServer, MessageService *pMessageService, FileService *pFileService );

        RestApi *getRestApi() const;
        void setRestApi( RestApi *pValue );

        MeteorDDP *getDdp() const;
        void setDdp( MeteorDDP *ddp );

        QSharedPointer<RocketChatChannel> createChannelObject( const QString &pRoomId, const QString &pName, const QString &pType, bool insertIntoRepo = true );
        QSharedPointer<RocketChatChannel> createChannelObject( const QString &pRoomId, const QString &pName, const QString &pType, const QString &username, bool insertIntoRepo = true );

        void openPrivateChannelWith( const QString &pUsername );

        void loadJoinedChannelsFromServer( void );
        void loadJoinedChannelsFromDb( void );
        void persistChannel( const QSharedPointer<RocketChatChannel> &pChannel );
        void persist();
        void loadUsersOfChannel( const QSharedPointer<RocketChatChannel> &pChannel );
        void subscribeChannel( const QSharedPointer<RocketChatChannel> &pChannel );
        void subscribeChannel( const QString &pChannel );

        void fillChannelWithMessages( const QSharedPointer<RocketChatChannel> &pChannel );

        ChannelRepository *getChannels() const;
        void setChannels( ChannelRepository *pChannels );

        void leaveChannel( const QString &pId );

        void deleteChannel( const QString &pId );

        void hideRoom( const QString &pId );

        void reportContent( const QString &pMessageId, const QString &pAuthorId );

        void searchRoom( const QString &pTerm, const QString &pType );

        channelVector searchRoomLocal( const QString &pTerm, const QHash<QString, bool> &pFilter, const QString &pType, const QHash<QString, bool> &pNames );

        ~RocketChatChannelService() = default;




    protected:
        PersistanceLayer *mStorage = nullptr;
        RocketChatServerData *mServer = nullptr;
        ChannelRepository *mChannels = nullptr;
        FileService *mFileService = nullptr;
        MeteorDDP *mDdp;

        QList<QSharedPointer<RocketChatChannel>> processChannelData( const QJsonArray &pChannelArray, bool pJoined, bool pUpdateOnly );

        MessageService *mMessageService;
    signals:
        void channelsLoaded( const channelVector &pChannelList, bool pJoined );
        void usersLoaded( const QString &roomId, const userVector &pUserList );
        void directChannelReady( const QString &roomId, const QString &type );
};

#endif // ROCKETCHATCHANNELFACTORY_H
