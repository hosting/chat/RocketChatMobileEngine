/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#ifndef ROCKETCHATSERVER_H
#define ROCKETCHATSERVER_H

#include <QObject>
#include <QVariantList>
#include <algorithm>    // std::transform
#include <QVariant>
#include <QList>
#include <QString>
#include <QMimeType>
#include <QCryptographicHash>
#include <QJsonArray>
#include "utils.h"
#include "persistancelayer.h"
#include "api/meteorddp.h"
#include "api/restapi.h"
#include "repos/userrepository.h"
#include "repos/channelrepository.h"
#include "repos/loginmethodsrepository.h"
#include "repos/entities/loginmethod.h"
#include "ddpRequests/pingrequest.h"
#include "ddpRequests/ddploginrequest.h"
#include "ddpRequests/rocketchatsubscribechannelrequest.h"
#include "ddpRequests/ddpufscreaterequest.h"
#include "ddpRequests/rocketchatsubscribeusernotify.h"
#include "ddpRequests/rocketchatnotifynoticesrequest.h"
#include "ddpRequests/rocketchatsubscriptionchangedrequest.h"
#include "ddpRequests/rocketchatmarkchannelasreadrequest.h"
#include "ddpRequests/rocketchatcreatepublicgrouprequest.h"
#include "ddpRequests/ddpunsubscriptionrequest.h"
#include "ddpRequests/rocketchatcreateprivategrouprequest.h"
#include "ddpRequests/rocketchatadduserstochannel.h"
#include "ddpRequests/rocketchatsubscriberoomrequest.h"
#include "ddpRequests/rocketchatupdatepushtokenrequest.h"
#include "ddpRequests/ddplogoutrequest.h"
#include "ddpRequests/rocketchatsubscribeactiveusers.h"
#include "ddpRequests/rocketchatsubscribeuserdata.h"
#include "ddpRequests/rocketchatchangeuserpresencedefaultstatus.h"
#include "ddpRequests/rocketchatchangeuserpresencestatus.h"
#include "ddpRequests/rocketchatupdatejitsitimeout.h"
#include "ddpRequests/rocketchatsubscribeloginmethods.h"
#include "ddpRequests/ddpopenidloginrequest.h"
#include "ddpRequests/ddpsamlloginrequest.h"
#include "ddpRequests/rocketchatblockuserrequest.h"
#include "ddpRequests/rocketchatunblockuserrequest.h"
#include "ddpRequests/rocketchatsubscriberoomschanged.h"
#include "ddpRequests/rocketchatmessagesearchrequest.h"
#include "ddpRequests/rocketchatgetusernamesuggestion.h"
#include "ddpRequests/rocketchatsetusername.h"
#include "restRequests/restrequest.h"
#include "restRequests/restlogoutrequest.h"
#include "restRequests/getserverinforequest.h"
#include "fileuploader.h"
#include "notifications/notifications.h"
#include "services/rocketchatchannelservice.h"
#include "services/messageservice.h"
#include "services/requests/loadhistoryservicerequest.h"
#include "services/fileservice.h"
#include "repos/emojirepo.h"
#include "repos/filesrepo.h"
#include "rocketchatserverconfig.h"
#include "repos/channelrepository.h"
#include "services/emojiservice.h"
#include "restRequests/getrequest.h"
#include "segfaulthandler.h"
#include "CustomModels/usermodel.h"
#include "CustomModels/loginmethodsmodel.h"
#include "restRequests/restmerequest.h"
#include "ddpRequests/rocketchatcreateaccount.h"
#include "ddpRequests/rocketchatgetroombyid.h"
#include "ddpRequests/rocketchatgetroombynameandtype.h"
#include "ddpRequests/rocketchatgetroomidbynameorid.h"
#include "ddpRequests/rocketchatspotlightrequest.h"

class RocketChatChannelService;
class ChannelRepository;
class FileUploader;
class PersistanceLayer;
typedef QSharedPointer<RestRequest> RestApiRequest;
class MessageService;
class EmojiService;
class DdpLogoutRequest;
class FileService;
class EmojiRepo;
class ChannelModel;

enum class ConnectionState {
    OFFLINE,
    CONNECTED,
    ONLINE
};

class RocketChatServerData : public MessageListener
{

        Q_OBJECT
    public:
        RocketChatServerData( QString pId, QString pBaseUrl,
                              bool pUnsecure = false );

        ~RocketChatServerData();
        void setServerId( const QString &pValue );

        void persistData();
    public slots:

        void init();
        void initDb();
        void initConnections();

        void createPublicGroup( const QString &pChannelName, const QStringList &pUsers, bool pReadonly );
        void createPrivateGroup( const QString &pChannelName, const QStringList &pUsers, bool pReadonly );
        void addUsersToChannel( const QString &pChannelId, const QStringList &pUsers );
        void sendMessage( const QString &pMessage, const QString &pChannelId );

        void uploadFile( const QString &pChannelId, const QString &pFile );
        void cancelUpload( const QString &pFileId );

        void login( const QString &pUsername, const QString &pPassword );
        void loginWithHash( const QString &pUsername, const QString &pHash );
        void loginWithToken( const QString &pUsername, const QString &pToken, bool pResume = false );
        void loginWithOpenIDToken( const QString &pToken, const QString &pSecret );
        void loginWithMethod( const QString &method, const QString &payload );
        void loginWtihSamlToken( const QString &pToken );

        void joinChannel( const QString &pChannelId, bool pForce = false );
        void joinChannelByNameAndType( const QString &pChannelName, const QString &pType );

        void loadRecentHistory( const QString &pChannelId );

        void loadHistoryTill( const QString &pChannelId, qint64 pTs );


        void getFileRessource( const QString &pUrl );
        void getFileRessource( const QString &pUrl, const QString &pType );

        void getLoginMethods();

        void getServerInfo();

        void logout( void );

        void setConnectionState( const ConnectionState &pValue );

        void resume( void );

        void joinJitsiCall( const QString &pRoomId );

        void setUnreadMessages( const QString &pChannelId, int count );

        void openPrivateChannelWith( const QString &pUsername );

        void setCurrentChannel( const QString &value );

        void markChannelAsRead( const QString &pChannelId );

        void sendPushToken( const QString &pToken );

        void switchChannel( const QString &pServer, const QString &pRid, const QString &pName, const QString &pType );

        void switchChannelByName( const QString &pRid, const QString &pType );

        void requestGetChannelDetails( const QString &pChannelId );

        void requestIsLoggedIn();

        void setUserPresenceDefaultStatus( int pStatus );

        void setUserPresenceStatus( int pStatus );

        void onStateChanged( const Qt::ApplicationState &pState );

        void createVideoCall( const QString &pRid );

        void disconnectFromServer();

        void searchForRoomIdByName( const QString &pName );

        void blockUser( const QString &pChannelId );

        void unBlockUser( const QString &pChannelId );

        void leaveChannel( const QString &pId );

        void hideChannel( const QString &pId );

        void reportAbusiveContent( const QString &pMessageId, const QString &pAuthor );

        void searchMessage( const QString &pTerm, const QString &pRoom );

        void searchRoom( const QString &pTerm, const QString &pType );

        void requestNewVideoPath();

        void setSetting( const QString &pKey, const QString &pValue );

        void getSetting( const QString &pKey );
        void deleteMessage( QString rid, QString id );
        void reconnect();

    public:
        QString getServerId() const;
        bool isConnected() const;
        bool isWebsocketValid( void ) const;
        void getCustomEmojis();
        RestApi *getRestApi() const;
        ChannelRepository *getChannels() const;
        FilesRepo *getFiles() const;
        RocketChatChannelService *mChannelService = nullptr;
        void handleChannelMessage( const QJsonObject &pMessage );
        uint diffToLastDDPPing();
        void sendUnsentMessages( void );
        void sendApiRequest( const RestApiRequest &pRequest, bool pRetry = true );
        void sendDdprequest( const QSharedPointer<DDPRequest> &pRequest, bool pRetry = true, bool pSendWhenConnected = false );
        bool initialised = 0;
        ConnectionState getConnectionState() const;
        QString getCurrentChannel() const;
        QString getUsername() const;
        void setUsername( const QString &pUsername );
        QString getUserId() const;
        void setUserId( const QString &userId );
        EmojiRepo *getEmojiRepo() const;
        void setEmojiRepo( EmojiRepo *emojiRepo );
        void createAccount( const QString &username, const QString &email, const QString &password );
        void offlineLogin();
        void wipeDbAndReconnect();
    protected:

        RocketChatServerData();
        ConnectionState mConnectionState = ConnectionState::OFFLINE;
        QString mUsername = "";
        QString mUserId = "";
        QString mBaseUrl = "";
        QString mApiUri = "";
        QString mServerId = "";
        uint mTokenExpire = 0;
        QString mResumeToken;
        bool mUnsecureConnection = false;
        QSharedPointer<RocketChatUser> mOwnUser;

        MeteorDDP *mDdpApi = nullptr;
        RestApi *mRestApi = nullptr;
        UserRepository mUsers;
        LoginMethodsRepository mLoginMethodRepo;
        ChannelRepository *mChannels = nullptr;
        EmojiRepo *mEmojiRepo = nullptr;
        FilesRepo *mFilesRepo = nullptr;
        RocketChatServerConfig config;
        bool mConnected = false;
        bool mLoggedIn = false;
        bool mResumeOperation = false;
        MessageService *mMessageService = nullptr;
        PersistanceLayer *mStorage = nullptr;
        EmojiService *mEmojiService = nullptr;
        FileService *mFileService = nullptr;
        QLinkedList<QSharedPointer<DDPRequest>> mUnsendDdpRequests;
        QLinkedList<QSharedPointer<DDPRequest>> mUnsendDdpRequestsOnConnected;
        QLinkedList<RestApiRequest> mUnsendRestRequests;
        QMap<QString, FileUploader *> mFileUploads;
        QString mCurrentChannel;
        QMutex mUnsentMutex;
        UserModel *userModel = nullptr;
        LoginMethodsModel *loginMethodsModel = nullptr;
        ChannelModel *channelsModel = nullptr;
        ChannelModel *directModel = nullptr;
        ChannelModel *groupsModel = nullptr;
        std::function<void ( QMultiMap<QString, QSharedPointer<RocketChatMessage>> *messages )> historyLoaded;
        QString mPendingSwitchRoomRequest;
        QString mPendingSwitchRoomRequestType;

        bool mCustomEmojisReady = false;

        void getServerSettings( void );
        void onDDPConnected();
        void onDDPDisonnected();

        void onDDPAuthenticated();
        void onDDPMessageReceived( const QJsonObject &pMessage );
        void onChannelsLoaded( const QList<QSharedPointer<RocketChatChannel> > &pChannels );
        void onLoggedIn();
        bool isStreamRoomMessage( const QJsonObject &pMessage ) const;
        bool isUserJoinMessage( const QJsonObject &pMessage ) const;
        void handleStreamRoomMessage( const QJsonObject &pMessage );
        void handleUserJoinMessage( const QJsonObject &pMessage );
        void onMessageReceived( const QString &pServerId, const QString &pChannelId, const QString &pMessage );
        void onLoginError();
        void onResume();
        void setRestApi( RestApi * );

        void loadEmojis();
        void loadHistories( void );
        void onUsersLoaded( const QString &pChannelId, const QVector<QSharedPointer<RocketChatUser>> &pUserList );

        void onUnreadCountChanged();

    signals:
        void ddpConnected( const QString &pServerId );
        void loggedIn( const QString &pServer );
        void onHashLoggedIn( const QString &pServer );
        void loginError( void );
        void loggedOut( const QString &pServerId );
        void fileUploadFinished( const QString &pData );
        void fileuploadStarted( const QString &pData );
        void fileUploadProgressChanged( const QString &pData );
        void error( const QString &pError );
        void fileRessourceProcessed( const QString &pUrl, const QString &pPath, bool pShowInline );
        void customEmojisReceived( QVariantList pEmojis );
        void channelSwitchRequest( QSharedPointer<RocketChatChannel> pChannel );
        void openUrl( const QString &url );
        void readyToCheckForPendingNotification();
        void registerForPush();
        void allChannelsReady( const QVariantList &channels );
        void channelDetailsReady( QVariantMap details, const QString &channelId );
        void unreadCountChanged( const QString &pServerId, uint pUnread );
        void newLoginMethod( const QMap<QString, QVariant> &entry );
        void resetLoginMethods();
        void offlineMode( void );
        void loggingIn();
        void userStatusChanged( int status );
        void videoPath( const QString &path );
        void setting( const QString &pKey, const QString &pValue );
        void offline( void );

        // MessageListener interface
    public:
        bool handlesMessage( const QJsonObject &object ) override;
        QString getBaseUrl() const;
        QSharedPointer<RocketChatUser> getOwnUser() const;
        void setOwnUser( const QSharedPointer<RocketChatUser> &ownUser );
};

#endif // ROCKETCHATSERVER_H
