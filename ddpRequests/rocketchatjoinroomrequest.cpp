#include "rocketchatjoinroomrequest.h"

RocketChatJoinRoomRequest::RocketChatJoinRoomRequest( const QString &pRoomId )
{
    QJsonArray params = {pRoomId, QJsonValue::Null};
    buildRequest( QStringLiteral( "joinRoom" ), params );
}

RocketChatJoinRoomRequest::RocketChatJoinRoomRequest( const QString &pRoomId, const DdpCallback &pSuccess ): RocketChatJoinRoomRequest( pRoomId )
{
    setSuccess( pSuccess );
}
