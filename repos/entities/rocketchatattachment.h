/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#ifndef ROCKETCHATATTACHMENT_H
#define ROCKETCHATATTACHMENT_H
#include <QString>
#include <QObject>

class RocketChatAttachment
{
    public:
        RocketChatAttachment( const QString &pFileUrl, const QString &pType, const QString &pTitle );

        const QString &getUrl() const;
        const QString &getType() const;
        const QString &getTitle() const;

        int getWidth() const;
        void setWidth( int pValue );

        int getHeight() const;
        void setHeight( int pValue );

    protected:
        QString mUrl;
        QString mType;
        QString mTitle;
        int mWidth = -1;
        int mHeight = -1;
};

#endif // ROCKETCHATATTACHMENT_H
