/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include "restrequest.h"

RestRequest::RestRequest( QByteArray pData, QString pPath, RestRequest::methods pMethod, RestRequestCallback pSuccess ):
    mContent( std::move( pData ) ), mPath( std::move( pPath ) ), mMethod( pMethod ), mSuccess( std::move( pSuccess ) )
{
}

RestRequest::RestRequest( QByteArray pData, QString pPath, RestRequest::methods pMethod, RestRequestCallback pSuccess, RestRequestCallback pError ):
    mContent( std::move( pData ) ), mPath( std::move( pPath ) ), mMethod( pMethod ), mSuccess( std::move( pSuccess ) ), mError( std::move( pError ) )
{
}

RestRequest::methods RestRequest::getMethod() const
{
    return mMethod;
}

void RestRequest::setMethod( const methods &pValue )
{
    mMethod = pValue;
}

QString RestRequest::getPath() const
{
    return mPath;
}

void RestRequest::setPath( const QString &pValue )
{
    mPath = pValue;
}

QByteArray RestRequest::getContent() const
{
    return mContent;
}

void RestRequest::setContent( const QByteArray &pValue )
{
    mContent = pValue;
}

RestRequestCallback RestRequest::getSuccess() const
{
    return mSuccess;
}

void RestRequest::setSuccess( const RestRequestCallback &pValue )
{
    mSuccess = pValue;
}

RestRequestCallback RestRequest::getError() const
{
    return mError;
}

void RestRequest::setError( const RestRequestCallback &pValue )
{
    mError = pValue;
}

bool RestRequest::getAbsolutePath() const
{
    return mAbsolutePath;
}

QString RestRequest::getMimeType() const
{
    return mMimeType;
}

void RestRequest::setMimeType( const QString &pValue )
{
    mMimeType = pValue;
}

bool RestRequest::isRawData()
{
    return mRawData;
}
