/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#ifndef SORTEDVECTOR_H
#define SORTEDVECTOR_H

#include <QVector>
#include <QSharedPointer>
#include <QMutex>

struct SortedVectorProperties {
    enum class sortOrder {
        asc,
        dsc
    };
};
template<typename T> class SortedVector : public QList<QSharedPointer<T>>
{
    public:

        int insertSort( const QSharedPointer<T> & );
        int findInsertPosition( const QSharedPointer<T> & ) const;
        int findPosition( const QSharedPointer<T> & ) const;
        std::tuple<int, int> reOrder();
        bool operator =( const QVector<QSharedPointer<T>> &o )
        {
            this->clear();
            this->append( o );
            return true;
        }
    private:
        mutable QMutex mSortMutex;
};
#include "sortedvector.cpp"
#endif // SORTEDVECTOR_H
