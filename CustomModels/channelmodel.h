/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#ifndef CHANNELMODEL_H
#define CHANNELMODEL_H

#include <QAbstractListModel>
#include <QSharedPointer>
#include <QMultiMap>
#include <QSet>
#include <QDebug>
#include <QString>
#include <QTimer>

#include "repos/entities/rocketchatchannel.h"
#include "container/sortedvector.h"

class RocketChatChannel;

class ChannelModel : public QAbstractListModel
{
        Q_OBJECT
        enum class ChannelRoles : int {
            name = Qt::UserRole + 1,
            roomId,
            unreadMessages,
            lastMessageText,
            channelType,
            readonly,
            archived,
            blocked,
            deleted,
            ownerId,
            ownerName,
            username,
            userStatus,
            avatarImg
        };
    public:
        ChannelModel() = default;
        int rowCount( const QModelIndex &parent = QModelIndex() ) const;
        QVariant data( const QModelIndex &index, int role = Qt::DisplayRole ) const;
        bool addChannel( const QSharedPointer<RocketChatChannel> & );
        QString getType() const;
        void setType( const QString &value );
        void sortChanged( void );
        void clear( void );
    private:
        SortedVector<RocketChatChannel> channelList;
        QSet<QString> duplicateCheck;
        QHash<int, QByteArray> roleNames() const;
        QString type;
        void onNewerMessage( const QString &id, qint64 timestamp );
        void onUnreadMessageChanged( const QString &id, int number );
        void onDataChanged( const QString &id, const QString &property );
        void onDeleted( const QString &pId, bool deleted );
        void onChannelOrderChanged( const QString &pId, qint64 pTimestamp );

        QTimer timer;
    signals:
        void countChanged( void );
        void unreadMessagesChanged( void );

    public slots:
        void addChannelSlot( const QSharedPointer<RocketChatChannel> & );
        bool addChannelsSlot( const QList<QSharedPointer<RocketChatChannel>> & );

};

#endif // CHANNELMODEL_H
