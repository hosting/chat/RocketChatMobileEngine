#ifndef ROCKETCHATGETROOMIDBYNAMEORID_H
#define ROCKETCHATGETROOMIDBYNAMEORID_H

#include "ddpmethodrequest.h"

class RocketChatGetRoomIdByNameOrId: public DDPMethodRequest
{
    public:
        explicit RocketChatGetRoomIdByNameOrId( const QString &pName );
        RocketChatGetRoomIdByNameOrId( const QString &pName, const DdpCallback &success );

};

#endif // ROCKETCHATGETROOMIDBYNAMEORID_H
