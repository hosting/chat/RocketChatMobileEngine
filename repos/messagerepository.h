/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#ifndef MESSAGEREPOSITORY_H
#define MESSAGEREPOSITORY_H
#include <QVector>
#include <QList>
#include <algorithm>
#include <QJsonObject>
#include "abstractbaserepository.h"
#include "repos/entities/rocketchatmessage.h"
#include "utils.h"

class RocketChatMessage;
typedef QSharedPointer<RocketChatMessage> ChatMessage;
class MessageRepository : public QObject, public AbstractBaseRepository<RocketChatMessage>
{
        Q_OBJECT
    public:
        explicit MessageRepository( QObject *parent = nullptr );
        ChatMessage youngest();
        const ChatMessage &youngest() const;
        ChatMessage oldest();
        const ChatMessage &oldest() const;
        void remove(QString id);
        QVector<QSharedPointer<RocketChatMessage> > messagesAsObjects();
        bool add( const QString &pId, const ChatMessage & );
        const QMap<qint64, QSharedPointer<RocketChatMessage> > &timestampIndex() const;
        void setTimestampIndex( const QMap<qint64, QSharedPointer<RocketChatMessage> > &timestampIndex );

    protected:
        QMap<qint64, QSharedPointer<RocketChatMessage> > mTimestampIndex;
        qint64 oldestReceivedMessageFromServer = 0;

};

#endif // MESSAGEREPOSITORY_H
