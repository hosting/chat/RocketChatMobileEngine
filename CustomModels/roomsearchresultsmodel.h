#ifndef ROOMSEARCHRESULTSMODEL_H
#define ROOMSEARCHRESULTSMODEL_H

#include <QAbstractListModel>

#include "repos/entities/rocketchatchannel.h"
#include "container/sortedvector.h"

typedef QList<QSharedPointer<RocketChatChannel>> channelVector;

class RoomSearchResultsModel: public QAbstractListModel
{
        Q_OBJECT
    public:

        enum class ChannelRoles : int {
            name = Qt::UserRole + 1,
            roomId,
            unreadMessages,
            channelType,
            readonly,
            archived,
            blocked,
            deleted,
            ownerId,
            ownerName,
            username
        };

        RoomSearchResultsModel() = default;

        // QAbstractItemModel interface
    public:
        int rowCount( const QModelIndex &parent ) const;
        QVariant data( const QModelIndex &index, int role ) const;
        QHash<int, QByteArray> roleNames() const;
        Q_INVOKABLE void clear();
        Q_INVOKABLE void addChannels( const channelVector &pChannels );

    protected:
        SortedVector<RocketChatChannel> channelList;
};

#endif // ROOMSEARCHRESULTSMODEL_H
