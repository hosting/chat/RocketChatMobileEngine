/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#ifndef NOTIFICATIONABSTRACT_H
#define NOTIFICATIONABSTRACT_H

#include <QObject>

class NotificationAbstract : public QObject
{
        Q_OBJECT
    public:
        NotificationAbstract();

        virtual void registerWithService() = 0;



        QString getToken() const;
        void setToken( const QString &pValue );

        virtual void checkForPendingMessages() = 0;

    protected:
        QString mToken;

    signals:
        void tokenReceived( QString pToken );
        void messageReceived( QString pServer, QString pRid, QString pName, QString pType );
        void switchChannelByName( QString pChannel );

};

#endif // NOTIFICATIONABSTRACT_H
