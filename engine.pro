TEMPLATE = lib 
QT += core network websockets sql concurrent
CONFIG += c++14 static

SOURCES +=  api/meteorddp.cpp \
    api/restapi.cpp \
    ddpRequests/ddploginrequest.cpp \
    ddpRequests/ddprequest.cpp \
    ddpRequests/ddpsubscriptionrequest.cpp \
    ddpRequests/methodrequest.cpp \
    ddpRequests/pingrequest.cpp \
    ddpRequests/rocketchatsubscribechannelrequest.cpp \
    ddpRequests/rocketchatsubscribeusernotify.cpp \
    ddpRequests/rocketchatnotifynoticesrequest.cpp \
    ddpRequests/rocketchatsubscriptionchangedrequest.cpp \
    ddpRequests/rocketchatmarkchannelasreadrequest.cpp \
    ddpRequests/ddpunsubscriptionrequest.cpp \
    ddpRequests/rocketchatcreateprivategrouprequest.cpp \
    ddpRequests/rocketchatadduserstochannel.cpp \
    ddpRequests/ddpufscreaterequest.cpp \
    ddpRequests/rocketchatupdatepushtokenrequest.cpp \
    ddpRequests/rocketchatgetroomsrequest.cpp \
    ddpRequests/rocketchatmessagesearchrequest.cpp \
    ddpRequests/rocketchatsubscribeactiveusers.cpp \
    ddpRequests/rocketchatsubscriberoomrequest.cpp \
    ddpRequests/ddpsamlloginrequest.cpp \
    ddpRequests/rocketchatupdatejitsitimeout.cpp \
    ddpRequests/rocketchatcreateaccount.cpp \
    ddpRequests/rocketchatsubscribeloginmethods.cpp \
    ddpRequests/ddpopenidloginrequest.cpp \
    repos/abstractbaserepository.cpp \
    repos/messagerepository.cpp \
    repos/channelrepository.cpp \
    repos/userrepository.cpp \
    repos/emojirepo.cpp \
    repos/entities/rocketchatuser.cpp \
    repos/entities/rocketchatmessage.cpp \
    repos/entities/rocketchatattachment.cpp \
    repos/entities/rocketchatchannel.cpp \
    repos/entities/tempfile.cpp \
    repos/entities/audiofile.cpp \
    repos/entities/documentfile.cpp \
    repos/entities/videofile.cpp \
    repos/entities/emoji.cpp \
    persistancelayer.cpp \
    rocketchat.cpp \
    rocketchatserver.cpp \
    utils.cpp \
    restRequests/getrequest.cpp \
    restRequests/restrequest.cpp \
    restRequests/restsendmessagerequest.cpp \
    restRequests/postrequest.cpp \
    restRequests/restpaths.cpp \
    restRequests/restgetjoinedroomsrequest.cpp \
    restRequests/restloginrequest.cpp \
    restRequests/restfileuploadrequest.cpp \
    restRequests/restgetjoinedgroupsrequest.cpp \
    restRequests/restlogoutrequest.cpp \
    restRequests/restmerequest.cpp \
    notifications/notificationabstract.cpp \
    notifications/notifications.cpp \
    fileuploader.cpp \
    services/messageservice.cpp \
    services/rocketchatchannelservice.cpp \
    services/requests/loadhistoryservicerequest.cpp \
    services/requests/loadHistoryrequestcontainer.cpp \
    services/requests/loadmissedmessageservicerequest.cpp \
    services/fileservice.cpp \
    services/emojiservice.cpp \
    CustomModels/usermodel.cpp \
    rocketchatserverconfig.cpp \
    CustomModels/channelmodel.cpp \
    container/sortedvector.cpp \
    container/observablelist.cpp \
    container/modelobserver.cpp \
    ddpRequests/rocketchatchangeuserpresencedefaultstatus.cpp \
    CustomModels/messagemodel.cpp \
    CustomModels/loginmethodsmodel.cpp \
    CustomModels/models.cpp \
    repos/loginmethodsrepository.cpp \
    repos/entities/loginmethod.cpp \
    restRequests/getserverinforequest.cpp \
    ddpRequests/rocketchatblockuserrequest.cpp \
    ddpRequests/rocketchatunblockuserrequest.cpp \
    restRequests/getserverinforequest.cpp \
    ddpRequests/rocketchatleaveroomrequest.cpp \
    ddpRequests/rocketchathideroomrequest.cpp \
    ddpRequests/rocketchatsubscriberoomschanged.cpp \
    ddpRequests/rocketchatmessagesearchrequest.cpp \
    CustomModels/messagesearchresultsmodel.cpp \
    restRequests/restspotlightrequest.cpp \
    CustomModels/roomsearchresultsmodel.cpp \
    ddpRequests/rocketchatgetroombynameandtype.cpp \
    ddpRequests/rocketchatgetroombyid.cpp \
    ddpRequests/rocketchatjoinroomrequest.cpp \
    ddpRequests/rocketchatgetroomidbynameorid.cpp \
    ddpRequests/rocketchatspotlightrequest.cpp \
    repos/entities/rocketchatreplymessage.cpp \
    ddpRequests/rocketchatgetusernamesuggestion.cpp \
    ddpRequests/rocketchatsetusername.cpp \
    ddpRequests/rocketchatcreatepublicgrouprequest.cpp \
    ddpRequests/rocketchatchangeuserpresencestatus.cpp \
    ddpRequests/rocketchatsubscribeuserdata.cpp \
    CustomModels/emojismodel.cpp \
    repos/filesrepo.cpp

HEADERS += \
    api/meteorddp.h \
    api/restapi.h \
    ddpRequests/ddploginrequest.h \
    ddpRequests/ddpmethodrequest.h \
    ddpRequests/ddprequest.h \
    ddpRequests/ddpsubscriptionrequest.h \
    ddpRequests/pingrequest.h \
    ddpRequests/rocketchatsubscribechannelrequest.h \
    ddpRequests/rocketchatsubscribeusernotify.h \
    ddpRequests/rocketchatnotifynoticesrequest.h \
    ddpRequests/rocketchatsubscriptionchangedrequest.h \
    ddpRequests/rocketchatmarkchannelasreadrequest.h \
    ddpRequests/ddpunsubscriptionrequest.h \
    ddpRequests/rocketchatcreateprivategrouprequest.h \
    ddpRequests/rocketchatadduserstochannel.h \
    ddpRequests/ddpufscreaterequest.h \
    ddpRequests/rocketchatgetroomsrequest.h \
    ddpRequests/rocketchatsubscriberoomrequest.h \
    ddpRequests/rocketchatsubscribeactiveusers.h \
    ddpRequests/ddplogoutrequest.h \
    ddpRequests/rocketchatupdatepushtokenrequest.h \
    ddpRequests/rocketchatmessagesearchrequest.h \
    ddpRequests/ddpsamlloginrequest.h \
    ddpRequests/rocketchatupdatejitsitimeout.h \
    ddpRequests/rocketchatcreateaccount.h \
    ddpRequests/rocketchatsubscribeloginmethods.h \
    ddpRequests/ddpopenidloginrequest.h \
    repos/abstractbaserepository.h \
    repos/channelrepository.h \
    repos/userrepository.h \
    repos/messagerepository.h \
    repos/emojirepo.h \
    repos/entities/rocketchatuser.h \
    repos/entities/rocketchatattachment.h \
    repos/entities/rocketchatchannel.h \
    repos/entities/rocketchatmessage.h \
    repos/entities/tempfile.h \
    repos/entities/audiofile.h \
    repos/entities/documentfile.h \
    repos/entities/videofile.h \
    repos/entities/emoji.h \
    persistancelayer.h \
    rocketchat.h \
    rocketchatserver.h \
    utils.h \
    restRequests/getrequest.h \
    ddpRequests/rocketchatmessagesearchrequest.h \
    notifications/notificationabstract.h \
    restRequests/postrequest.h \
    restRequests/restpaths.h \
    restRequests/restgetjoinedroomsrequest.h \
    restRequests/restloginrequest.h \
    restRequests/restgetjoinedgroupsrequest.h \
    restRequests/restmerequest.h \
    restRequests/getrequest.h \
    restRequests/restfileuploadrequest.h \
    notifications/notifications.h \
    fileuploader.h \
    restRequests/restfileuploadrequest.h \
    services/messageservice.h \
    services/rocketchatchannelservice.h \
    services/requests/loadhistoryservicerequest.h \
    services/requests/loadHistoryrequestcontainer.h \
    services/requests/loadmissedmessageservicerequest.h \
    services/fileservice.h \
    services/emojiservice.h \
    ddpRequests/ddplogoutrequest.h \
    rocketchatserverconfig.h \
    CustomModels/usermodel.h \
    CustomModels/channelmodel.h \
    container/sortedvector.h \
    container/observablelist.h \
    container/modelobserver.h \
    config.h \
    CustomModels/messagemodel.h \
    CustomModels/loginmethodsmodel.h \
    CustomModels/models.h \
    repos/loginmethodsrepository.h \
    repos/entities/loginmethod.h \
    restRequests/getserverinforequest.h \
    ddpRequests/rocketchatblockuserrequest.h \
    ddpRequests/rocketchatunblockuserrequest.h \
    restRequests/getserverinforequest.h \
    ddpRequests/rocketchatleaveroomrequest.h \
    ddpRequests/rocketchathideroomrequest.h \
    ddpRequests/rocketchatsubscriberoomschanged.h \
    ddpRequests/rocketchatmessagesearchrequest.h \
    CustomModels/messagesearchresultsmodel.h \
    restRequests/restspotlightrequest.h \
    CustomModels/roomsearchresultsmodel.h \
    ddpRequests/rocketchatgetroombynameandtype.h \
    ddpRequests/rocketchatgetroombyid.h \
    ddpRequests/rocketchatjoinroomrequest.h \
    ddpRequests/rocketchatgetroomidbynameorid.h \
    ddpRequests/rocketchatspotlightrequest.h \
    api/messagelistener.h \
    repos/entities/rocketchatreplymessage.h \
    ddpRequests/rocketchatgetusernamesuggestion.h \
    ddpRequests/rocketchatsetusername.h \
    ddpRequests/rocketchatcreatepublicgrouprequest.h \
    ddpRequests/rocketchatchangeuserpresencestatus.h \
    ddpRequests/rocketchatchangeuserpresencedefaultstatus.h \
    ddpRequests/rocketchatsubscribeuserdata.h \
    CustomModels/emojismodel.h \
    repos/filesrepo.h

linux{

    SOURCES += segfaulthandler.cpp
    HEADERS += segfaulthandler.h

}

android{
    QT += androidextras

    SOURCES += android/androidfiledialog.cpp\
        notifications/android/googlepushnotifications.cpp\
        notifications/android/androidnotificationreceiver.cpp\
        android/androidcheckpermissions.cpp \
        android/androidbadges.cpp \
        android/androidstatusbarcolor.cpp \
        android/androidfiledialog.cpp \
        android/androidcookies.cpp




    HEADERS += android/androidfiledialog.h\
        notifications/android/googlepushnotifications.h\
        notifications/android/androidnotificationreceiver.h\
        android/androidcheckpermissions.h \
        android/androidbadges.h \
        android/androidstatusbarcolor.h \
        android/androidfiledialog.h \
        android/androidcookies.h

}

win32{
    QMAKE_CXXFLAGS += /MP
}

ios{

    QT += gui-private

    SOURCES += notifications/ios/applepushnotifications.cpp \
        notifications/ios/iosdevicetokenstorage.cpp \
        segfaulthandler.cpp \
        notifications/ios/iosnotificationreceiver.cpp


    HEADERS += notifications/ios/applepushnotifications.h \
        notifications/ios/iosdevicetokenstorage.h \
        notifications/ios/iosnotificationreceiver.h \
        notifications/ios/DelegateClass.h

    OBJECTIVE_SOURCES += notifications/ios/DelegateClass.mm


    LIBS += -framework UserNotifications

}
CONFIG(release, debug|release) {
    CONFIG  += qt release
    DEFINES += QT_NO_DEBUG_OUTPUT
    DEFINES += QT_NO_DEBUG
    # compiler options: O3 optimize
    linux:!android{
         CONFIG += ltcg
         QMAKE_CXXFLAGS += -Ofast -flto -funroll-loops -fno-signed-zeros -fno-trapping-math
    }
    android:{
         QMAKE_CXXFLAGS += -Ofast -funroll-loops -fno-signed-zeros -fno-trapping-math
    }
    ios:{
        CONFIG += ltcg
        QMAKE_CXXFLAGS += -Ofast -funroll-loops -fno-signed-zeros -fno-trapping-math
    }
    win32:{
        QMAKE_CXXFLAGS += /O2
    }
}

RESOURCES += \
    sql.qrc

DISTFILES += \
    sql/migrations/3.sql \
    sql/migrations/4.sql \
    sql/migrations/5.sql \
    sql/migrations/6.sql


