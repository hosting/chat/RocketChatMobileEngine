/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#ifndef RESTREQUEST_H
#define RESTREQUEST_H
#include <QJsonObject>
#include <functional>
class RestApi;
class QNetworkReply;
typedef std::function<void (QNetworkReply *,QJsonObject pData, RestApi*)> RestRequestCallback;

class RestRequest
{
public:
    enum class methods {
         GET,
         POST
     };
    RestRequest(QByteArray, QString, methods, RestRequestCallback);
    RestRequest( QByteArray, QString , methods, RestRequestCallback pSuccess, RestRequestCallback pError);
    methods getMethod() const;
    void setMethod(const methods &pValue);

    QString getPath() const;
    void setPath(const QString &pValue);

    QByteArray getContent() const;
    void setContent(const QByteArray &pValue);

    RestRequestCallback getSuccess() const;
    void setSuccess(const RestRequestCallback &pValue);

    RestRequestCallback getError() const;
    void setError(const RestRequestCallback &pValue);

    QString getMimeType() const;
    void setMimeType(const QString &pValue);

    bool isRawData();

    bool getAbsolutePath() const;

protected:
    RestRequest() = default;
    QByteArray mContent;
    QString mPath;
    methods mMethod;
    RestRequestCallback mSuccess = nullptr;
    RestRequestCallback mError = nullptr;
    QString mMimeType = "text/plain";
    bool mAbsolutePath = false;
    bool mRawData = false;
};

#endif // RESTREQUEST_H
