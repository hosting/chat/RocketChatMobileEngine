
/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#include <algorithm>
#include "messageservice.h"

#include "repos/entities/rocketchatreplymessage.h"
#include "services/fileservice.h"


//MessageService::MessageService( PersistanceLayer *pPersistanceLayer,
//                                RocketChatServerData *pServer,
//                                QHash<QString, QString> &pEmojisMap ): mEmojisMap( pEmojisMap )
//{
//    this->mPersistanceLayer = pPersistanceLayer;
//    this->mServer = pServer;
//}

MessageService::MessageService( QObject *parent, PersistanceLayer *pPersistanceLayer,
                                RocketChatServerData *pServer,
                                EmojiRepo *pEmojiRepo, FileService *pFileService ): QObject( parent ),
    mReplyRegeEx( "\\[ \\]\\(.*\\)</a>" ), mEmojiRepo( pEmojiRepo ), mMessagesModel( Models::getMessagesModel() ),
    mReplyReplyRegeEx( "\\[ \\]\\(.*\\)" ), mSearchResults( Models::getMessagesSearchModel() ), mFileService( pFileService )
{
    this->mPersistanceLayer = pPersistanceLayer;
    this->mServer = pServer;
    qRegisterMetaType<MessageList>( "MessageList" );
}

void MessageService::loadHistory( const QSharedPointer<LoadHistoryServiceRequest> &pRequest )
{
    if ( !pRequest.isNull() ) {
        switch ( pRequest->getSource() ) {
            case LoadHistoryServiceRequest::Source::DataBase:
                loadHistoryFromDb( pRequest );
                break;

            case LoadHistoryServiceRequest::Source::SERVER:
                loadHistoryFromServer( pRequest );
                break;

            default:
                loadHistoryFromAutoSource( pRequest );
                break;
        }
    }
}



void MessageService::checkForMissingMessages( const QSharedPointer<LoadMissedMessageServiceRequest>
        &pRequest )
{
    Q_UNUSED( pRequest )

}

void MessageService::sendMessage( const QSharedPointer<RocketChatMessage> &pMessage )
{
    if ( !pMessage.isNull() ) {
        QJsonObject params;
        params[QStringLiteral( "rid" )] = pMessage->getRoomId();
        params[QStringLiteral( "msg" )] = pMessage->getMessageString();
        auto sendMessageRequest = QSharedPointer<DDPMethodRequest>::create( QStringLiteral( "sendMessage" ), QJsonArray( {params} ) );
        mServer->sendDdprequest( sendMessageRequest, true );
    }
}

void MessageService::persistMessage( const QSharedPointer<RocketChatMessage> &pMessage )
{
    if ( !pMessage.isNull() ) {
        QJsonDocument doc( pMessage->getData() );
        QString msg = doc.toJson();
        mPersistanceLayer->addMessage( pMessage->getId(), pMessage->getRoomId(), pMessage->getAuthor(),
                                       pMessage->getTimestamp(), msg, pMessage->getAuthorId() );
    }
}

void MessageService::persistMessages( const MessageList &pMessage )
{
    mPersistanceLayer->transaction();

    for ( const auto &currentMessage : pMessage ) {
        if ( !currentMessage.isNull() ) {
            persistMessage( currentMessage );
        }
    };

    mPersistanceLayer->askForcommit();
}

void MessageService::persist()
{
    if ( mServer ) {
        mPersistanceLayer->transaction();
        auto channels = mServer->getChannels();

        for ( const auto &channel : channels->getElements() ) {
            if ( channel.isNull() ) {
                auto messages = channel->getMessageRepo();

                for ( const auto &message : messages->getElements() ) {
                    if ( !message.isNull() ) {
                        persistMessage( message );
                    }
                }
            }
        }

        mPersistanceLayer->askForcommit();
    }
}

QSharedPointer<RocketChatMessage> MessageService::parseMessage( const QJsonObject &pMessageData,
        bool linkify )
{
    if ( mServer ) {
        ChatMessage message( nullptr );
        QString userId = mServer->getUserId();
        bool blocked = false;

        if ( Q_LIKELY( pMessageData.contains( QStringLiteral( "msg" ) ) ) ) {
            //parse message String
            QString msgString = pMessageData[QStringLiteral( "msg" )].toString();

            if ( linkify ) {
                msgString = Utils::removeUtf8Emojis( msgString );

                msgString = Utils::linkiFy( msgString );
                msgString = msgString.replace( QStringLiteral( "\n" ), QStringLiteral( "<br>" ) );
                msgString = Utils::escapeHtml( msgString );
                msgString = Utils::emojiFy( msgString, mEmojiRepo );

            }

            QString msgType;
            QString author;
            QString authorId;

            if ( Q_LIKELY( pMessageData.contains( "t" ) ) ) {
                msgType = pMessageData["t"].toString();
            }

            if ( Q_UNLIKELY( mServer->getUsername() == "apple.store" ) && msgType == "jitsi_call_started" ) {
                return nullptr;
            }

            QJsonObject userObject = pMessageData["u"].toObject();

            if ( Q_LIKELY( userObject.contains( QStringLiteral( "username" ) ) ) ) {
                author = userObject[QStringLiteral( "username" )].toString();
            }

            // if alias given, us it instead of the username, required for bridges
            if ( Q_LIKELY( pMessageData.contains( QStringLiteral( "alias" ) ) ) ) {
                author = pMessageData[QStringLiteral( "alias" )].toString();
            }

            if ( Q_LIKELY( userObject.contains( "_id" ) ) ) {
                authorId = userObject[QStringLiteral( "_id" )].toString();
            }

            bool ownMessage = false;

            if ( authorId == userId ) {
                ownMessage = true;
            }

            QJsonObject timestampObject = pMessageData[QStringLiteral( "ts" )].toObject();
            qint64 timestamp = 0;

            if ( Q_LIKELY( timestampObject.contains( QStringLiteral( "$date" ) ) ) ) {
                timestamp = static_cast<qint64>( timestampObject[QStringLiteral( "$date" )].toDouble() );
            }

            QDateTime date = QDateTime::fromMSecsSinceEpoch( timestamp );

            QString formattedDate = date.toString( QStringLiteral( "dd.MM.yyyy" ) );
            QString formattedTime = date.toString( QStringLiteral( "hh:mm" ) );
            auto attachments = processAttachments( pMessageData );
            message = QSharedPointer<RocketChatMessage>::create( pMessageData );
            message->setAttachments( attachments );
            message->setOwnMessage( ownMessage );
            message->setAuthorId( authorId );

            if ( !attachments.isEmpty() ) {
                auto firstAttachment  = attachments.first();

                if ( !firstAttachment.isNull() ) {
                    if ( firstAttachment->getType() == "replyMessage" ) {
                        QString messageText = msgString;
                        messageText.replace( mReplyRegeEx, "" );
                        msgString  = messageText;
                    } else {
                        msgString = firstAttachment->getTitle();
                    }
                }
            }

            //TODO: place RocketChatUserObject inside Message instead...
            auto then = [ message ]( QSharedPointer<TempFile> tempfile, bool showInline ) {
                message->setAvatarImg( tempfile );
            };
            auto avatarUrl = "/avatar/" + author + ".jpg";
            auto avatarRequest = QSharedPointer<FileRequest>::create( avatarUrl, "temp", then, true );
            mFileService->getFileRessource( avatarRequest );

            message->setAuthor( author );
            message->setFormattedDate( formattedDate );
            message->setFormattedTime( formattedTime );
            message->setMessageString( msgString );
            message->setMessageType( msgType );

            if ( mBlockedUsers.contains( author ) ) {
                blocked = true;
            }
        }

        if ( Q_UNLIKELY( message.isNull() ) ) {
            qDebug() << "invalid messag";
        }

        message->setServer( mServer->getBaseUrl() );

        if ( !blocked ) {
            return message;
        }
    }

    return nullptr;

}

void MessageService::addUserToBlockList( const QString &pId )
{
    mBlockedUsers.insert( pId );
    auto result = QMetaObject::invokeMethod( mMessagesModel, "addBlockedUser", Q_ARG( QString, pId ) );
    Q_ASSERT( result );
}

void MessageService::addChannelToBlockList( const QString &pId )
{
    if ( mServer ) {
        QString roomId = pId;
        QString userId = mServer->getUserId();
        QString otherUserId = roomId.replace( userId, "" );
        addUserToBlockList( otherUserId );
    }
}

void MessageService::searchMessage( const QString &pTerm, const QString &pRoom )
{
    if ( mServer ) {
        auto ddpSuccess = [ = ]( const QJsonObject & pResponse,
        MeteorDDP * pDdp ) {
            Q_UNUSED( pDdp )

            if ( pResponse.contains( "result" ) ) {
                QJsonObject result = pResponse["result"].toObject();

                auto major = std::get<0>( RocketChatServerConfig::serverVersion );
                auto minor = std::get<1>( RocketChatServerConfig::serverVersion );
                auto patch = std::get<2>( RocketChatServerConfig::serverVersion );

                MessageList messageList;

                if ( major >= 0 && minor >= 70 ) {
                    if ( result.contains( "message" ) ) {
                        auto message = result["message"].toObject();

                        if ( message.contains( "docs" ) ) {
                            auto docs = message["docs"].toArray();

                            for ( const auto doc : docs ) {
                                messageList.append( parseMessage( doc.toObject() ) );
                            }
                        }
                    }
                } else {
                    if ( result.contains( "messages" ) ) {
                        auto messagesArray = result["messages"].toArray();

                        for ( const auto message : messagesArray ) {
                            messageList.append( parseMessage( message.toObject() ) );
                        }
                    }
                }

                if ( !messageList.isEmpty() ) {
                    auto result = QMetaObject::invokeMethod( mSearchResults, "setSearchResults", Q_ARG( MessageList, messageList ) );
                    Q_ASSERT( result );
                }
            }
        };

        auto request = QSharedPointer<RocketChatMessageSearchRequest>::create( pTerm, pRoom );
        request->setSuccess( ddpSuccess );

        mServer->sendDdprequest( request );
    }
}

void MessageService::loadHistoryFromServer( const QSharedPointer<LoadHistoryRequestContainer> &pContainer )
{
    if ( !pContainer.isNull() && mServer ) {
        auto requests = pContainer->getRequests();
        pContainer->mAlredayReceived = 0;

        //TODO: fix memory leak risk
        auto duplicateCheck = QSharedPointer<QSet<QString>>::create();

        if ( requests.length() ) {
            auto list = new QMultiMap<QString, ChatMessage>;

            //loop over all channels
            for ( const auto &currentChannelRequest : requests ) {
                if ( !currentChannelRequest.isNull() ) {
                    QString roomId = currentChannelRequest->getChannelIds().first();
                    QJsonObject startObj ;
                    startObj[QStringLiteral( "$date" )] = currentChannelRequest->getStart();
                    QJsonObject endObj ;
                    endObj[QStringLiteral( "$date" )] = currentChannelRequest->getEnd();
                    QJsonArray params;

                    int limit = currentChannelRequest->getLimit();

                    if ( currentChannelRequest->getEnd() < 0 ) {
                        if ( currentChannelRequest->getStart() < 0 ) {
                            params = {roomId, QJsonValue::Null, limit, QJsonValue::Null};
                        } else {
                            params = {roomId, QJsonValue::Null, limit, startObj};
                        }
                    } else {

                        params = {roomId, endObj, limit, startObj};
                    }

                    auto request = QSharedPointer<DDPMethodRequest>::create( QStringLiteral( "loadHistory" ), params );
                    DdpCallback ddpSuccess = [ = ]( const QJsonObject & pResponse,
                    MeteorDDP * pDdp ) {
                        Q_UNUSED( pDdp );

                        if ( Q_LIKELY( pResponse.contains( QStringLiteral( "result" ) ) ) ) {
                            QJsonObject result = pResponse[QStringLiteral( "result" )].toObject();

                            if ( Q_LIKELY( result.contains( QStringLiteral( "messages" ) ) ) ) {
                                QJsonArray messages  = result[QStringLiteral( "messages" )].toArray();

                                for ( auto currentMessage : messages ) {

                                    auto messageObject = parseMessage( currentMessage.toObject(), true );

                                    if ( !messageObject.isNull() && !duplicateCheck->contains( messageObject->getId() ) ) {
                                        list->insert( messageObject->getRoomId(), messageObject );
                                        duplicateCheck->insert( messageObject->getId() );
                                    }
                                }
                            }
                        }

                        pContainer->mAlredayReceived++;

                        if ( pContainer->mAlredayReceived == pContainer->mRequests.count() ) {
                            auto containerSuccess = pContainer->getSuccess();

                            if ( containerSuccess ) {
                                containerSuccess( list );
                            }
                        }
                    };

                    request->setSuccess( ddpSuccess );

                    mServer->sendDdprequest( request );
                }
            }
        }
    }
}

void MessageService::loadHistoryFromServer( const QSharedPointer<LoadHistoryServiceRequest> &pRequest )
{
    if ( !pRequest.isNull() ) {
        QList<QSharedPointer<LoadHistoryServiceRequest>> list;
        list.append( pRequest );
        auto container = QSharedPointer<LoadHistoryRequestContainer>::create( list, pRequest->getSuccess() );
        container->setSuccess( pRequest->getSuccess() );
        loadHistoryFromServer( container );
    }
}

QMultiMap<QString, ChatMessage> *MessageService::loadHistoryFromDb(
    const QSharedPointer<LoadHistoryServiceRequest> &pRequest )
{
    auto channelMap  = new QMultiMap<QString, ChatMessage>;

    if ( !pRequest.isNull() ) {
        QStringList channelIds = pRequest->getChannelIds();
        double start = pRequest->getStart();
        int limit = pRequest->getLimit();
        QList<QJsonObject> messages;
        messages.reserve( 100 );

        for ( const auto &channelId : channelIds ) {
            QList<QJsonObject> result = mPersistanceLayer->getMessagesByRid( channelId,
                                        static_cast<qint64>( start ), static_cast< int >( limit ) );
            messages.append( result );
        }

        for ( const auto &current : messages ) {
            auto parsedMessage = parseMessage( current, false );

            if ( Q_LIKELY( !parsedMessage.isNull() ) ) {
                channelMap->insert( parsedMessage->getRoomId(), parsedMessage );
            }
        }
    }

    return channelMap;
}

void MessageService::loadHistoryFromAutoSource( const QSharedPointer<LoadHistoryServiceRequest> &pRequest )
{
    int limit = pRequest->getLimit();
    auto channelMap = loadHistoryFromDb( pRequest );
    QList<QSharedPointer<LoadHistoryServiceRequest>> requestList;
    requestList.reserve( 50 );
    auto serverSuccess = [ = ]( QMultiMap<QString, QSharedPointer<RocketChatMessage>> *messages ) {
        if ( messages != nullptr ) {
            auto success = pRequest->getSuccess();
            success( messages );
        }
    };

    for ( const auto &key :  pRequest->getChannelIds() ) {
        if ( Q_LIKELY( channelMap->contains( key ) ) ) {
            MessageList currentList = channelMap->values( key );

            if ( currentList.count() < limit ) {
                std::sort( currentList.begin(), currentList.end() );
                double timestamp;

                if ( !currentList.empty() ) {
                    auto latestMessage = currentList.last();
                    timestamp = latestMessage->getTimestamp();
                } else {
                    timestamp = QDateTime::currentDateTime().toMSecsSinceEpoch();
                }

                auto newrequest = QSharedPointer<LoadHistoryServiceRequest>::create( key );
                newrequest->setStart( timestamp );
                newrequest->setLimit( limit - currentList.count() );

                if ( pRequest->getEnd() > -1 ) {
                    newrequest->setEnd( pRequest->getEnd() );
                }

                requestList.append( newrequest );
            }
        } else {
            auto newrequest =  QSharedPointer<LoadHistoryServiceRequest>::create( key );
            double timestamp = QDateTime::currentDateTime().toMSecsSinceEpoch();

            if ( pRequest->getStart() > -1 ) {
                timestamp = pRequest->getStart();
            }

            newrequest->setStart( timestamp );

            if ( pRequest->getEnd() > - 1 ) {
                newrequest->setEnd( pRequest->getEnd() );
            }

            newrequest->setLimit( limit );
            requestList.append( newrequest );
        }

        //in all cases load the last three days
        if ( !pRequest->getNoLastThreeDays() ) {
            double end = QDateTime::currentMSecsSinceEpoch();
            double start = end - ( 86400 * 3 * 1000 );
            auto lastThreeDays = QSharedPointer<LoadHistoryServiceRequest>::create( key );
            pRequest->setStart( start );
            pRequest->setStart( start );
            requestList.append( lastThreeDays );
        }
    }

    auto success = pRequest->getSuccess();
    success( channelMap );
    qDebug() << "loaded from database now loading from sever with " << requestList.size() << "requests";
    auto requests = QSharedPointer<LoadHistoryRequestContainer>::create( requestList, serverSuccess );
    loadHistoryFromServer( requests );
}

QList<QSharedPointer<RocketChatAttachment>> MessageService::processAttachments(
            const QJsonObject &pMessageObj )
{

    QList<QSharedPointer<RocketChatAttachment> > attachmentsList;

    if ( Q_LIKELY( pMessageObj.contains( QStringLiteral( "file" ) ) ) ) {
        QJsonObject fileObj = pMessageObj[QStringLiteral( "file" )].toObject();

        if ( Q_LIKELY( fileObj.contains( QStringLiteral( "_id" ) ) ) ) {
            QJsonArray attachments = pMessageObj[QStringLiteral( "attachments" )].toArray();

            if ( Q_LIKELY( attachments.size() ) ) {
                QString type;
                QString url;
                QString title;
                int width = 100;
                int height = 100;
                QJsonObject attachment = attachments[0].toObject();

                if ( Q_LIKELY( attachment.contains( QStringLiteral( "image_url" ) ) ) ) {
                    type = QStringLiteral( "image" );
                    url = attachment[QStringLiteral( "image_url" )].toString();

                    if ( attachment.contains( QStringLiteral( "image_dimensions" ) ) ) {
                        QJsonObject imageDimensions = attachment[QStringLiteral( "image_dimensions" )].toObject();

                        if ( imageDimensions.contains( QStringLiteral( "height" ) )
                                && imageDimensions.contains( QStringLiteral( "width" ) ) ) {
                            height = imageDimensions[QStringLiteral( "height" )].toDouble();
                            width = imageDimensions[QStringLiteral( "width" )].toDouble();
                        }
                    }
                } else if ( attachment.contains( QStringLiteral( "video_url" ) ) ) {
                    type = QStringLiteral( "video" );
                    url = attachment[QStringLiteral( "video_url" )].toString();
                } else if ( attachment.contains( QStringLiteral( "audio_url" ) ) ) {
                    type = QStringLiteral( "audio" );
                    url = attachment[QStringLiteral( "audio_url" )].toString();
                } else if ( attachment.contains( QStringLiteral( "title_link" ) ) ) {
                    type = QStringLiteral( "file" );
                    url = attachment[QStringLiteral( "title_link" )].toString();
                }

                if ( Q_LIKELY( attachment.contains( QStringLiteral( "title" ) ) ) ) {
                    title = attachment[QStringLiteral( "title" )].toString();
                }

                auto attachmentObject = QSharedPointer<RocketChatAttachment>::create( url, type, title );
                attachmentObject->setHeight( height );
                attachmentObject->setWidth( width );

                attachmentsList.append( attachmentObject );
            }
        }
    }

    if ( pMessageObj.contains( "urls" ) ) {
        QJsonArray attachments = pMessageObj[QStringLiteral( "attachments" )].toArray();

        for ( QJsonValueRef attachment : attachments ) {
            QJsonObject attachmentObject  = attachment.toObject();

            if ( attachmentObject.contains( "text" ) ) {
                QString text = attachmentObject["text"].toString();
                QString url = "";

                if ( attachmentObject.contains( "message_link" ) ) {
                    url = attachmentObject["message_link"].toString();
                }

                QString author = attachmentObject["author_name"].toString();
                text = text.replace( mReplyRegeEx, "" );
                text = text.replace( mReplyReplyRegeEx, "" );
                auto reply = QSharedPointer<RocketChatReplyMessage>::create( text, author );
                attachmentsList.append( reply );
            }
        }
    }

    return attachmentsList;
}

void MessageService::deleteMessagesNotInList( MessageMap *pMessages, QString pChannelId, bool pCheckForYounger )
{
    QMap<qint64, QSharedPointer<RocketChatMessage>> timeIndex;
    QList<QString> ids;

    if ( mServer ) {
        auto channel = mServer->getChannels()->get( pChannelId );

        for ( const auto message : pMessages->values( pChannelId ) ) {
            timeIndex[message->getTimestamp()] = message;
            ids.append( message->getId() );
        }

        qint64 oldest = timeIndex.first()->getTimestamp();
        qint64 newest = timeIndex.last()->getTimestamp();
        qint64 youngest = channel->getYoungestMessage()->getTimestamp();

        if ( pCheckForYounger ) {
            newest = newest < youngest ? youngest : newest;
        }

        auto blacklist = channel->whiteList( ids, newest, oldest );

        for ( QString messageId : blacklist ) {
            mPersistanceLayer->deleteMessage( messageId );
        }
    }
}
