#ifndef FILESREPO_H
#define FILESREPO_H

#include <QObject>
#include "abstractbaserepository.h"
#include "repos/entities/tempfile.h"

class FilesRepo : public QObject, public AbstractBaseRepository<TempFile>
{
        Q_OBJECT
    public:
        explicit FilesRepo( QObject *parent = nullptr );
};

#endif // FILESREPO_H
