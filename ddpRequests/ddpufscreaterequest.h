/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#ifndef DDPUFSCREATEREQUEST_H
#define DDPUFSCREATEREQUEST_H

#include <functional>

#include "ddpmethodrequest.h"
#include "rocketchatserverconfig.h"


class DDPUfsCreateRequest : public DDPMethodRequest
{
    public:
        DDPUfsCreateRequest( const QString &pChannelId, const QString &filename, int size,
                             const QString &pMime, const DdpCallback &pSuccess );
        DDPUfsCreateRequest( const QString &pChannelId, const QString &pFilename, int pSize, const QString &pMime );

};

#endif // DDPUFSCREATEREQUEST_H
