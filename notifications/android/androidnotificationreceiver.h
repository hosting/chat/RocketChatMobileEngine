/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#ifndef ANDROIDNOTIFICATIONRECEIVER_H
#define ANDROIDNOTIFICATIONRECEIVER_H

#include <QString>
#include <QAndroidJniEnvironment>
#include <QAndroidJniObject>
#include <QGlobalStatic>
#include "notifications/android/googlepushnotifications.h"

class GooglePushNotifications;

class AndroidNotificationReceiver
{
    public:
        AndroidNotificationReceiver();
        ~AndroidNotificationReceiver();

        void doRegister( GooglePushNotifications *pGooglePushNotifications );

        static AndroidNotificationReceiver *instance();
        static void registrationIdHandler( JNIEnv *env, jobject thiz, jstring errorMessge );
        static void errorHandler( JNIEnv *env, jobject thiz, jstring errorMessage );
        static void messageReceived( JNIEnv *env, jobject thiz, jstring server, jstring rid, jstring name, jstring type );

        GooglePushNotifications *receiver() const;
        void setReceiver( GooglePushNotifications *receiver );

    private:
        static AndroidNotificationReceiver *mAndroidReceiver;

        GooglePushNotifications *mReceiver = nullptr;
        QString mSenderId;

};

#endif // ANDROIDNOTIFICATIONRECEIVER_H
