/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#include "meteorddp.h"
#include "meteorddp.h"
#include <QString>
#include <QWebSocket>
#include <QTime>
#include <QCryptographicHash>
#include <functional>
#include <QAbstractSocket>
#include "ddpRequests/ddploginrequest.h"

MeteorDDP::MeteorDDP( QObject *parent, const QString &pUri, bool pUnsecure ): QObject( parent ), unsecure( pUnsecure )
{
    mError = new QJsonParseError;
    init( pUri );
}


void MeteorDDP::init( const QString &pUri )
{
    QString protocol = QStringLiteral( "wss://" );

    if ( unsecure ) {
        protocol = QStringLiteral( "ws://" );
    }

    QUrl wsUri = QUrl( protocol + pUri + QStringLiteral( "/websocket" ) );
    qDebug() << wsUri;
    qDebug() << "meteor init" ;
    mWebsocketUri = pUri;
    mResponseBinding.reserve( 100 );
    connect( &mWebsocket, &QWebSocket::textMessageReceived, this, &MeteorDDP::onTextMessageReceived, Qt::UniqueConnection );
    connect( &mWebsocket, &QWebSocket::connected, this, &MeteorDDP::onConnected, Qt::UniqueConnection );
    connect( &mWebsocket, &QWebSocket::disconnected, this, &MeteorDDP::ddpDisconnected );
    connect( this, &MeteorDDP::sendMessageSignal, this, &MeteorDDP::sendJson, Qt::UniqueConnection );
    connect( &mWebsocket, QOverload<QAbstractSocket::SocketError>::of( &QWebSocket::error ),
    [ = ]( QAbstractSocket::SocketError error ) {
        qDebug() << mWebsocket.errorString();
    } );

    mWebsocket.open( wsUri );
    QDateTime now = QDateTime::currentDateTime();
    uint currentTime = now.toTime_t();
    mLastPing = currentTime;
}

void MeteorDDP::registerMessageHandler( MessageListener &listener )
{
    connect( this, &MeteorDDP::messageReceived, &listener, &MessageListener::handlesMessage );
}

void MeteorDDP::registerMessageHandler( MessageListener *listener )
{
    connect( this, &MeteorDDP::messageReceived, listener, &MessageListener::handlesMessage );
}

void MeteorDDP::connectWithServer()
{
    QJsonObject msgObj;
    QJsonArray support;

    support.append( "1" );

    msgObj[QStringLiteral( "msg" )] = QStringLiteral( "connect" );
    msgObj[QStringLiteral( "version" )] = "1";
    msgObj[QStringLiteral( "support" )] = support;
    sendJson( msgObj );
}

void MeteorDDP::onTextMessageReceived( const QString &pMsg )
{
#if defined(Q_OS_LINUX)|| defined(Q_OS_IOS)
    qDebug() << pMsg << "\n";
#endif

    if ( Q_LIKELY( pMsg.length() ) ) {
        QJsonDocument jsonResponse = QJsonDocument::fromJson( pMsg.toUtf8(), mError );
        QJsonObject objectResponse = jsonResponse.object();

        if ( Q_LIKELY( mError->error ==  QJsonParseError::NoError && objectResponse.contains( QStringLiteral( "msg" ) ) ) ) {
            QDateTime now = QDateTime::currentDateTime();
            mLastPing = now.toTime_t();
            QString messagePart = objectResponse[QStringLiteral( "msg" )].toString();

            if ( messagePart == QStringLiteral( "ping" ) || messagePart == QStringLiteral( "pong" ) ) {

                emit messageReceived( objectResponse );
            }

            if ( messagePart == QStringLiteral( "ping" ) ) {
                qDebug() << QString::number( mLastPing );
                pong();
            } else if ( messagePart == QStringLiteral( "ready" ) ) {
                if ( Q_LIKELY( objectResponse.contains( QStringLiteral( "subs" ) ) ) ) {
                    QJsonArray subsArray = objectResponse[QStringLiteral( "subs" )].toArray();

                    if ( Q_LIKELY( subsArray.size() ) ) {
                        QString id = subsArray[0].toString();

                        if ( Q_LIKELY( mResponseBinding.contains( id ) && !mResponseBinding[id].isNull() ) ) {
                            DdpCallback succesFuncton = mResponseBinding[id]->getSuccess();

                            if ( Q_LIKELY( succesFuncton ) ) {
                                succesFuncton( objectResponse, this );
                            }
                        }
                    }
                }

                //emit ddpLoginConfigured();
            } else if ( messagePart == QStringLiteral( "connected" ) ) {
                qDebug() << "connection confirmed";
                emit ddpConnected();
            } else if ( messagePart == QStringLiteral( "added" ) || messagePart == QStringLiteral( "changed" ) || messagePart == QStringLiteral( "removed" ) ) {
                if ( objectResponse.contains( QStringLiteral( "fields" ) ) ) {
                    QJsonObject fields = objectResponse[QStringLiteral( "fields" )].toObject();

                    if ( Q_LIKELY( fields.contains( QStringLiteral( "args" ) ) ) ) {
                        QJsonArray args = fields[QStringLiteral( "args" )].toArray();

                        for ( auto currentArg : args ) {
                            QJsonObject argObj = currentArg.toObject();

                            if ( Q_LIKELY( argObj.contains( QStringLiteral( "_id" ) ) ) ) {
                                //qDebug()<<"message received";
                                emit messageReceived( objectResponse );
                            }
                        }
                    } else {
                        emit messageReceived( objectResponse );
                    }
                }
            }

            if ( objectResponse.contains( QStringLiteral( "id" ) ) ) {

                QString id = objectResponse[QStringLiteral( "id" )].toString();

                if ( Q_LIKELY( !mReceivedEvents.contains( id ) ) ) {
                    mReceivedEvents.insert( id );

                    if ( Q_LIKELY( mResponseBinding.contains( id ) ) ) {
                        DdpCallback succesFunction = mResponseBinding[id]->getSuccess();
                        DdpCallback errorFunction =  mResponseBinding[id]->getError();
                        bool error = objectResponse.contains( QStringLiteral( "error" ) );

                        if ( Q_UNLIKELY( error && errorFunction ) ) {
                            errorFunction( objectResponse, this );
                        } else if ( Q_LIKELY( succesFunction ) ) {
                            succesFunction( objectResponse, this );
                        }
                    }
                }
            }
        }
    }
}

void MeteorDDP::sendJson( const QJsonObject &pMsgObj )
{

    QJsonDocument msgJson = QJsonDocument( pMsgObj );
    QString msgString = msgJson.toJson( QJsonDocument::Compact );
    qDebug() << "message String" << msgString;
    mWebsocket.sendTextMessage( msgString );
}

bool MeteorDDP::getUnsecure() const
{
    return unsecure;
}

void MeteorDDP::onConnected()
{
    qDebug() << "connected websocket";
    mWebsocketConnectionEstablished = true;
    // emit( websocketConnected() );
    this->connectWithServer();
}


void MeteorDDP::pong()
{
    QJsonObject msgObj;
    msgObj[QStringLiteral( "msg" )] = QStringLiteral( "pong" );
    msgObj[QStringLiteral( "id" )] = getFrameCount();
    sendJson( msgObj );
}



void MeteorDDP::sendRequest( const QSharedPointer<DDPRequest> &pDdpRequest )
{
    if ( !pDdpRequest.isNull() ) {
        QString frame = getFrameCount();
        QJsonObject rawRequest =  pDdpRequest->getRawRequest();

        if ( !rawRequest.contains( QStringLiteral( "id" ) ) ) {
            rawRequest[QStringLiteral( "id" )] = frame;
        }

        pDdpRequest->setFrame( frame );
        mResponseBinding[frame] = pDdpRequest;
        emit sendMessageSignal( rawRequest );
        // sendJson( rawRequest );
    }

}

//debug
void MeteorDDP::resume()
{
    qDebug() << "resuming ddp";
    qDebug() << "websocket valid: " << mWebsocket.isValid();
    QUrl wsUri = QUrl( QStringLiteral( "wss://" ) + mWebsocketUri + QStringLiteral( "/websocket" ) );

    if ( mWebsocket.state() == QAbstractSocket::UnconnectedState ) {
        mWebsocket.open( wsUri );
    } else if ( mWebsocket.state() != QAbstractSocket::ConnectingState ) {
        auto const connection = new QMetaObject::Connection;
        *connection = connect( &mWebsocket, &QWebSocket::disconnected, [ = ]() {
            qDebug() << "websocket closed";
            mWebsocket.open( wsUri );
            QDateTime now = QDateTime::currentDateTime();
            uint currentTime = now.toTime_t();
            mLastPing = currentTime;

            if ( connection != nullptr ) {
                QObject::disconnect( *connection );
                delete connection;
            }
        } );
        mWebsocket.close();
    }

    if ( mWebsocket.state() == QAbstractSocket::UnconnectedState ) {

        // connectWithServer();
    }
}

void MeteorDDP::unsetResponseBinding( const QString &pId )
{
    if ( mResponseBinding.contains( pId ) ) {
        mResponseBinding.remove( pId );
    }
}

bool MeteorDDP::websocketIsValid()
{
    return mWebsocket.isValid();
}

uint MeteorDDP::diffToLastMessage()
{
    QDateTime now = QDateTime::currentDateTime();
    uint currentTime = now.toTime_t();
    uint difference = currentTime - mLastPing;
    return difference;
}

void MeteorDDP::disconnectFromServer()
{
    qDebug() << "disconnect from server";
    mWebsocket.close();
}

void MeteorDDP::setResumeToken( const QString &pToken )
{
    this->mToken = pToken;
}

void MeteorDDP::setUserId( const QString &pUserId )
{
    this->mUid = pUserId;
}


QString MeteorDDP::getFrameCount()
{
    return QString::number( frameCount++ );
}

QString MeteorDDP::getToken() const
{
    return mToken;
}

void MeteorDDP::setToken( const QString &pValue )
{
    mToken = pValue;
}

bool MeteorDDP::getWebsocketConnectionEstablished() const
{
    return mWebsocketConnectionEstablished;
}
