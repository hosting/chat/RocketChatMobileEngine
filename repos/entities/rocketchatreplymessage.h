#ifndef ROCKETCHATREPLYMESSAGE_H
#define ROCKETCHATREPLYMESSAGE_H

#include "rocketchatattachment.h"
#include "rocketchatmessage.h"

#include <QObject>

class RocketChatReplyMessage : public RocketChatAttachment, public RocketChatMessage
{
    public:
        RocketChatReplyMessage( const QString &pText, const QString &pAuthor, const QString &pUrl = "" );
        using RocketChatMessage::getMessageString;
        using RocketChatMessage::getAuthor;
};

#endif // ROCKETCHATREPLYMESSAGE_H
