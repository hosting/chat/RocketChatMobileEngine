/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#ifndef SEGFAULTHANDLER_H
#define SEGFAULTHANDLER_H

#include <QObject>
#include <QHash>

#if defined(Q_OS_ANDROID) || defined(Q_OS_MACOS) || defined(Q_OS_LINUX) ||defined(Q_OS_IOS)
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#endif

class SegfaultHandler : public QObject
{
        Q_OBJECT
    public:
        explicit SegfaultHandler( QObject *parent = 0 );
        explicit SegfaultHandler( const QString & );

        ~SegfaultHandler();

    private:
        static void segfaultHandlerMethod( int pSigno );

        void init();
#if defined(Q_OS_ANDROID) || defined(Q_OS_MACOS) || defined(Q_OS_LINUX) ||defined(Q_OS_IOS)
        struct sigaction mAct;
        struct sigaction mOldAct;
        static QHash<Qt::HANDLE, struct sigaction> mActs;
#endif
        QString mFailurePoint;
        static QHash<Qt::HANDLE, QString> mFailurePoints;

    signals:

    public slots:
};

#endif // SEGFAULTHANDLER_H
