/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#ifndef LOADHISTORYFROMSERVERREQUEST_H
#define LOADHISTORYFROMSERVERREQUEST_H
#include <QSharedPointer>
#include "loadhistoryservicerequest.h"
#include "repos/entities/rocketchatmessage.h"
#include <QAtomicInt>
class LoadHistoryRequestContainer
{
        friend class MessageService;
    public:
        LoadHistoryRequestContainer( const QList<QSharedPointer<LoadHistoryServiceRequest>> &, const
                                     std::function<void ( QMultiMap<QString, QSharedPointer<RocketChatMessage>>* )> & );

        QList<QSharedPointer<LoadHistoryServiceRequest> > getRequests() const;


        std::function<void ( QMultiMap<QString, QSharedPointer<RocketChatMessage> > * )> getSuccess() const;
        void setSuccess( const std::function<void ( QMultiMap<QString, QSharedPointer<RocketChatMessage> > * )> &pValue );

    protected:
        QList<QSharedPointer<LoadHistoryServiceRequest>> mRequests;
        std::function<void ( QMultiMap<QString, QSharedPointer<RocketChatMessage>>* )> mSuccess;
        QAtomicInt mAlredayReceived;
};

#endif // LOADHISTORYFROMSERVERREQUEST_H
