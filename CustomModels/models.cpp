#include "models.h"

UserModel *Models::mUserModel = nullptr;
LoginMethodsModel *Models::mLoginMethodsModel = nullptr;
ChannelModel *Models::mDirectChannelsModel = nullptr;
ChannelModel *Models::mPublicGroupsModel = nullptr;
ChannelModel *Models::mPrivateGroupsModel = nullptr;
MessagesModel *Models::mMessagesModel = nullptr;
MessageSearchResultsModel *Models::mMessagesSearchModel = nullptr;
RoomSearchResultsModel *Models::mRoomsSearchModel = nullptr;
EmojisModel *Models::mEmojisModel = nullptr;

UserModel *Models::getUsersModel()
{
    return mUserModel;
}

LoginMethodsModel *Models::getLoginMethodsModel()
{
    return mLoginMethodsModel;
}

ChannelModel *Models::getDirectChannelsModel()
{
    return mDirectChannelsModel;
}

ChannelModel *Models::getPublicGroupsModel()
{
    return mPublicGroupsModel;
}

ChannelModel *Models::getPrivateGroupsModel()
{
    return mPrivateGroupsModel;
}

MessagesModel *Models::getMessagesModel()
{
    return mMessagesModel;
}

MessageSearchResultsModel *Models::getMessagesSearchModel()
{
    return mMessagesSearchModel;
}

RoomSearchResultsModel *Models::getRoomSearchModel()
{
    return mRoomsSearchModel;
}

void Models::resetModels()
{
    mDirectChannelsModel->clear();
    auto result = QMetaObject::invokeMethod( mLoginMethodsModel, "clear" );
    Q_ASSERT( result );
    // mLoginMethodsModel->clear();
    mPublicGroupsModel->clear();
    mPrivateGroupsModel->clear();
    mMessagesSearchModel->clear();
    mRoomsSearchModel->clear();
    mEmojisModel->clear();
}

void Models::init()
{
    if ( mDirectChannelsModel == nullptr ) {
        mDirectChannelsModel = new ChannelModel();
    }

    if ( mLoginMethodsModel == nullptr ) {
        mLoginMethodsModel = new LoginMethodsModel();
    }

    if ( mPublicGroupsModel == nullptr ) {
        mPublicGroupsModel = new ChannelModel();
    }

    if ( mPrivateGroupsModel == nullptr ) {
        mPrivateGroupsModel = new ChannelModel();
    }

    if ( mMessagesModel == nullptr ) {
        mMessagesModel = new MessagesModel();
    }

    if ( mUserModel == nullptr ) {
        mUserModel = new UserModel();
    }

    if ( mMessagesSearchModel == nullptr ) {
        mMessagesSearchModel = new MessageSearchResultsModel();
    }

    if ( mRoomsSearchModel == nullptr ) {
        mRoomsSearchModel = new RoomSearchResultsModel();
    }

    if ( mEmojisModel == nullptr ) {
        mEmojisModel = new EmojisModel();
    }
}

Models::~Models()
{
    Models::mDirectChannelsModel->deleteLater();
    Models::mLoginMethodsModel->deleteLater();
    Models::mMessagesModel->deleteLater();
    Models::mPrivateGroupsModel->deleteLater();
    Models::mPublicGroupsModel->deleteLater();
    Models::mUserModel->deleteLater();
    Models::mMessagesSearchModel->deleteLater();
    Models::mRoomsSearchModel->deleteLater();
}

EmojisModel *Models::getEmojisModel()
{
    return mEmojisModel;
}
