/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/

#include "persistancelayer.h"
#include "rocketchatserverconfig.h"

std::tuple<int, int, int> RocketChatServerConfig::serverVersion = std::make_tuple( -1, -1, -1 );
QString RocketChatServerConfig::uniqueId;
QString RocketChatServerConfig::jitsiMeetUrl;
QString RocketChatServerConfig::jitsiMeetPrefix;

void RocketChatServerConfig::init()
{
    PersistanceLayer *mStorage = PersistanceLayer::instance();
    QString serverVersion = mStorage->getSetting( "serverversion" );

    if ( !serverVersion.isEmpty() ) {
        QStringList verParts = serverVersion.split( "," );

        if ( verParts.length() > 2 ) {
            std::get<0>( RocketChatServerConfig::serverVersion ) = verParts[0].toInt();
            std::get<1>( RocketChatServerConfig::serverVersion ) = verParts[1].toInt();
            std::get<2>( RocketChatServerConfig::serverVersion ) = verParts[2].toInt();
        }
    }
}
