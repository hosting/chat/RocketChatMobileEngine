/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include <QCryptographicHash>
#include <QRegularExpressionMatch>
#include <QRegularExpression>
#include "ddploginrequest.h"




DDPLoginRequest::DDPLoginRequest( const QString &pToken )
{
    //["{\"msg\":\"method\",\"method\":\"login\",\"params\":[{\"resume\":\"mkdhgeChLGBAzGleEilxx7CgqIVkjthWdLmWsrCKsmH\"}],\"id\":\"1\"}"]
    QJsonObject tokenParameter =  {{QStringLiteral( "resume" ), pToken}};
    QJsonArray params = {tokenParameter};
    buildRequest( QStringLiteral( "login" ), params );
}

DDPLoginRequest::DDPLoginRequest( const QString &pToken, const DdpCallback &success ): DDPLoginRequest( pToken )
{
    setSuccess( success );
}



DDPLoginRequest::DDPLoginRequest( const QString &pUsername, const QString &pPswHash )
{
    QJsonArray params;
    QJsonObject user;
    QJsonObject loginData;
    QJsonObject passwordObj;

    QRegularExpression emailRegex( QStringLiteral( "^[0-9a-zA-Z]+([0-9a-zA-Z]*[-._+])*[0-9a-zA-Z]+@[0-9a-zA-Z]+([-.][0-9a-zA-Z]+)*([0-9a-zA-Z]*[.])[a-zA-Z]{2,6}$" ) );
    QRegularExpressionMatch emailMatch = emailRegex.match( pUsername );

    if ( emailMatch.hasMatch() ) {
        user[QStringLiteral( "email" )] = pUsername;
    } else {
        user[QStringLiteral( "username" )] = pUsername;
    }

    //QByteArray hashArray = QCryptographicHash::hash( password.toUtf8(), QCryptographicHash::Sha256 );
    passwordObj[QStringLiteral( "digest" )] = pPswHash;
    passwordObj[QStringLiteral( "algorithm" )] = QStringLiteral( "sha-256" );
    loginData[QStringLiteral( "user" )] = user;
    loginData[QStringLiteral( "password" )] = passwordObj;
    params.append( loginData );
    buildRequest( QStringLiteral( "login" ), params );
}

DDPLoginRequest::DDPLoginRequest( const QString &pUsername, const QString &pPswHash, const DdpCallback &success ): DDPLoginRequest( pUsername, pPswHash )
{
    setSuccess( success );
}
