#ifndef ROCKETCHATSETUSERNAME_H
#define ROCKETCHATSETUSERNAME_H

#include "ddpmethodrequest.h"


class RocketChatSetUsername : public DDPMethodRequest
{
    public:
        RocketChatSetUsername( const QString &pUsername, const DdpCallback &pSuccess );
};

#endif // ROCKETCHATSETUSERNAME_H
