/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include "usermodel.h"

#include <QDateTime>

UserModel::UserModel( QObject *parent ): QAbstractListModel( parent )
{

}

int UserModel::rowCount( const QModelIndex &parent ) const
{
    Q_UNUSED( parent );
    int count = userOfCurrentChannel.count();
    qDebug() << "count  of current channel users" << count;
    return count;
}

QVariant UserModel::data( const QModelIndex &index, int role ) const
{
    auto userList = userOfCurrentChannel.values();

    if ( userList.count() < index.row() ) {
        return QVariant();
    }

    const auto &user = userList.at( index.row() );

    auto roleType = static_cast<UserRoles>( role );

    switch ( roleType ) {
        case UserRoles::UserName:
            return user->getUserName();

        case UserRoles::name:
            return user->getName();

        case UserRoles::UserId:
            return  user->getUserId();

        case UserRoles::status: {
                int status = 2;

                switch ( user->getStatus() ) {
                    case RocketChatUser::status::ONLINE :
                        status = 0;
                        break;

                    case RocketChatUser::status::AWAY :
                        status = 1;
                        break;

                    case RocketChatUser::status::OFFLINE:
                        status = 2;
                        break;

                    case RocketChatUser::status::BUSY:
                        status = 3;
                        break;
                }

                return status;
            }
    }

    return QVariant();
}

bool UserModel::insertUser( const QString &channelId, const QSharedPointer<RocketChatUser> &user )
{
    beginResetModel();

    if ( !user.isNull() ) {
        QSharedPointer<RocketChatUser> userToAdd;
        mMutex.lock();

        if ( mAllUsers.contains( user->getUserId() ) ) {
            auto oldUser = mAllUsers[user->getUserId()];

            if ( !user->getSurname().isEmpty() ) {
                oldUser->setSurname( user->getSurname() );
            }

            if ( !user->getLastname().isEmpty() ) {
                oldUser->setLastname( user->getLastname() );
            }

            if ( !user->getName().isEmpty() ) {
                oldUser->setName( user->getName() );
            }

            if ( !user->getUserId().isEmpty() ) {
                oldUser->setUserId( user->getUserId() );
            }

            userToAdd = oldUser;
        } else {
            userToAdd = user;
            mAllUsers.insert( userToAdd->getUserId(), userToAdd );
            connect( userToAdd.data(), &RocketChatUser::statusChanged, this, &UserModel::refreshUserList );
        }

        userMap.insert( channelId, userToAdd );
        mMutex.unlock();
        setCurrent( getCurrent() );
    } else {
        return true;
    }

    endResetModel();
    //TODO: give some meainingfull return
    return true;
}

void UserModel::refreshUserList()
{
    beginResetModel();
    endResetModel();
}

QString UserModel::getCurrent() const
{
    return current;
}

void UserModel::setCurrent( const QString &value )
{
    current = value;
    mMutex.lock();
    userOfCurrentChannel.clear();
    beginResetModel();
    QList<QSharedPointer<RocketChatUser>> userList = userMap.values( value );
    mMutex.unlock();

    for ( const auto &current : userList ) {
        userOfCurrentChannel.insert( current->getUserId(), current );
    }

    endResetModel();
}

void UserModel::onCurrentChannelChanged( const QString &newText )
{
    current = newText;
}

QSharedPointer<RocketChatUser> UserModel::getUserById( const QString &pId )
{
    QSharedPointer<RocketChatUser> user;
    //mMutex.lock();

    if ( mAllUsers.contains( pId ) ) {
        user = mAllUsers[pId];
    }

    //mMutex.unlock();
    return user;
}

void UserModel::addUser( const QSharedPointer<RocketChatUser> &pUser )
{
    if ( !pUser.isNull() ) {
        mMutex.lock();

        if ( !mAllUsers.contains( pUser->getUserId() ) ) {
            mAllUsers.insert( pUser->getUserId(), pUser );
            connect( pUser.data(), &RocketChatUser::statusChanged, this, &UserModel::refreshUserList );
        } else {
            auto oldUser = mAllUsers[pUser->getUserId()];
            oldUser->setStatus( pUser->getStatus() );
        }

        mMutex.unlock();
    }
}

QHash<int, QByteArray> UserModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[static_cast<int>( UserRoles::UserName )] = QByteArrayLiteral( "username" );
    roles[static_cast<int>( UserRoles::UserId )] =  QByteArrayLiteral( "userId" );
    roles[static_cast<int>( UserRoles::name )] = QByteArrayLiteral( "name" );
    roles[static_cast<int>( UserRoles::status )] = QByteArrayLiteral( "status" );
    return roles;
}
