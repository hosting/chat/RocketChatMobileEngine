#ifndef ROCKETCHATSUBSCRIBEROOMSCHANGED_H
#define ROCKETCHATSUBSCRIBEROOMSCHANGED_H

#include <QJsonArray>
#include "ddpsubscriptionrequest.h"

class RocketChatSubscribeRoomsChanged : public DDPSubscriptionRequest
{
    public:
        explicit RocketChatSubscribeRoomsChanged( const QString &pUserId );
};

#endif // ROCKETCHATSUBSCRIBEROOMSCHANGED_H
