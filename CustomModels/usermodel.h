/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#ifndef USERMODEL_H
#define USERMODEL_H
#include <QAbstractListModel>
#include <QSharedPointer>
#include <QMultiMap>
#include <QSet>
#include <QDebug>
#include <QMap>
#include <QList>
#include <QMutex>

#include "repos/entities/rocketchatuser.h"

typedef QSharedPointer<RocketChatUser> User;

class UserModel: public QAbstractListModel
{
        Q_OBJECT
        Q_PROPERTY( QString currentChannel READ getCurrent WRITE setCurrent NOTIFY currentChannelChanged )
        enum class UserRoles : int {
            UserName = Qt::UserRole + 1,
            UserId,
            name,
            status
        };
    public slots:
        bool insertUser( const QString &, const QSharedPointer<RocketChatUser> & );
        void refreshUserList();
        void addUser( const QSharedPointer<RocketChatUser> &pUser );

    public:
        explicit UserModel( QObject *parent = nullptr );
        int rowCount( const QModelIndex &parent = QModelIndex() ) const;
        QVariant data( const QModelIndex &index, int role = Qt::DisplayRole ) const;
        QString getCurrent() const;
        void setCurrent( const QString &value );
        void onCurrentChannelChanged( const QString &newText );

        QSharedPointer<RocketChatUser> getUserById( const QString &pId );


    protected:
        QHash<int, QByteArray> roleNames() const;
        QSet<QString> duplicateCheck;
        QMultiMap<QString, QSharedPointer<RocketChatUser>> userMap;
        QMap<QString, QSharedPointer<RocketChatUser>> userOfCurrentChannel;
        QMap<QString, QSharedPointer<RocketChatUser>> mAllUsers;
        QString current;
    public:
        QMutex mMutex;

    signals:
        void currentChannelChanged( const QString &newText );
        void countChanged();
};

#endif // USERMODEL_H
