/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#ifndef LOADHISTORYSERVICEREQUEST_H
#define LOADHISTORYSERVICEREQUEST_H

#include <QStringList>
#include <QSharedPointer>
#include <functional>
#include "repos/entities/rocketchatmessage.h"
enum class Source;
class LoadHistoryServiceRequest
{
    public:
        explicit LoadHistoryServiceRequest( const QString &pChannelId );
        explicit LoadHistoryServiceRequest( QStringList pChannelIds );
        LoadHistoryServiceRequest( QString pChannelId, int pLimit );
        LoadHistoryServiceRequest( QStringList pChannelIds, int pLimit );

        enum class Source {
            AUTO,
            DataBase,
            SERVER
        };
        Source getSource() const;
        void setSource( const Source &pValue );

        double getStart() const;
        void setStart( double pValue );


        QStringList getChannelIds() const;

        int getLimit() const;
        void setLimit( int pValue );

        std::function<void ( QMultiMap<QString, QSharedPointer<RocketChatMessage> > *pMessages )> getSuccess() const;
        void setSuccess( const std::function<void ( QMultiMap<QString, QSharedPointer<RocketChatMessage> > *pMessages )> &pValue );

        double getEnd() const;
        void setEnd( double pValue );

        bool getNoLastThreeDays() const;
        void setNoLastThreeDays( bool pValue );

    protected:
        QStringList mChannelIds;
        Source mSource = Source::AUTO;
        double mStart = -1;
        double mEnd = -1;
        int mLimit = 50;
        std::function<void ( QMultiMap<QString, QSharedPointer<RocketChatMessage> > *messages )> mSuccess = nullptr;
        bool mNoLastThreeDays = false;
};

#endif // LOADHISTORYSERVICEREQUEST_H
