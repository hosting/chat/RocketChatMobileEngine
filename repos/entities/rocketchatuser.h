/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#ifndef ROCKETCHATUSER_H
#define ROCKETCHATUSER_H

#include <QObject>
#include <QSharedPointer>
#include "tempfile.h"

class RocketChatUser : public QObject
{
        Q_OBJECT

    public:

        enum class status : int {
            ONLINE = 0,
            AWAY = 1,
            OFFLINE = 2,
            BUSY = 3
        };
        explicit RocketChatUser( QString pId )                                            ;

        const QString &getUserName() const;
        void setUserName( const QString &pValue );

        const QString &getSurname() const;
        void setSurname( const QString &pValue );

        const QString &getLastname() const;
        void setLastname( const QString &pValue );

        const QString &getUserId() const;
        void setUserId( const QString &pValue );

        status getStatus() const;
        void setStatus( status );
        void setStatus( const QString &pStatus );
        

        bool operator==( const RocketChatUser &lhs ) const;

        const QString &getName() const;
        void setName( const QString &name );

    private:
        QString mUserName;
        QString mSurname;
        QString mLastname;
        QString mUserId;
        QString mName;
        status mStatus = status::OFFLINE;
        QSharedPointer<TempFile> mAvatarImg;
    signals:
        void statusChanged();
        void avatarChanged();
    public slots:
};

#endif // ROCKETCHATUSER_H
