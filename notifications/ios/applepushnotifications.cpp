/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include "applepushnotifications.h"
#include <QDebug>

ApplePushNotifications::ApplePushNotifications()
{
    mInstance = IosDeviceTokenStorage::getInstance();
    mNotificationInstance = IosNotificationReceiver::getInstance();
    connect( mInstance, &IosDeviceTokenStorage::tokenReceived, this, &ApplePushNotifications::receiveToken );
    connect( mNotificationInstance, &IosNotificationReceiver::messageReceived, this, &ApplePushNotifications::receiveMessage );
    connect( mNotificationInstance, &IosNotificationReceiver::switchChannelByName, this, &ApplePushNotifications::onSwitchRequest );
}

void ApplePushNotifications::receiveMessage(QString pServer, QString pRid,QString pName,QString pType )
{
    emit messageReceived( pServer, pRid, pName, pType );
}

void ApplePushNotifications::receiveToken()
{
    qDebug() << "token received";
    QString newToken = mInstance->getToken();

    if ( newToken != "" ) {
        mToken = newToken;
        emit tokenReceived( newToken );
    }
}

void ApplePushNotifications::registerWithService()
{
    if ( mInstance->getToken() != "" ) {
        receiveToken();
    }
}

void ApplePushNotifications::checkForPendingMessages()
{
    emit switchChannelByName( mRoomName );
}

void ApplePushNotifications::onSwitchRequest( QString pName )
{
    mRoomName = pName;
    emit switchChannelByName( pName );
}
