#ifndef ROCKETCHATGETUSERNAMESUGGESTION_H
#define ROCKETCHATGETUSERNAMESUGGESTION_H

#include "ddpmethodrequest.h"

class RocketChatGetUsernameSuggestion : public DDPMethodRequest
{
    public:
        RocketChatGetUsernameSuggestion( const DdpCallback &pSuccess );
};

#endif // ROCKETCHATGETUSERNAMESUGGESTION_H
