/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#ifndef LOGINMETHOD_H
#define LOGINMETHOD_H

#include <QString>
#include <QJsonObject>
#include <QJsonDocument>
#include <QMap>
#include <QVariant>
#include <QDebug>

class LoginMethod
{
    public:
        LoginMethod();
        enum class Type {
            OPENID,
            SAML2,
            NONE
        };

        LoginMethod::Type getType() const;
        void setType( const Type &pType );

        const QString &getRedirectPath() const;
        void setRedirectPath( const QString &pUrl );

        const QString &getServerUrl() const;
        void setServerUrl( const QString &pServerUrl );

        const QString &getService() const;
        void setService( const QString &pService );

        const QString &getClientId() const;
        void setClientId( const QString &pClientId );

        const QString &getTokenPath() const;
        void setTokenPath( const QString &pTokenPath );

        const QString &getIdentityPath() const;
        void setIdentityPath( const QString &pIdentityPath );

        const QString &getAuthorizePath() const;
        void setAuthorizePath( const QString &pAuthorizationPath );

        const QString &getCredentialToken() const;
        void setCredentialToken( const QString &pCredentialToken );

        const QString &getButtonLabelText() const;
        void setButtonLabelText( const QString &pButtonLabelText );

        QString getState() const;

        QString getAuthorizeUrl() const;

        const QString &getRedirectUrl() const;

        QString getIdpUrl() const;

        QMap<QString, QVariant> toMap() const;

        operator QMap<QString, QVariant>();

    private:
        Type mType;
        QString mRedirectUrl;
        QString mServerUrl;
        QString mService;
        QString mClientId;
        QString mTokenPath;
        QString mIdentityPath;
        QString mAuthorizePath;
        QString mCredentialToken;
        QString mState;
        QString mButtonLabelText;

};

#endif // LOGINMETHOD_H
