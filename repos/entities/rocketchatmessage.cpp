/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#include "rocketchatmessage.h"

RocketChatMessage::RocketChatMessage( const QJsonObject &data, unsigned long timestamp, Type type ): timestamp( timestamp ), data( data ), type( type )
{
    messageString.reserve( 300 );
}

RocketChatMessage::RocketChatMessage( const QJsonObject &data ) : data( data ), type( Type::none )
{
    timestamp = static_cast<qint64>( Utils::getMessageTimestamp( data ) );
    roomId = data[QStringLiteral( "rid" )].toString();
    id = data[QStringLiteral( "_id" )].toString();

    messageString.reserve( 300 );

}

RocketChatMessage::RocketChatMessage( const QString &roomId, const QString &message ): type( Type::none )
{
    this->roomId = roomId;
    this->messageString = message;

    messageString.reserve( 300 );

}

qint64 RocketChatMessage::getTimestamp() const
{
    return timestamp;
}

bool RocketChatMessage::isEmpty() const
{
    return empty;
}

const QJsonObject &RocketChatMessage::getData() const
{
    return data;
}

const QString &RocketChatMessage::getRoomId() const
{
    return roomId;
}

void RocketChatMessage::setRoomId( const QString &value )
{
    roomId = value;
}

const QString &RocketChatMessage::getId() const
{
    return id;
}

const QString &RocketChatMessage::getAuthor() const
{
    return author;
}

void RocketChatMessage::setAuthor( const QString &value )
{
    author = value;
}

const QString &RocketChatMessage::getFormattedDate() const
{
    return formattedDate;
}

void RocketChatMessage::setFormattedDate( const QString &value )
{
    formattedDate = value;
}

const QString &RocketChatMessage::getFormattedTime() const
{
    return formattedTime;
}

void RocketChatMessage::setFormattedTime( const QString &value )
{
    formattedTime = value;
}

const QString &RocketChatMessage::getMessageString() const
{

    return messageString;
}

void RocketChatMessage::setMessageString( const QString &value )
{
    data[QStringLiteral( "msg" )] = value;
    messageString = value;
}

const QString &RocketChatMessage::getMessageType()
{
    if ( messageType.length() == 0 ) {
        this->messageType = QStringLiteral( "textMessage" );

        if ( !attachments.empty() ) {
            QSharedPointer<RocketChatAttachment> first = attachments.first();
            this->messageType = first->getType();
        }

    }

    return messageType;

}

void RocketChatMessage::setMessageType( const QString &value )
{
    messageType = value;
}

bool RocketChatMessage::getOwnMessage() const
{
    return ownMessage;
}

void RocketChatMessage::setOwnMessage( bool value )
{
    ownMessage = value;
}

const QList<QSharedPointer<RocketChatAttachment> > &RocketChatMessage::getAttachments() const
{
    return attachments;
}

void RocketChatMessage::setAttachments( const QList<QSharedPointer<RocketChatAttachment> > &value )
{
    attachments = value;
}

qint64 RocketChatMessage::getEmojiHash() const
{
    return mEmojiHash;
}

void RocketChatMessage::setEmojiHash( const qint64 &pValue )
{
    mEmojiHash = pValue;
}

const QString &RocketChatMessage::getAuthorId() const
{
    return authorId;
}

void RocketChatMessage::setAuthorId( const QString &value )
{
    authorId = value;
}

const QString &RocketChatMessage::getServer() const
{
    return server;
}

void RocketChatMessage::setServer( const QString &value )
{
    server = value;
}

const QString &RocketChatMessage::getRoomeTyp() const
{
    return roomeTyp;
}

void RocketChatMessage::setRoomeTyp( const QString &value )
{
    roomeTyp = value;
}

bool RocketChatMessage::inTimeIntervall( const qint64 newt, const qint64 oldt )
{
    return getTimestamp() >= oldt && getTimestamp() <= newt;
}

QSharedPointer<TempFile> RocketChatMessage::getAvatarImg() const
{
    return mAvatarImg;
}

void RocketChatMessage::setAvatarImg( const QSharedPointer<TempFile> &avatarImg )
{
    if ( mAvatarImg != avatarImg && !avatarImg.isNull() ) {
        mAvatarImg = avatarImg;

        if ( mIntialized ) {
            emit dataChanged( id, "avatarImg" );
        }

        mIntialized = true;
    }
}
