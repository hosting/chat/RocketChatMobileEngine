/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/

#include <QDebug>
#include "androidfiledialog.h"

AndroidFileDialog::ResultReceiver::ResultReceiver( AndroidFileDialog *dialog ) : _dialog( dialog ) {}
AndroidFileDialog::ResultReceiver::~ResultReceiver() {}

void AndroidFileDialog::ResultReceiver::handleActivityResult( int receiverRequestCode, int resultCode, const QAndroidJniObject &data )
{
    jint RESULT_OK = QAndroidJniObject::getStaticField<jint>( "android/app/Activity", "RESULT_OK" );

    if ( receiverRequestCode == EXISTING_FILE_NAME_REQUEST && resultCode == RESULT_OK ) {
        QAndroidJniObject uri = data.callObjectMethod( "getData", "()Landroid/net/Uri;" );
        QString path = uriToPath( uri );
        _dialog->emitExistingFileNameReady( path );
    } else {
        _dialog->emitExistingFileNameReady( QString() );
    }
}

QString AndroidFileDialog::ResultReceiver::uriToPath( const QAndroidJniObject &uri )
{
    QAndroidJniEnvironment env;
    QString path = uri.callObjectMethod( "getPath", "()Ljava/lang/String;" ).toString();
    if ( uri.toString().startsWith( "file:", Qt::CaseInsensitive ) ) {
        return uri.callObjectMethod( "getPath", "()Ljava/lang/String;" ).toString();
    }
    QAndroidJniObject data = QAndroidJniObject::callStaticObjectMethod("com/osalliance/rocketchatMobile/MainActivity",
        "uriToData",
        "(Ljava/lang/String;)[B", QAndroidJniObject::fromString(uri.toString()).object<jstring>());
    qDebug() << "opening uri "<<uri.toString();
    QAndroidJniObject contentResolver = QtAndroid::androidActivity().callObjectMethod( "getContentResolver", "()Landroid/content/ContentResolver;" );
    QAndroidJniObject cursor = contentResolver.callObjectMethod( "query", "(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;", uri.object<jobject>(), 0, 0, 0, 0 );
    QAndroidJniObject DATA = QAndroidJniObject::fromString( "_display_name" );
    jint columnIndex = cursor.callMethod<jint>( "getColumnIndexOrThrow", "(Ljava/lang/String;)I", DATA.object<jstring>() );
    jbyteArray dataArray = data.object<jbyteArray>();
    jsize iconSize = env->GetArrayLength(dataArray);
    jbyte *realdata = env->GetByteArrayElements(dataArray, JNI_FALSE);
    QString cache = QStandardPaths::writableLocation( QStandardPaths::TempLocation );
    QDir cacheDir(cache);
    QUuid id = QUuid::createUuid();


    if ( env->ExceptionCheck() ) {
        qWarning() << "Fileupload: JAVA exception: ";
        env->ExceptionDescribe();
        env->ExceptionClear();
        return QString( "null" );
    }

    cursor.callMethod<jboolean>( "moveToFirst", "()Z" );
    QAndroidJniObject result = cursor.callObjectMethod( "getString", "(I)Ljava/lang/String;", columnIndex );
            QString displayName =  result.isValid() ? result.toString() : QString( "null" );
            QString cacheFile = cacheDir.filePath(displayName);
            qDebug() <<"cachefile: " << cacheFile;
            QFile file(cacheFile);
            if(realdata != nullptr){
                if(file.open(QIODevice::WriteOnly)){
                    file.write((const char*)realdata,iconSize);
                }
                qDebug() <<"wrote file " << cacheFile;
                return cacheFile;
            }
            return QString( "null" );
}

AndroidFileDialog::AndroidFileDialog( QObject *parent ) : QObject( parent )
{
    receiver = new ResultReceiver( this );
}

AndroidFileDialog::~AndroidFileDialog()
{
    delete receiver;
}

bool AndroidFileDialog::provideExistingFileName()
{
    QAndroidJniObject ACTION_GET_CONTENT = QAndroidJniObject::fromString( "android.intent.action.GET_CONTENT" );
    QAndroidJniObject intent( "android/content/Intent" );

    QAndroidJniEnvironment env;
    jobjectArray stringArray = env->NewObjectArray( 3, env->FindClass( "java/lang/String" ), nullptr );
    env->SetObjectArrayElement( stringArray, 0, QAndroidJniObject::fromString( QStringLiteral( "file/*" ) ).object<jstring>() );
    env->SetObjectArrayElement( stringArray, 1, QAndroidJniObject::fromString( QStringLiteral( "image/*" ) ).object<jstring>() );
    env->SetObjectArrayElement( stringArray, 2, QAndroidJniObject::fromString( QStringLiteral( "video/*" ) ).object<jstring>() );
    QAndroidJniObject jniArray = QAndroidJniObject::fromLocalRef( stringArray );
    QAndroidJniObject EXTRA_MIME_TYPES = QAndroidJniObject::getStaticObjectField<jstring>( "android/content/Intent", "EXTRA_MIME_TYPES" );

    if ( ACTION_GET_CONTENT.isValid() && intent.isValid() ) {
        intent.callObjectMethod( "setAction", "(Ljava/lang/String;)Landroid/content/Intent;", ACTION_GET_CONTENT.object<jstring>() );
        intent.callObjectMethod( "setType", "(Ljava/lang/String;)Landroid/content/Intent;", QAndroidJniObject::fromString( "*/*" ).object<jstring>() );
        intent.callObjectMethod( "putExtra", "(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;", EXTRA_MIME_TYPES.object<jstring>(), jniArray.object<jobjectArray>() );
        QtAndroid::startActivity( intent.object<jobject>(), EXISTING_FILE_NAME_REQUEST, receiver );
        return true;
    }

    return false;

}

void AndroidFileDialog::emitExistingFileNameReady( const QString &result )
{
    emit existingFileNameReady( result );
}
