#ifndef ROCKETCHATMESSAGESEARCHREQUEST_H
#define ROCKETCHATMESSAGESEARCHREQUEST_H

#include "ddpmethodrequest.h"

class RocketChatMessageSearchRequest : public DDPMethodRequest
{
    public:
        RocketChatMessageSearchRequest() = default;
        RocketChatMessageSearchRequest( const QString &pTerm, const QString &pRoomId );
        RocketChatMessageSearchRequest( const QString &pTerm, const QString &pRoomId, const DdpCallback &pSuccess );

};

#endif // ROCKETCHATMESSAGESEARCHREQUEST_H
