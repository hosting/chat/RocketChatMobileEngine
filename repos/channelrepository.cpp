/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include "channelrepository.h"
ChannelRepository::ChannelRepository( QObject *parent ): QObject( parent ),
    mChannelsModel( Models::getPublicGroupsModel() ), mGroupsModel( Models::getPublicGroupsModel() ), mDirectModel( Models::getDirectChannelsModel() ), mMessagesModel( Models::getMessagesModel() )
{
    mElements.reserve( 50 );
}

bool ChannelRepository::add( const QString &id, const QSharedPointer<RocketChatChannel> &msg )
{
    QString type = msg->getType();

    if ( !mElements.contains( id ) ) {
        if ( type == "c" ) {
            auto result = QMetaObject::invokeMethod( mChannelsModel,  "addChannelSlot", Q_ARG( QSharedPointer<RocketChatChannel>, msg ) );
            Q_ASSERT( result );
        } else if ( type == "d" ) {
            auto result = QMetaObject::invokeMethod( mDirectModel, "addChannelSlot", Q_ARG( QSharedPointer<RocketChatChannel>, msg ) );
            Q_ASSERT( result );
        } else if ( type == "p" ) {
            auto result = QMetaObject::invokeMethod( mGroupsModel,  "addChannelSlot", Q_ARG( QSharedPointer<RocketChatChannel>, msg ) );
            Q_ASSERT( result );
        }

        mNameIndex[msg->getName()] = msg;
        auto result = QMetaObject::invokeMethod( mMessagesModel, "addChannelSlot", Q_ARG( QSharedPointer<RocketChatChannel>, msg ) );
        Q_ASSERT( result );
        return AbstractBaseRepository::add( id, msg );
    } else {
        return false;
    }
}

bool ChannelRepository::add( const QSharedPointer<RocketChatChannel> &pChannel )
{
    QString id = pChannel->getRoomId();
    mNameIndex[pChannel->getName()] = pChannel;
    return add( id, pChannel );
}

bool ChannelRepository::add( const QString &pType, const QList<QSharedPointer<RocketChatChannel> > &pChannels )
{
    if ( !pChannels.empty() ) {
        for ( const auto &elem : pChannels ) {
            if ( !mElements.contains( elem->getRoomId() ) ) {

                mNameIndex[elem->getName()] = elem;
                auto result = QMetaObject::invokeMethod( mMessagesModel, "addChannelSlot", Q_ARG( QSharedPointer<RocketChatChannel>, elem ) );
                AbstractBaseRepository::add( elem->getRoomId(), elem );
            }
        }
    }

    if ( pType == "p" ) {
        auto result = QMetaObject::invokeMethod( mGroupsModel,  "addChannelsSlot", Q_ARG( QList<QSharedPointer<RocketChatChannel> >, pChannels ) );
    } else if ( pType == "c" ) {
        auto result = QMetaObject::invokeMethod( mChannelsModel,  "addChannelsSlot", Q_ARG( QList<QSharedPointer<RocketChatChannel> >, pChannels ) );
    } else if ( pType == "d" ) {
        auto result = QMetaObject::invokeMethod( mDirectModel, "addChannelsSlot", Q_ARG( QList<QSharedPointer<RocketChatChannel> >, pChannels ) );
    }

    return true;
}

QSharedPointer<RocketChatChannel> ChannelRepository::getChannelByName( const QString &pName )
{
    if ( mNameIndex.contains( pName ) ) {
        return mNameIndex[pName];
    }

    return nullptr;

}

const QSharedPointer<RocketChatChannel> ChannelRepository::getChannelByName( const QString &pName ) const
{
    if ( mNameIndex.contains( pName ) ) {
        return mNameIndex[pName];
    }

    return nullptr;

}
