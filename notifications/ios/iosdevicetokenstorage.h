/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#ifndef IOSDEVICETOKENSTORAGE_H
#define IOSDEVICETOKENSTORAGE_H

#include <QString>
#include <QObject>

class IosDeviceTokenStorage: public QObject
{
        Q_OBJECT
    public:
        static IosDeviceTokenStorage *getInstance();

        QString getToken();
        void setToken( const QString &mToken );

    private:
        IosDeviceTokenStorage() {}
        IosDeviceTokenStorage( const IosDeviceTokenStorage & ) = delete;

        static IosDeviceTokenStorage *mInstance;

        QString mToken = "";

    signals:
        void tokenReceived();
};

#endif // IOSDEVICETOKENSTORAGE_H
