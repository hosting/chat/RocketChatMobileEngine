#ifndef ROCKETCHATBLOCKUSERREQUEST_H
#define ROCKETCHATBLOCKUSERREQUEST_H

#include "ddpmethodrequest.h"

class RocketChatBlockUserRequest: public DDPMethodRequest
{
    public:
        RocketChatBlockUserRequest( const QString &pChannelId, const QString &pUserId );
};

#endif // ROCKETCHATBLOCKUSERREQUEST_H
