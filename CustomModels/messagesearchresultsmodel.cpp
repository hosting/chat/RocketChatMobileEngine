#include "messagesearchresultsmodel.h"

void MessageSearchResultsModel::clear()
{
    beginResetModel();
    mElements.clear();
    endResetModel();
}

int MessageSearchResultsModel::rowCount( const QModelIndex &parent ) const
{
    Q_UNUSED( parent )
    return mElements.length();
}

QVariant MessageSearchResultsModel::data( const QModelIndex &index, int role ) const
{
    QSharedPointer<RocketChatMessage> message;

    if ( !mElements.isEmpty() ) {
        message = mElements.at( index.row() );
    } else {
        return QVariant();
    }

    auto roleType = static_cast<MessageRoles>( role );


    switch ( roleType ) {
        case MessageRoles::text:
            return message->getMessageString();

        case MessageRoles::author:
            return message->getAuthor();

        case MessageRoles::authorId:
            return message->getAuthorId();

        case MessageRoles::linkurl:
            if ( !message->getAttachments().empty() ) {
                return message->getAttachments().first()->getUrl();
            } else {
                qWarning() << "linkurl not found";
            }

            break;

        case MessageRoles::type:
            return message->getMessageType();

        case MessageRoles::date:
            return message->getFormattedDate();

        case MessageRoles::time:
            return message->getFormattedTime();

        case MessageRoles::ownMessage:
            return message->getOwnMessage();

        case MessageRoles::height:
            if ( !message->getAttachments().empty() ) {
                return message->getAttachments().first()->getHeight();
            } else {
                qWarning() << "height not found";
            }

            break;

        case MessageRoles::width:
            if ( !message->getAttachments().empty() ) {
                return message->getAttachments().first()->getWidth();
            } else {
                qWarning() << "width not found";
            }

            break;

        case MessageRoles::formattedDate:
            return message->getFormattedDate();

        case MessageRoles::formattedTime:
            return message->getFormattedTime();

        case MessageRoles::messageType:
            return message->getMessageType();

        case MessageRoles::blocked:
            return false;

        case MessageRoles::id:
            return message->getId();

        case MessageRoles::rid:
            return message->getRoomId();

        case MessageRoles::timestamp:
            return message->getTimestamp();
    }

    return QVariant();

}

QHash<int, QByteArray> MessageSearchResultsModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[static_cast<int>( MessageRoles::text )] = QByteArrayLiteral( "msg" );
    roles[static_cast<int>( MessageRoles::author )] = QByteArrayLiteral( "author" );
    roles[static_cast<int>( MessageRoles::authorId )] = QByteArrayLiteral( "authorId" );
    roles[static_cast<int>( MessageRoles::linkurl )] = QByteArrayLiteral( "linkurl" );
    roles[static_cast<int>( MessageRoles::type )] = QByteArrayLiteral( "type" );
    roles[static_cast<int>( MessageRoles::date )] = QByteArrayLiteral( "date" );
    roles[static_cast<int>( MessageRoles::time )] = QByteArrayLiteral( "time" );
    roles[static_cast<int>( MessageRoles::ownMessage )] = QByteArrayLiteral( "ownMessage" );
    roles[static_cast<int>( MessageRoles::height )] = QByteArrayLiteral( "height" );
    roles[static_cast<int>( MessageRoles::width )] = QByteArrayLiteral( "width" );
    roles[static_cast<int>( MessageRoles::formattedDate )] = QByteArrayLiteral( "formattedDate" );
    roles[static_cast<int>( MessageRoles::formattedTime )] = QByteArrayLiteral( "formattedTime" );
    roles[static_cast<int>( MessageRoles::messageType )] = QByteArrayLiteral( "messageType" );
    roles[static_cast<int>( MessageRoles::blocked )] = QByteArrayLiteral( "blocked" );
    roles[static_cast<int>( MessageRoles::id )] = QByteArrayLiteral( "id" );
    roles[static_cast<int>( MessageRoles::rid )] = QByteArrayLiteral( "rid" );
    roles[static_cast<int>( MessageRoles::timestamp )] = QByteArrayLiteral( "timestamp" );

    return roles;
}

void MessageSearchResultsModel::setSearchResults( const MessageList &pResults )
{
    beginResetModel();
    mElements.clear();
    mElements << pResults;
    endResetModel();
}

int MessageSearchResultsModel::getPositionOfMessage( const QString &pId )
{
    int pos = -1;

    for ( int i = 0; i < mElements.size(); i++ ) {
        auto element = mElements.at( i );

        if ( element->getId() == pId ) {
            pos = i;
            break;
        }
    }

    return pos;
}
