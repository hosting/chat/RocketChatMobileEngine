/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#ifndef FILESERVICE_H
#define FILESERVICE_H
#include <QSet>
#include <QString>
#include <QSharedPointer>
#include <QMimeDatabase>
#include "repos/entities/tempfile.h"
#include "rocketchatserver.h"
#include "persistancelayer.h"
class RocketChatServerData;
class PersistanceLayer;

class FileRequest
{
    public:
        FileRequest( const QString &url, const QString &type, const std::function<void ( QSharedPointer<TempFile>, bool )> &then, bool showInline );
        QString type = "";
        QString url = "";
        std::function<void ( QSharedPointer<TempFile>, bool )> then;
        bool showInline = true;
};

class FileService
{
    public:
        explicit FileService( RocketChatServerData *server );
        bool getFileRessource( const QSharedPointer< FileRequest > &pRequest );
        void getFilesFromDb();
    protected:
        QMutex mDownloadMutex;
        QSharedPointer<TempFile> getFileFromCache( const QSharedPointer< FileRequest > &pRequest );
        void getFileFromServer( const QSharedPointer< FileRequest > &pRequest );
        QString getFileHash( QFile *file );
        QSet<QString> mCurrentDownloads;
        RocketChatServerData *mServer;
        PersistanceLayer *mStorage;
        QMimeDatabase mMimeDb;
        QString determineAvatarLocation( const QUrl &pUrl, const QMimeType &pType );
        QString determineLocation( const QUrl &pUrl, const QString &pType, bool pShowInline = true );
        QString mImagepath;
        QString mVideoPath;
        QString mDocumentPath;
        QString mTempPath;
        QString mMoviePath;
        QString mMusicPath;
};

#endif // FILESERVICE_H
