/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#ifndef CHANNELREPOSITORY_H
#define CHANNELREPOSITORY_H

#include <QObject>
#include "utils.h"
#include "abstractbaserepository.h"
#include "repos/entities/rocketchatchannel.h"
#include "CustomModels/channelmodel.h"
#include "CustomModels/messagemodel.h"
#include "CustomModels/models.h"
class MessagesModel;
class RocketChatChannel;
class ChannelModel;

class ChannelRepository : public QObject, public AbstractBaseRepository<RocketChatChannel>
{
        Q_OBJECT
    public:

        explicit ChannelRepository( QObject *parent = nullptr );
        bool add( const QString &id, const QSharedPointer<RocketChatChannel> & );
        bool add( const QSharedPointer<RocketChatChannel> &pChannel );
        bool add( const QString &pType, const QList<QSharedPointer<RocketChatChannel>> &pChannels );
        QSharedPointer<RocketChatChannel> getChannelByName( const QString &pName );
        const QSharedPointer<RocketChatChannel> getChannelByName( const QString &pName ) const;

        ~ChannelRepository() = default;
    private:
        ChannelModel *mChannelsModel, *mGroupsModel, *mDirectModel;
        QHash<QString, QSharedPointer<RocketChatChannel> > mNameIndex;
        MessagesModel *mMessagesModel;

    signals:
        void orderChanged( QString channelId );
};

#endif // CHANNELREPOSITORY_H
