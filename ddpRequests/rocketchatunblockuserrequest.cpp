#include "rocketchatunblockuserrequest.h"

RocketChatUnblockUserRequest::RocketChatUnblockUserRequest( const QString &pChannelId, const QString &pUserId )
{
    QJsonArray params;
    QJsonObject userData;
    userData[QStringLiteral( "rid" )] = pChannelId;
    userData[QStringLiteral( "blocked" )] = pUserId;
    params.append( userData );
    buildRequest( QStringLiteral( "unblockUser" ), params );
}
