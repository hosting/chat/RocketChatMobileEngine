/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include "loadmissedmessageservicerequest.h"

LoadMissedMessageServiceRequest::LoadMissedMessageServiceRequest( const QString &pChannelId, const MessageList &pMessages )
{
    this->mChannelId = pChannelId;
    this->mMessages = pMessages;
}

std::function<void ( QMultiMap<QString, QSharedPointer<RocketChatMessage> > *pMessages )> LoadMissedMessageServiceRequest::getSuccess() const
{
    return mSuccess;
}

void LoadMissedMessageServiceRequest::setSuccess( const std::function<void ( QMultiMap<QString, QSharedPointer<RocketChatMessage> > *pMessages )> &pValue )
{
    mSuccess = pValue;
}

QString LoadMissedMessageServiceRequest::getChannelId() const
{
    return mChannelId;
}

MessageList LoadMissedMessageServiceRequest::getMessages() const
{
    return mMessages;
}
