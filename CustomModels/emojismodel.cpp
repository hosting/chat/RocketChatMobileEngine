#include "emojismodel.h"
#include <QDebug>

EmojisModel::EmojisModel()
{
    mDuplicateCheck.reserve( 2000 );
}

int EmojisModel::rowCount( const QModelIndex &parent ) const
{
    Q_UNUSED( parent );

    if ( mData.contains( mCurrentCategory ) ) {
        return mData[mCurrentCategory].size();
    }

    return 0;

}

QVariant EmojisModel::data( const QModelIndex &index, int role ) const
{
    int row = index.row();

    auto list = mData[mCurrentCategory];
    auto emoji = list[row];

    auto roleType = static_cast<EmojiRoles>( role );

    switch ( roleType ) {
        case EmojiRoles::emojiText:
            return emoji->getIdentifier();

        case EmojiRoles::image:
            return emoji->getFilePath();

        case EmojiRoles::category:
            return emoji->getCategory();

        case EmojiRoles::order:
            return emoji->getOrder();

        case EmojiRoles::custom:
            if ( emoji->getCategory() == "custom" ) {
                return true;
            } else {
                return false;
            }
    }
}

void EmojisModel::addEmoji( const QSharedPointer<Emoji> &pEmoji )
{
    if ( !mDuplicateCheck.contains( pEmoji->getIdentifier() ) ) {
        auto category = pEmoji->getCategory();
        int order = pEmoji->getOrder();

        if ( mData.contains( category ) ) {
            auto list = mData[category];
            auto firstElem = list.first();
            auto lastElem = list.last();

            if ( order <= firstElem->getOrder() ) {
                list.prepend( pEmoji );
            } else if ( order >= lastElem->getOrder() ) {
                list.append( pEmoji );
            } else {
                int orderDiffLast = lastElem->getOrder() - order;
                int listLength = list.length();

                if ( orderDiffLast > order ) {
                    for ( int i = listLength; i >= 0; i-- ) {
                        if ( list[i]->getOrder() >= order ) {
                            list.insert( i - 1, pEmoji );
                        }
                    }
                } else {
                    for ( int i = 0; i < listLength; i++ ) {
                        if ( list[i]->getOrder() <= order ) {
                            list.insert( i, pEmoji );
                        }
                    }
                }
            }

        } else {
            mData.insert( category, {pEmoji} );
        }

        mDuplicateCheck.insert( pEmoji->getIdentifier() );
    }
}

void EmojisModel::addEmojisByCategory( const QString &pCategory, const QList<QSharedPointer<Emoji> > &pList )
{
    if ( mCurrentCategory == pCategory ) {
        beginResetModel();
    }

    if ( !mData.contains( pCategory ) ) {
        mData.insert( pCategory, QList<QSharedPointer<Emoji>>() );
        mData[pCategory].reserve( 600 );
    }

    for ( auto &element : pList ) {
        if ( !mDuplicateCheck.contains( element->getIdentifier() ) ) {
            mDuplicateCheck.insert( element->getIdentifier() );
            mData[pCategory].append( element );
        }
    }

    if ( mCurrentCategory == pCategory ) {
        endResetModel();
    }

}

void EmojisModel::clear()
{
    mData.clear();
    mDuplicateCheck.clear();
    mCurrentCategory.clear();
}

QHash<int, QByteArray> EmojisModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[static_cast<int>( EmojiRoles::emojiText )] = QByteArrayLiteral( "emojiText" );
    roles[static_cast<int>( EmojiRoles::category )] = QByteArrayLiteral( "category" );
    roles[static_cast<int>( EmojiRoles::image )] = QByteArrayLiteral( "image" );
    roles[static_cast<int>( EmojiRoles::order )] = QByteArrayLiteral( "order" );
    roles[static_cast<int>( EmojiRoles::custom )] = QByteArrayLiteral( "custom" );
    return roles;
}

QString EmojisModel::currentCategory() const
{
    return mCurrentCategory;
}

void EmojisModel::setCurrentCategory( const QString &currentCategory )
{
    beginResetModel();
    mCurrentCategory = currentCategory;
    endResetModel();
}
