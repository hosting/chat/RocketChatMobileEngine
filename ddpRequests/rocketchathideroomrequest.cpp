#include "rocketchathideroomrequest.h"

RocketChatHideRoomRequest::RocketChatHideRoomRequest( const QString &pId )
{
    QJsonArray params;
    params.append( pId );
    buildRequest( QStringLiteral( "hideRoom" ), params );
}
