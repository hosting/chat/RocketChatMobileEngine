/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#include "loginmethodsmodel.h"

LoginMethodsModel::LoginMethodsModel( QObject *parent ): QAbstractListModel( parent )
{
    QMap<QString, QVariant> entry;
    entry.insert( QStringLiteral( "service" ), QStringLiteral( "user+password" ) );
    elements.append( entry );
}

QVariantMap LoginMethodsModel::get( int row )
{
    if ( row <= elements.size() ) {
        return elements[row];
    }

    return QVariantMap();
}

int LoginMethodsModel::rowCount( const QModelIndex &parent ) const
{
    Q_UNUSED( parent )
    return elements.size();
}

QVariant LoginMethodsModel::data( const QModelIndex &index, int role ) const
{
    if ( elements.size() >= index.row() ) {
        auto roleType = static_cast<LoginMethodsRoles>( role );

        switch ( roleType ) {
            case LoginMethodsRoles::service :
                return elements[index.row()][QStringLiteral( "service" )];

            case LoginMethodsRoles::idpUrl :
                return elements[index.row()][QStringLiteral( "idpUrl" )];

            case LoginMethodsRoles::buttonText :
                return elements[index.row()][QStringLiteral( "buttonText" )];

            case LoginMethodsRoles::redirectUrl :
                return elements[index.row()][QStringLiteral( "redirectUrl" )];
        }
    }

    return QVariant();
}

void LoginMethodsModel::clear()
{
    beginResetModel();
    elements.clear();
    duplicateCheck.clear();
    QMap<QString, QVariant> entry;
    entry.insert( QStringLiteral( "service" ), QStringLiteral( "user+password" ) );
    elements.append( entry );
    endResetModel();
}

QHash<int, QByteArray> LoginMethodsModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[static_cast<int>( LoginMethodsRoles::service )] = QByteArrayLiteral( "service" );
    roles[static_cast<int>( LoginMethodsRoles::idpUrl )] = QByteArrayLiteral( "idpUrl" );
    roles[static_cast<int>( LoginMethodsRoles::buttonText )] = QByteArrayLiteral( "buttonText" );
    roles[static_cast<int>( LoginMethodsRoles::redirectUrl )] = QByteArrayLiteral( "redirectUrl" );
    return roles;
}



void LoginMethodsModel::addLoginMethod( const QMap<QString, QVariant> &pEntry )
{
    QString name = pEntry[QStringLiteral( "service" )].toString();

    if ( !duplicateCheck.contains( name ) ) {
        beginResetModel();
        elements.append( pEntry );
        endResetModel();
        duplicateCheck.insert( name );
    }
}
