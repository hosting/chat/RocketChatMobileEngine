#include "rocketchatmessagesearchrequest.h"

RocketChatMessageSearchRequest::RocketChatMessageSearchRequest( const QString &pTerm, const QString &pRoomId )
{
    QJsonArray params;

    params.append( pTerm );

    if ( pRoomId == nullptr || pRoomId.isEmpty() ) {
        params.append( QJsonValue::Null );
    } else {
        params.append( pRoomId );
    }

    params.append( 20 );
    buildRequest( QStringLiteral( "messageSearch" ), params );
}

RocketChatMessageSearchRequest::RocketChatMessageSearchRequest( const QString &pTerm, const QString &pRoomId, const DdpCallback &pSuccess ): RocketChatMessageSearchRequest( pTerm, pRoomId )
{
    mSuccess = pSuccess;
}
