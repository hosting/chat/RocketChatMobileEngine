/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#include "emoji.h"

//TODO data form db already contain :.*: thats confusing an inconsitent
Emoji::Emoji( QString name, QString extension, QString category ): mCategory( std::move( category ) )
{
    mName = std::move( name );
    this->mIdentifier = ':' + mName + ':';
    mExtension = std::move( extension );
}

Emoji::Emoji( QString name, QString category, QString filePath, QString html ): mCategory( std::move( category ) )
{
    mName = std::move( name );
    this->mIdentifier = mName;
    mFilePath = std::move( filePath );
    QFileInfo fileInfo( mFilePath );
    mhtml = std::move( html );
    mExtension = fileInfo.completeSuffix();
}

Emoji::Emoji( QString name, QString category, QString filePath, QString html, QString unicode ): mCategory( std::move( category ) ), mUnicodeChar( std::move( unicode ) )
{
    mName = std::move( name );
    this->mIdentifier = ':' + mName + ':';
    mFilePath = std::move( filePath );
    QFileInfo fileInfo( mFilePath );
    mhtml = std::move( html );
    mExtension = fileInfo.completeSuffix();
}

Emoji::Emoji( QString name, QString category, QString filePath, QString html, QString unicode, int order ): mCategory( std::move( category ) ), mUnicodeChar( std::move( unicode ) ), mOrder( order )
{
    mName = std::move( name );
    this->mIdentifier =  mName ;
    mFilePath = std::move( filePath );
    QFileInfo fileInfo( mFilePath );
    mhtml = std::move( html );
    mExtension = fileInfo.completeSuffix();
}

const QString &Emoji::getIdentifier() const
{
    return mIdentifier;
}

QVariantMap Emoji::toQVariantMap()
{
    QVariantMap entry;
    entry[QStringLiteral( "category" )] = mCategory;
    entry[QStringLiteral( "text" )] = mIdentifier;
    entry[QStringLiteral( "file" )] = mFilePath;
    entry[QStringLiteral( "order" )] = mOrder;
    return entry;
}

Emoji::operator QVariantMap()
{
    return toQVariantMap();
}

const QString &Emoji::getHtml() const
{
    return mhtml;
}

void Emoji::setHtml( const QString &value )
{
    mhtml = value;
}

const QString &Emoji::getExtension() const
{
    return mExtension;
}

const QString &Emoji::getName() const
{
    return mName;
}

void Emoji::setName( const QString &name )
{
    mName = name;
}

const QString &Emoji::getUnicodeChar() const
{
    return mUnicodeChar;
}

void Emoji::setUnicodeChar( const QString &unicodeChar )
{
    mUnicodeChar = unicodeChar;
}

int Emoji::getOrder() const
{
    return mOrder;
}

void Emoji::setOrder( int pOrder )
{
    mOrder = pOrder;
}

const QString &Emoji::getCategory() const
{
    return mCategory;
}

void Emoji::setCategory( const QString &category )
{
    mCategory = category;
}
