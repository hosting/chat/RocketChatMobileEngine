/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include "loginmethodsrepository.h"

LoginMethodsRepository::LoginMethodsRepository( QObject *parent ) : QObject( parent )
{

}

void LoginMethodsRepository::addLoginMethod( const QJsonObject &pObject, const QString &pBaseUrl )
{
    if ( pObject.contains( QStringLiteral( "service" ) ) && pObject.contains( QStringLiteral( "serverURL" ) ) && pObject.contains( QStringLiteral( "clientId" ) ) &&
            pObject.contains( QStringLiteral( "tokenPath" ) ) && pObject.contains( QStringLiteral( "identityPath" ) ) &&
            pObject.contains( QStringLiteral( "authorizePath" ) ) && pObject.contains( QStringLiteral( "scope" ) ) ) {

        LoginMethod loginMethod;
        loginMethod.setServerUrl( pObject[QStringLiteral( "serverURL" )].toString() );

        loginMethod.setClientId( pObject[QStringLiteral( "clientId" )].toString() );
        loginMethod.setService( pObject[QStringLiteral( "service" )].toString() );
        loginMethod.setTokenPath( pObject[QStringLiteral( "tokenPath" )].toString() );
        loginMethod.setIdentityPath( pObject[QStringLiteral( "identityPath" )].toString() );
        loginMethod.setAuthorizePath( pObject[QStringLiteral( "authorizePath" )].toString() );
        loginMethod.setRedirectPath( pBaseUrl + QStringLiteral( "/_oauth/" ) + loginMethod.getService() );
        loginMethod.setButtonLabelText( pObject[QStringLiteral( "buttonLabelText" )].toString() );

        if ( pObject[QStringLiteral( "scope" )] == QStringLiteral( "openid" ) ) {
            loginMethod.setType( LoginMethod::Type::OPENID );
            loginMethod.setCredentialToken( Utils::getRandomString() );

            addLoginMethod( loginMethod );
        }
    }
}

void LoginMethodsRepository::addLoginMethod( const LoginMethod &pMethod )
{
    mData.insert( pMethod.getService(), pMethod );
}

LoginMethod LoginMethodsRepository::get( const QString &pMethodName )
{
    return mData[pMethodName];
}

LoginMethod LoginMethodsRepository::operator[]( const QString &pString )
{
    return get( pString );
}

bool LoginMethodsRepository::contains( const QString &pTerm )
{
    return mData.contains( pTerm );
}

void LoginMethodsRepository::clear()
{
    mData.clear();
}
