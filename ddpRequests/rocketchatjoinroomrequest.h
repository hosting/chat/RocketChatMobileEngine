#ifndef ROCKETCHATJOINROOMREQUEST_H
#define ROCKETCHATJOINROOMREQUEST_H

#include "ddpmethodrequest.h"

class RocketChatJoinRoomRequest: DDPMethodRequest
{
    public:
        explicit RocketChatJoinRoomRequest( const QString &pRoomId );
        RocketChatJoinRoomRequest( const QString &pRoomId, const DdpCallback &pSuccess );

};

#endif // ROCKETCHATJOINROOMREQUEST_H
