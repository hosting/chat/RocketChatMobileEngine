#ifndef ROCKETCHATSTATSREQUEST_H
#define ROCKETCHATSTATSREQUEST_H

#include "ddpmethodrequest.h"


class RocketChatStatsRequest : public DDPMethodRequest
{
public:
    RocketChatStatsRequest();
};

#endif // ROCKETCHATSTATSREQUEST_H
