#include "iosdevicetokenstorage.h"
#include "iosnotificationreceiver.h"

class IosDeviceTokenStorage;

@interface DelegateObject:NSObject
{
    IosDeviceTokenStorage *m_instance;
    IosNotificationReceiver *mNotificationsInstance;
}

- (id) initWithCPPInstance;

- (void) setToken:(NSString *)token;

- (void) sendMessage:(NSString *)pServer : (NSString *)pRid : (NSString *)pName : (NSString *)pType;

- (void) openChannelByName:(NSString *)channelName;

@end

