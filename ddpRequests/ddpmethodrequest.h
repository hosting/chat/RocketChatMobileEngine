#ifndef METHODREQUEST_H
#define METHODREQUEST_H

#include <QJsonArray>
#include <QString>

#include "ddprequest.h"

class DDPMethodRequest : public DDPRequest
{
    public:
        DDPMethodRequest( const QString &pMethod, const QJsonArray &pParams );
        DDPMethodRequest( const QString &pMethod, const QJsonArray &pParams, const DdpCallback &pSuccess );

    protected:
        DDPMethodRequest() = default;
        void buildRequest( const QString &pMethod, const QJsonArray &pParams );
};

#endif // METHODREQUEST_H
