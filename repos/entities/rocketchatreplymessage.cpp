#include "rocketchatreplymessage.h"


RocketChatReplyMessage::RocketChatReplyMessage( const QString &pText, const QString &pAuthor, const QString &pUrl )
    : RocketChatAttachment( pUrl, QStringLiteral( "replyMessage" ), pUrl ), RocketChatMessage( QJsonObject(), 0, RocketChatMessage::Type::replymessage )
{
    mUrl = pUrl;
    messageString = pText;
    author = pAuthor;
}
