/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#ifndef RESTPATHS_H
#define RESTPATHS_H
#include <QString>

class RestPaths
{
    public:
        const static QString apiLogin;
        const static QString apiLogoff;
        const static QString apiMe;
        const static QString apiGetRooms;
        const static QString apiJoinRoom;
        const static QString apiLeaveRoom;
        const static QString apiRoomMessages;
        const static QString apiSendMessage;
        const static QString apiJoinedRooms;
        const static QString apiJoinedGroups;
        const static QString apiServerInfo;
        const static QString apiSpotLight;
};

#endif // RESTPATHS_H
