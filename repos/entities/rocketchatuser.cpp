/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include "rocketchatuser.h"

RocketChatUser::RocketChatUser( QString pId ): mUserId( std::move( pId ) )
{

}

const QString &RocketChatUser::getUserName() const
{
    return mUserName;
}

void RocketChatUser::setUserName( const QString &pValue )
{
    mUserName = pValue;
}

const QString &RocketChatUser::getSurname() const
{
    return mSurname;
}

void RocketChatUser::setSurname( const QString &pValue )
{
    mSurname = pValue;
}

const QString &RocketChatUser::getLastname() const
{
    return mLastname;
}

void RocketChatUser::setLastname( const QString &pValue )
{
    mLastname = pValue;
}

const QString &RocketChatUser::getUserId() const
{
    return mUserId;
}

void RocketChatUser::setUserId( const QString &pValue )
{
    mUserId = pValue;
}

RocketChatUser::status RocketChatUser::getStatus() const
{
    return mStatus;
}

void RocketChatUser::setStatus( status pStatus )
{
    // if ( pStatus != mStatus ) {
    mStatus = pStatus;
    emit statusChanged();
    //  }
}

void RocketChatUser::setStatus( const QString &pStatus )
{
    if ( !pStatus.isEmpty() ) {
        status newStatus;

        if ( pStatus == QStringLiteral( "online" ) ) {
            newStatus = status::ONLINE;
        } else if ( pStatus == QStringLiteral( "away" ) ) {
            newStatus = status::AWAY;
        } else if ( pStatus == QStringLiteral( "offline" ) ) {
            newStatus = status::OFFLINE;
        } else if ( pStatus == QStringLiteral( "busy" ) ) {
            newStatus = status::BUSY;
        } else {
            newStatus = status::OFFLINE;
        }

        setStatus( newStatus );
    }
}

bool RocketChatUser::operator==( const RocketChatUser &lhs ) const
{
    return lhs.getUserId() == this->getUserId();
}

const QString &RocketChatUser::getName() const
{
    return mName;
}

void RocketChatUser::setName( const QString &name )
{
    mName = name;
}
