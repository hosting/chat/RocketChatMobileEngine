#include "restspotlightrequest.h"

RestSpotlightRequest::RestSpotlightRequest( const QString &pTerm ): RestSpotlightRequest( pTerm, "", nullptr )
{

}

RestSpotlightRequest::RestSpotlightRequest( const QString &pTerm, const QString &pType ): RestSpotlightRequest( pTerm, pType, nullptr )
{

}

RestSpotlightRequest::RestSpotlightRequest( const QString &pTerm, const QString &pType, const RestRequestCallback &pSuccess ): GetRequest( RestPaths::apiSpotLight, pSuccess, nullptr )
{
    if ( pType == QStringLiteral( "d" ) ) {
        setPath( getPath() + QStringLiteral( "?query=@" ) + pTerm );
    } else if ( pType != QString() ) {
        setPath( getPath() + QStringLiteral( "?query=#" ) + pTerm );
    } else {
        setPath( getPath() + QStringLiteral( "?query=" ) + pTerm );
    }
}

RestSpotlightRequest::RestSpotlightRequest( const QString &pTerm, const RestRequestCallback &pSuccess ): GetRequest( RestPaths::apiSpotLight, pSuccess, nullptr )
{
    setPath( getPath() + QStringLiteral( "?query=" ) + pTerm );
}
