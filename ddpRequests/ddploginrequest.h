/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#ifndef DDPLOGINREQUEST_H
#define DDPLOGINREQUEST_H

#include <functional>

#include "ddpmethodrequest.h"

class DDPLoginRequest : public DDPMethodRequest
{
    public:
        DDPLoginRequest( const QString &pUsername, const QString &pPswHash, const DdpCallback & );
        DDPLoginRequest( const QString &pUsername, const QString &pPswHash );
        DDPLoginRequest( const QString &pToken, const DdpCallback & );
        explicit DDPLoginRequest( const QString &pToken );
};

#endif // DDPLOGINREQUEST_H
