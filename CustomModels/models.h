#ifndef MODELS_H
#define MODELS_H

#include "channelmodel.h"
#include "loginmethodsmodel.h"
#include "messagemodel.h"
#include "usermodel.h"
#include "messagesearchresultsmodel.h"
#include "roomsearchresultsmodel.h"
#include "emojismodel.h"

class ChannelModel;
class MessagesModel;
class MessageSearchResultsModel;
class RoomSearchResultsModel;

class Models
{
    public:
        Models() = delete;
        Models &operator=( const Models & ) = delete;

        static UserModel *getUsersModel();
        static LoginMethodsModel *getLoginMethodsModel();
        static ChannelModel *getDirectChannelsModel();
        static ChannelModel *getPublicGroupsModel();
        static ChannelModel *getPrivateGroupsModel();
        static MessagesModel *getMessagesModel();
        static MessageSearchResultsModel *getMessagesSearchModel();
        static RoomSearchResultsModel *getRoomSearchModel();
        static EmojisModel *getEmojisModel();
        static void resetModels();
        static void init();

        ~Models();


    private:
        static UserModel *mUserModel;
        static LoginMethodsModel *mLoginMethodsModel;
        static ChannelModel *mDirectChannelsModel;
        static ChannelModel *mPublicGroupsModel;
        static ChannelModel *mPrivateGroupsModel;
        static MessagesModel *mMessagesModel;
        static MessageSearchResultsModel *mMessagesSearchModel;
        static RoomSearchResultsModel *mRoomsSearchModel;
        static EmojisModel *mEmojisModel;
};

#endif // MODELS_H
