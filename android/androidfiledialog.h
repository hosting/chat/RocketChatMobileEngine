/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#ifndef ANDROIDFILEDIALOG_H
#define ANDROIDFILEDIALOG_H
#include <QObject>
#include <QAndroidJniObject>
#include <QtAndroid>
#include <QAndroidActivityResultReceiver>
#include <QAndroidJniEnvironment>
#include <QStandardPaths>
#include <QDir>
#include <QUuid>

class AndroidFileDialog : public QObject
{
    Q_OBJECT public:
        explicit AndroidFileDialog( QObject *parent = nullptr );
        virtual ~AndroidFileDialog();
        bool provideExistingFileName();
    private:
        class ResultReceiver : public QAndroidActivityResultReceiver
        {
                AndroidFileDialog *_dialog;
            public:
                explicit ResultReceiver( AndroidFileDialog *dialog );
                virtual ~ResultReceiver();
                void handleActivityResult( int receiverRequestCode, int resultCode, const QAndroidJniObject &data );
                QString uriToPath( const QAndroidJniObject &uri );
        };
        static const int EXISTING_FILE_NAME_REQUEST = 1;
        ResultReceiver *receiver;
        void emitExistingFileNameReady( const QString &result );
    signals:
        void existingFileNameReady( QString result );
};

#endif // ANDROIDFILEDIALOG_H
