#ifndef ROCKETCHATSUBSCRIBEUSERDATA_H
#define ROCKETCHATSUBSCRIBEUSERDATA_H

#include "ddpsubscriptionrequest.h"

class RocketChatSubscribeUserData: public DDPSubscriptionRequest
{
    public:
        RocketChatSubscribeUserData();
};

#endif // ROCKETCHATSUBSCRIBEUSERDATA_H
