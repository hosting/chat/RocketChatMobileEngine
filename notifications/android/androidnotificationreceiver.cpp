/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include "androidnotificationreceiver.h"
#include <QAndroidJniEnvironment>
#include <QAndroidJniObject>
#include <QDebug>

AndroidNotificationReceiver *AndroidNotificationReceiver::mAndroidReceiver = nullptr;

AndroidNotificationReceiver::AndroidNotificationReceiver(): mReceiver( nullptr )
{
    JNINativeMethod messageReceivedMethod[] {
        {"messageReceived", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", reinterpret_cast<void *>( &AndroidNotificationReceiver::messageReceived )}

    };

    const char *classNameReceive = "com.osalliance.rocketchatMobile/ReceiveTextMessage";

    if ( QAndroidJniObject::isClassAvailable( classNameReceive ) ) {
        QAndroidJniObject javaClass( classNameReceive );

        if ( javaClass.isValid() ) {
            QAndroidJniEnvironment env;
            jclass objectClass = env->GetObjectClass( javaClass.object<jobject>() );
            env->RegisterNatives( objectClass,
                                  messageReceivedMethod,
                                  sizeof( messageReceivedMethod ) / sizeof( messageReceivedMethod[0] ) );
            env->DeleteLocalRef( objectClass );

        } else {
            qDebug() << classNameReceive << "is not valid";
        }
    } else {
        qDebug() << classNameReceive << "is not available";
    }
}

AndroidNotificationReceiver::~AndroidNotificationReceiver()
{

}

void AndroidNotificationReceiver::doRegister( GooglePushNotifications *pGooglePushNotifications )
{

    qDebug() << "called register";
    mReceiver = pGooglePushNotifications;

    QAndroidJniObject regIdObject = QAndroidJniObject::callStaticObjectMethod(
                                        "com/osalliance/rocketchatMobile/FcmMessageTokenHandler",
                                        "getToken",
                                        "()Ljava/lang/String;" );

    QString qStr = regIdObject.toString();

    if ( !qStr.isEmpty() ) {
        mAndroidReceiver->mReceiver->setGcmRegistrationId( qStr );
    }
}

AndroidNotificationReceiver *AndroidNotificationReceiver::instance()
{
    if ( !mAndroidReceiver ) {
        mAndroidReceiver = new AndroidNotificationReceiver;
    }

    return mAndroidReceiver;
}

void AndroidNotificationReceiver::registrationIdHandler( JNIEnv *env, jobject thiz, jstring registrationId )
{
    Q_UNUSED( thiz )
    qDebug() << "received gcm token";
    const char *nativeString = env->GetStringUTFChars( registrationId, 0 );
    QString str = QString::fromUtf8( nativeString );
    qDebug() << "regId: " << str;
    env->ReleaseStringUTFChars( registrationId, nativeString );
    mAndroidReceiver->mReceiver->setGcmRegistrationId( str );
}

void AndroidNotificationReceiver::errorHandler( JNIEnv *env, jobject thiz, jstring errorMessage )
{
    Q_UNUSED( thiz )
    const char *nativeString = env->GetStringUTFChars( errorMessage, nullptr );
    qWarning( nativeString );
    env->ReleaseStringUTFChars( errorMessage, nativeString );

}

void AndroidNotificationReceiver::messageReceived( JNIEnv *env, jobject thiz, jstring server, jstring rid, jstring name, jstring type )
{
    Q_UNUSED( thiz );
    const char *ridCpp = env->GetStringUTFChars( rid, nullptr );
    const char *serverCpp = env->GetStringUTFChars( server, nullptr );
    const char *nameCpp = env->GetStringUTFChars( name, nullptr );
    const char *typeCpp = env->GetStringUTFChars( type, nullptr );
    QString ridStr = QString::fromUtf8( ridCpp );
    QString serverStr = QString::fromUtf8( serverCpp );
    QString nameStr = QString::fromUtf8( nameCpp );
    QString typeStr = QString::fromUtf8( typeCpp );

    AndroidNotificationReceiver *androidNotificationReceiver = AndroidNotificationReceiver::instance();

    if ( androidNotificationReceiver->mReceiver != nullptr ) {
        androidNotificationReceiver->mReceiver->pushMessageReceived( serverStr, ridStr, nameStr, typeStr );
    }

    qDebug() << "received push notification in cpp: " << ridCpp << " " << serverCpp;
}

GooglePushNotifications *AndroidNotificationReceiver::receiver() const
{
    return mReceiver;
}

void AndroidNotificationReceiver::setReceiver( GooglePushNotifications *receiver )
{
    mReceiver = receiver;
}
