/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#ifndef ROCKTCHATDDPREQUEST_H
#define ROCKTCHATDDPREQUEST_H

#include <QObject>
#include <functional>
#include <QJsonObject>

class MeteorDDP;
typedef std::function<void ( QJsonObject, MeteorDDP * )> DdpCallback ;
class DDPRequest : public QObject
{
        Q_OBJECT
    public:
        explicit DDPRequest( const QJsonObject &pRequest );
        DDPRequest( const QJsonObject &pRequest, DdpCallback pSuccess );
        DDPRequest( const QJsonObject &pRequest, DdpCallback pSuccess, DdpCallback pError );
        QJsonObject getRawRequest() const;
        QString getFrame() const;
        void setFrame( const QString &value );

        DdpCallback getError() const;
        void setError( const DdpCallback &pValue );

        DdpCallback getSuccess() const;
        void setSuccess( const DdpCallback &pValue );

    protected:
        DDPRequest() = default;
        QJsonObject mRawRequest;
        DdpCallback mSuccess = nullptr;
        DdpCallback mError = nullptr;
        QString mFrame;

    signals:

    public slots:
};

#endif // ROCKTCHATDDPREQUEST_H
