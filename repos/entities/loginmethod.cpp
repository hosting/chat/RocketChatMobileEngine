/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#include "loginmethod.h"

LoginMethod::LoginMethod(): mType( Type::NONE )
{


}

LoginMethod::Type LoginMethod::getType() const
{
    return mType;
}

void LoginMethod::setType( const Type &type )
{
    mType = type;
}

const QString &LoginMethod::getRedirectPath() const
{
    return mRedirectUrl;
}

void LoginMethod::setRedirectPath( const QString &pUrl )
{
    mRedirectUrl = pUrl;
}

const QString &LoginMethod::getServerUrl() const
{
    return mServerUrl;
}

void LoginMethod::setServerUrl( const QString &pServerUrl )
{
    mServerUrl = pServerUrl;
}

const QString &LoginMethod::getService() const
{
    return mService;
}

void LoginMethod::setService( const QString &pService )
{
    mService = pService;
}

const QString &LoginMethod::getClientId() const
{
    return mClientId;
}

void LoginMethod::setClientId( const QString &pClientId )
{
    mClientId = pClientId;
}

const QString &LoginMethod::getTokenPath() const
{
    return mTokenPath;
}

void LoginMethod::setTokenPath( const QString &pTokenPath )
{
    mTokenPath = pTokenPath;
}

const QString &LoginMethod::getIdentityPath() const
{
    return mIdentityPath;
}

void LoginMethod::setIdentityPath( const QString &pIdentityPath )
{
    mIdentityPath = pIdentityPath;
}

const QString &LoginMethod::getAuthorizePath() const
{
    return mAuthorizePath;
}

void LoginMethod::setAuthorizePath( const QString &pAuthorizationPath )
{
    mAuthorizePath = pAuthorizationPath;
}

const QString &LoginMethod::getCredentialToken() const
{
    return mCredentialToken;
}

void LoginMethod::setCredentialToken( const QString &pCredentialToken )
{
    mCredentialToken = pCredentialToken;
}

QString LoginMethod::getState() const
{
    QJsonObject stateObj;
    stateObj[QStringLiteral( "loginStyle" )] = QStringLiteral( "popup" );
    stateObj[QStringLiteral( "credentialToken" )] = mCredentialToken;
    stateObj[QStringLiteral( "isCordova" )] = false;
    stateObj[QStringLiteral( "redirectUrl" )] = getRedirectUrl();
    QJsonDocument doc( stateObj );
    QByteArray json = doc.toJson();
    return json.toBase64();
}

QString LoginMethod::getAuthorizeUrl() const
{
    return mServerUrl + mAuthorizePath;
}

const QString &LoginMethod::getRedirectUrl() const
{
    return mRedirectUrl;
}

QString LoginMethod::getIdpUrl() const
{
    QString redirectUrl;
    redirectUrl += getAuthorizeUrl();
    redirectUrl += QStringLiteral( "?client_id=" );
    redirectUrl += mClientId;
    redirectUrl += QStringLiteral( "&redirect_uri=" );
    redirectUrl += getRedirectUrl();
    redirectUrl += QStringLiteral( "&response_type=code&state=" );
    redirectUrl += getState();
    redirectUrl += QStringLiteral( "&scope=openid" );
    qDebug() << redirectUrl;
    return redirectUrl;
}

QMap<QString, QVariant> LoginMethod::toMap() const
{
    QMap<QString, QVariant> map;
    map.insert( QStringLiteral( "idpUrl" ), getIdpUrl() );
    map.insert( QStringLiteral( "redirectUrl" ), getRedirectUrl() );
    map.insert( QStringLiteral( "service" ), getService() );
    map.insert( QStringLiteral( "buttonText" ), getButtonLabelText() );
    qDebug() << map[QStringLiteral( "idpUrl" )];

    return map;
}

LoginMethod::operator QMap<QString, QVariant>()
{
    return toMap();
}

const QString &LoginMethod::getButtonLabelText() const
{
    return mButtonLabelText;
}

void LoginMethod::setButtonLabelText( const QString &pButtonLabelText )
{
    mButtonLabelText = pButtonLabelText;
}
