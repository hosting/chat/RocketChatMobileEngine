/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#include <QJsonObject>

#include "ddprequest.h"

DDPRequest::DDPRequest( const QJsonObject &pRequest ): mRawRequest( pRequest )
{

}

DDPRequest::DDPRequest( const QJsonObject &pRequest, DdpCallback pSuccess ): mRawRequest( pRequest ), mSuccess( std::move( pSuccess ) )
{

}

DDPRequest::DDPRequest( const QJsonObject &pRequest, DdpCallback pSuccess, DdpCallback error ): mRawRequest( pRequest ), mSuccess( std::move( pSuccess ) ), mError( std::move( error ) )
{

}


QJsonObject DDPRequest::getRawRequest() const
{
    return mRawRequest;
}

QString DDPRequest::getFrame() const
{
    return mFrame;
}

void DDPRequest::setFrame( const QString &pValue )
{
    mFrame = pValue;
}

DdpCallback DDPRequest::getSuccess() const
{
    return mSuccess;
}

void DDPRequest::setSuccess( const DdpCallback &pValue )
{
    mSuccess = pValue;
}

DdpCallback DDPRequest::getError() const
{
    return mError;
}

void DDPRequest::setError( const DdpCallback &pValue )
{
    mError = pValue;
}
