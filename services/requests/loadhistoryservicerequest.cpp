/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include "loadhistoryservicerequest.h"

LoadHistoryServiceRequest::LoadHistoryServiceRequest( const QString &pChannelId ): LoadHistoryServiceRequest( QStringList( pChannelId ) )
{

}

LoadHistoryServiceRequest::LoadHistoryServiceRequest( QStringList pChannelIds ): mChannelIds( std::move( pChannelIds ) )
{

}

LoadHistoryServiceRequest::LoadHistoryServiceRequest( QString pChannelId, int pLimit ): LoadHistoryServiceRequest( std::move( pChannelId ) )
{
    mLimit = pLimit;
}

LoadHistoryServiceRequest::LoadHistoryServiceRequest( QStringList pChannelIds, int pLimit ): LoadHistoryServiceRequest( std::move( pChannelIds ) )
{
    mLimit = pLimit;
}

LoadHistoryServiceRequest::Source LoadHistoryServiceRequest::getSource() const
{
    return mSource;
}

void LoadHistoryServiceRequest::setSource( const Source &pValue )
{
    mSource = pValue;
}

double LoadHistoryServiceRequest::getStart() const
{
    return mStart;
}

void LoadHistoryServiceRequest::setStart( double pValue )
{
    mStart = pValue;
}


QStringList LoadHistoryServiceRequest::getChannelIds() const
{
    return mChannelIds;
}

int LoadHistoryServiceRequest::getLimit() const
{
    return mLimit;
}

void LoadHistoryServiceRequest::setLimit( int pValue )
{
    mLimit = pValue;
}

std::function<void ( QMultiMap<QString, QSharedPointer<RocketChatMessage> > *pMessages )> LoadHistoryServiceRequest::getSuccess() const
{
    return mSuccess;
}

void LoadHistoryServiceRequest::setSuccess( const std::function<void ( QMultiMap<QString, QSharedPointer<RocketChatMessage> > *pMessages )> &pValue )
{
    mSuccess = pValue;
}

double LoadHistoryServiceRequest::getEnd() const
{
    return mEnd;
}

void LoadHistoryServiceRequest::setEnd( double pValue )
{
    mEnd = pValue;
}

bool LoadHistoryServiceRequest::getNoLastThreeDays() const
{
    return mNoLastThreeDays;
}

void LoadHistoryServiceRequest::setNoLastThreeDays( bool pValue )
{
    mNoLastThreeDays = pValue;
}
