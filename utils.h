/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/

#ifndef UTILS_H
#define UTILS_H

#include <QString>
#include <QRegularExpression>
#include <random>
#include "repos/emojirepo.h"
#include "repos/entities/emoji.h"

class EmojiRepo;
class Utils
{
    public:
        Utils( const Utils & ) {}
        static QString removeUtf8Emojis( const QString &pText );
        static QString linkiFy( const QString &pText );
        static QString emojiFy( const QString &pText, EmojiRepo *pEmojiRepo );
        static QString escapeHtml( const QString &pText);
        static double getMessageTimestamp( const QJsonObject &pMessage );
        static bool compareMessageTimestamps( const QJsonObject &pMessage1, const QJsonObject &pMessage2 );
        static bool messageHasTimestamp( const QJsonObject &pMessage );
        static QString getPathPrefix();
        static QString escapeUnicodeEmoji( const QString &pString );
        static QString replaceUnicodeEmojis( const QString &pText, EmojiRepo *pEmojiRepo );
        static qint16 hash( const QStringList &pStrings );
        static QString getRandomString( int pLength = 43 );

};

#endif // UTILS_H
