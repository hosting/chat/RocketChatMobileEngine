/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include "emojirepo.h"

EmojiRepo::EmojiRepo( QObject *parent ): QObject( parent )
{
    mElements.reserve( 3000 );
}

bool EmojiRepo::add( const QSharedPointer<Emoji> &pEmoji )
{
    return add( pEmoji->getIdentifier(), pEmoji );
}

bool EmojiRepo::add( const QString &pId, const QSharedPointer<Emoji> &pEmoji )
{
    if ( !pEmoji.isNull() ) {
        if ( !pEmoji->getOrder() ) {
            mHighestOrder++;
            pEmoji->setOrder( mHighestOrder );
        } else {
            auto emojiOrder = pEmoji->getOrder();

            if ( emojiOrder > mHighestOrder ) {
                mHighestOrder = emojiOrder;
            }
        }

        return AbstractBaseRepository::add( pId, pEmoji );
    }

    return false;

}
