#ifndef ROCKETCHATHIDEROOMREQUEST_H
#define ROCKETCHATHIDEROOMREQUEST_H

#include "ddpmethodrequest.h"

class RocketChatHideRoomRequest: public DDPMethodRequest
{
    public:
        explicit RocketChatHideRoomRequest( const QString &pId );
};

#endif // ROCKETCHATHIDEROOMREQUEST_H
