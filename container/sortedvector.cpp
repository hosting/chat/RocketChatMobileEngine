/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#include "sortedvector.h"


template<typename T>
int SortedVector<T>::insertSort( const QSharedPointer<T> &pointer )
{
    int row = -1;

    if ( this->isEmpty() ) {
        this->append( pointer );
        row = 0;
    } else {
        auto elementSmallerThanNew = std::lower_bound( this->begin(), this->end(), pointer, []( const QSharedPointer<T> &first, const QSharedPointer<T> &second )->bool{
            return ( *first ) > ( *second );
        } );

        row = std::distance( this->begin(), elementSmallerThanNew );

        this->insert( elementSmallerThanNew, pointer );
    }

    return row;
}

template<typename T>
int SortedVector<T>::findInsertPosition( const QSharedPointer<T> &pointer ) const
{
    auto elementSmallerThanNew = std::lower_bound( this->begin(), this->end(), pointer, []( const QSharedPointer<T> &first, const QSharedPointer<T> &second )->bool{
        return ( *first ) < ( *second );
    } );

    return std::distance( this->begin(), elementSmallerThanNew );

}

template<typename T>
int SortedVector<T>::findPosition( const QSharedPointer<T> &pointer ) const
{
    auto pos = find( this->begin(), this->end(), pointer );
    return std::distance( this->begin(), pos );
}

template<typename T>
std::tuple<int, int> SortedVector<T>::reOrder()
{
    std::vector<QSharedPointer<T>> indices( this->size() );

    for ( const auto &elem : *this ) {
        indices.push_back( elem );
    }

    std::sort( this->begin(), this->end(), [ = ]( const QSharedPointer<T> &first, const QSharedPointer<T> &second )->bool{
        return ( *first ) > ( *second );
    } );

    int from = -1;
    int to = -1;

    for ( int i = 0; i < this->size() ; i++ ) {
        if ( indices[i] != ( *this )[i] ) {
            from = i;
            break;
        }
    }

    for ( int i = this->size() - 1; i >= 0; i-- ) {
        if ( indices[i] != ( *this )[i] ) {
            to = i;
            break;
        }
    }

    return std::make_tuple( from, to );
}

