/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#include "rocketchatchannel.h"
#include "segfaulthandler.h"



RocketChatChannel::RocketChatChannel( RocketChatServerData *pServer, MessageService *pMessageService, const QString &pRoomId, const QString &pName, const QString &pType ): mDeleted( false )
{
    Q_UNUSED( pServer )
    Q_UNUSED( pMessageService )
    this->mRoomId = pRoomId;
    this->mName = pName;
    this->mType = pType;
}

RocketChatChannel::RocketChatChannel(): mDeleted( false )
{

}

RocketChatChannel::~RocketChatChannel()
{
    mUsers.clear();
}

bool RocketChatChannel::addMessage( const ChatMessage &pMessage, bool nosignal )
{
    if ( !pMessage.isNull() ) {
        if ( mMessages.add( pMessage->getId(), pMessage ) ) {
            qint64 timestamp = pMessage.data()->getTimestamp();
            qint64 youngest = mMessages.youngest().data()->getTimestamp();

            if ( timestamp >= youngest && !nosignal ) {
                emit( messageAdded( getRoomId(), timestamp ) );
            }

            return true;
        }
    }

    return false;
}

const QHash<QString, QSharedPointer< RocketChatMessage >> &RocketChatChannel::getMessages() const
{
    return mMessages.getElements();
}

const QString &RocketChatChannel::getRoomId() const
{
    return mRoomId;
}

void RocketChatChannel::setRoomId( const QString &pValue )
{
    mRoomId = pValue;
}

const QString &RocketChatChannel::getName() const
{
    return mName;
}

void RocketChatChannel::setName( const QString &value )
{
    if ( mName != value && !value.isEmpty() ) {
        mName = value;
        emit dataChanged( mName, "name" );
    }
}


bool RocketChatChannel::addUser( const QSharedPointer<RocketChatUser> &user )
{
    QString userId = user->getUserId();

    if ( mUsers.empty() || !mUsers.contains( userId ) ) {
        mUsers[userId] = user;
        return true;
    }

    return false;
}

const QMap<QString, QSharedPointer<RocketChatUser> > &RocketChatChannel::getUsers() const
{
    return mUsers;
}

void RocketChatChannel::setUsers( const QMap<QString, QSharedPointer<RocketChatUser> > &value )
{
    mUsers = value;
}

void RocketChatChannel::setUsers( const QList<QSharedPointer<RocketChatUser> > &value )
{
    mUsers.clear();

    for ( const auto &currentUser : value ) {
        mUsers[currentUser->getUserName()] = currentUser;
    }

}

ChatMessage RocketChatChannel::getYoungestMessage()
{
    if ( !mMessages.isEmpty() ) {
        return mMessages.youngest();
    }

    return ChatMessage( nullptr );

}
const ChatMessage &RocketChatChannel::getYoungestMessage() const
{
    return mMessages.youngest();
}

ChatMessage RocketChatChannel::getOldestMessage()
{
    if ( !mMessages.isEmpty() ) {
        return mMessages.oldest();
    }

    return ChatMessage( nullptr );

}

const ChatMessage &RocketChatChannel::getOldestMessage() const
{
    return mMessages.oldest();
}

MessageRepository *RocketChatChannel::getMessageRepo()
{
    return &mMessages;
}

QList<QSharedPointer<RocketChatMessage>> RocketChatChannel::addMessages( const QList<QSharedPointer<RocketChatMessage>> &messagesList )
{
    QList<QSharedPointer<RocketChatMessage>> newMessages;

    for ( auto &currentMessage : messagesList ) {
        if ( !currentMessage.isNull() ) {
            if ( addMessage( currentMessage ) ) {
                newMessages.append( currentMessage );
            }
        }
    }

    if ( newMessages.count() ) {
        emit messageAdded( getRoomId(), 0 );
    }

    return newMessages;
}

bool RocketChatChannel::getJoined() const
{
    return mJoined;
}

void RocketChatChannel::setJoined( bool value )
{
    mJoined = value;
}

unsigned int RocketChatChannel::getUnreadMessages() const
{
    return mUnreadMessages;
}


void RocketChatChannel::setUnreadMessages( unsigned int value )
{
    qDebug() << "set unread to: " << value;

    if ( mUnreadMessages != value ) {
        mUnreadMessages = value;
        emit unreadMessagesChanged( mRoomId, value );
    }
}

const QString &RocketChatChannel::getType() const
{
    return mType;
}

void RocketChatChannel::setType( const QString &pType )
{
    if ( mType != pType  && !pType.isEmpty() ) {
        mType = pType;
        emit dataChanged( mRoomId, "type" );
    }
}


bool RocketChatChannel::getReadOnly() const
{
    return mReadOnly;
}

void RocketChatChannel::setReadOnly( bool value )
{
    if ( mReadOnly != value ) {
        mReadOnly = value;
        emit dataChanged( mRoomId, "readonly" );
    }
}

const QStringList &RocketChatChannel::getMuted() const
{
    return mMuted;
}

void RocketChatChannel::setMuted( const QStringList &value )
{
    if ( mMuted != value ) {
        mMuted = value;
        //emit dataChanged( mRoomId, "readonly" );
    }
}

bool RocketChatChannel::getArchived() const
{
    return mArchived;
}

void RocketChatChannel::setArchived( bool value )
{
    if ( mArchived != value ) {
        mArchived = value;
        emit dataChanged( mRoomId, "archived" );
    }
}

bool RocketChatChannel::getSelfMuted() const
{
    return mSelfMuted;
}

void RocketChatChannel::setSelfMuted( bool value )
{
    mSelfMuted = value;
}

int RocketChatChannel::operator >( const RocketChatChannel &channel ) const
{

    return getUpdatedAt() > channel.getUpdatedAt();
}

int RocketChatChannel::operator <( const RocketChatChannel &channel ) const
{
    return getUpdatedAt() < channel.getUpdatedAt();
}

bool RocketChatChannel::getDeleted() const
{
    return mDeleted;
}

void RocketChatChannel::setDeleted( bool deleted )
{
    mDeleted = deleted;
    emit channelDeleted( this->getRoomId(), deleted );
}

const QString &RocketChatChannel::getOwnerName() const
{
    return mOwnerName;
}

void RocketChatChannel::setOwnerName( const QString &pOwner )
{
    if ( mOwnerName != pOwner && !pOwner.isEmpty() ) {
        mOwnerName = pOwner;
        emit dataChanged( mRoomId, "ownerName" );
    }
}

const QString &RocketChatChannel::getOwnerId() const
{
    return mOwnerId;
}

void RocketChatChannel::setOwnerId( const QString &ownerId )
{
    if ( mOwnerId != ownerId && !ownerId.isEmpty() ) {
        mOwnerId = ownerId;
        emit dataChanged( mRoomId, "ownerId" );
    }
}

const QString &RocketChatChannel::getUsername() const
{
    return mUsername;
}

void RocketChatChannel::setUsername( const QString &username )
{
    if ( mUsername != username && !username.isEmpty() ) {
        mUsername = username;
        emit dataChanged( mRoomId, "username" );
    }
}

const QSharedPointer<RocketChatUser> &RocketChatChannel::getChatPartner() const
{
    return mChatPartner;
}

void RocketChatChannel::setChatPartner( const QSharedPointer<RocketChatUser> &chatPartner )
{
    if ( mChatPartner != chatPartner && !chatPartner.isNull() ) {
        mChatPartner = chatPartner;
        connect( chatPartner.data(), &RocketChatUser::statusChanged, this, [ = ]() {
            emit dataChanged( mRoomId, "userStatus" );
        } );
    }
}

const QString &RocketChatChannel::getChatPartnerId() const
{
    return mChatPartnerId;
}

void RocketChatChannel::setChatPartnerId( const QString &chatPartnerId )
{
    if ( mChatPartnerId != chatPartnerId && !chatPartnerId.isEmpty() ) {
        mChatPartnerId = chatPartnerId;
        //  emit dataChanged( mRoomId, "username" );
    }
}

QSharedPointer<TempFile> RocketChatChannel::getAvatarImg() const
{
    if ( mAvatarImg.isNull() ) {

    }

    return mAvatarImg;
}

void RocketChatChannel::setAvatarImg( const QSharedPointer<TempFile> &pAvatar )
{
    if ( mAvatarImg != pAvatar && !pAvatar.isNull() ) {
        mAvatarImg = pAvatar;
        emit dataChanged( mRoomId, "avatarImg" );
    }
}

qint64 RocketChatChannel::getUpdatedAt() const
{
    return mUpdatedAt;
}

void RocketChatChannel::setUpdatedAt( const qint64 &updatedAt )
{
    //skip signal the first time
    if ( mUpdatedAt != updatedAt && updatedAt != -1 ) {
        if ( mUpdatedAt != -1 ) {
            emit updatedChanged( mRoomId, updatedAt );
        }

        mUpdatedAt = updatedAt;
    }
}

qint64 RocketChatChannel::getCreatedAt() const
{
    return mCreatedAt;
}

void RocketChatChannel::setCreatedAt( const qint64 &createdAt )
{
    mCreatedAt = createdAt;
}

bool RocketChatChannel::getBlocked() const
{
    return mBlocked;
}

void RocketChatChannel::setBlocked( bool blocked )
{
    if ( mBlocked != blocked ) {
        mBlocked = blocked;
        emit dataChanged( mRoomId, "blocked" );
    }
}

QStringList RocketChatChannel::whiteList( const QList<QString> ids, const qint64 newest, const qint64 oldest )
{
    auto idSet = ids.toSet();
    auto currentList = mMessages.getElements().keys().toSet();
    auto blacklist = currentList - idSet;
    QList<QString> buffer = {};
    std::copy_if( blacklist.begin(), blacklist.end(), std::back_inserter( buffer ),
    [ = ]( const QString id ) {
        bool keep = true;

        if ( mMessages.contains( id ) && mMessages.get( id )->getRoomId() == getRoomId() ) {
            auto message = mMessages.get( id );
            keep = message->inTimeIntervall( newest, oldest );
        }

        return keep;
    }
                );
    blacklist = buffer.toSet();

    for ( QString id : blacklist ) {
        mMessages.remove( id );
    }

    return blacklist.toList();
}

void RocketChatChannel::deleteMessage( const QString id )
{
    emit messageDeleted( getRoomId(), id );

    if ( mMessages.contains( id ) ) {
        mMessages.remove( id );

    }

}
