/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include "restpaths.h"
const QString RestPaths::apiLogin {QStringLiteral( "/login" )};
const QString RestPaths::apiLogoff { QStringLiteral( "/logout" )};
const QString RestPaths::apiMe { QStringLiteral( "/me" )};
const QString RestPaths::apiGetRooms {QStringLiteral( "/channels.list" )};
const QString RestPaths::apiJoinRoom {QStringLiteral( "/channels.open" )};
const QString RestPaths::apiLeaveRoom { QStringLiteral( "/channels.leave" )};
const QString RestPaths::apiRoomMessages {QStringLiteral( "/channels.history" )};
const QString RestPaths::apiSendMessage { QStringLiteral( "/chat.postMessage" )};
const QString RestPaths::apiJoinedRooms {QStringLiteral( "/channels.list.joined" )};
const QString RestPaths::apiJoinedGroups { QStringLiteral( "/groups.list" )};
const QString RestPaths::apiServerInfo { QStringLiteral( "/info" )};
const QString RestPaths::apiSpotLight {QStringLiteral("/spotlight")};
