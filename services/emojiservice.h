/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#ifndef EMOJISERVICE_H
#define EMOJISERVICE_H
#include <QSharedPointer>
#include <QJsonArray>
#include <QJsonObject>
#include <QAtomicInt>
#include <QJsonObject>
#include <QSharedPointer>
#include "rocketchatserver.h"
#include "repos/entities/emoji.h"
#include "services/fileservice.h"

class RocketChatServerData;
class FileService;
class EmojiData
{
    public:
        explicit EmojiData( const QJsonArray &data );
        const QJsonArray &getEmojiData() const;
        QAtomicInt emojiCounter = 0;
        QAtomicInt emojisProcessed = 0;
        QJsonArray emojiData = {};
        std::function<void ( QList<QSharedPointer<Emoji>> )> success;
        //QList<QSharedPointer<Emoji> *parsedEmojis;
};

class EmojiService
{
    public:
        EmojiService( RocketChatServerData *server, FileService *pFileService );
        void loadCustomEmojis( std::function<void ( QList<QSharedPointer<Emoji>> )> success );
        QList<QSharedPointer<Emoji>> loadEmojisFromDb( void );
        void persistEmoji( const QSharedPointer<Emoji> &pEmoji );
    private:
        RocketChatServerData *server;
        void handleCustomEmojisReceived( EmojiData *data );
        QSharedPointer<Emoji> parseEmoji( const QHash<QString, QString> &pEmojiData );
        QSharedPointer<Emoji> parseEmoji( const QJsonObject &pEmojiData );
        PersistanceLayer *storage;
        FileService *mFileService;
        QList<QSharedPointer<Emoji>> mParsedCustomEmojisTemp;

    signals:
        void emojisReceived( const QList<QSharedPointer<Emoji>> &pEmojiList );
};

#endif // EMOJISERVICE_H
