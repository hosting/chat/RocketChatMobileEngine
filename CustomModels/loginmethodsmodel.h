/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#ifndef LOGINMETHODSMODEL_H
#define LOGINMETHODSMODEL_H

#include <QAbstractListModel>
#include <QMap>
#include <QSet>

class LoginMethodsModel : public QAbstractListModel
{
        Q_OBJECT
    public:
        enum class LoginMethodsRoles : int {
            service = Qt::UserRole + 1,
            idpUrl,
            buttonText,
            redirectUrl
        };
        explicit LoginMethodsModel( QObject *parent = nullptr );

    public slots:
        Q_INVOKABLE QVariantMap get( int row );
        Q_INVOKABLE void clear();

        int rowCount( const QModelIndex &parent ) const;
        QVariant data( const QModelIndex &index, int role ) const;
        void addLoginMethod( const QMap<QString, QVariant> &pEntry );

    private:
        QList<QMap<QString, QVariant> > elements;
        QSet<QString> duplicateCheck;

        // QAbstractItemModel interface
    public:
        QHash<int, QByteArray> roleNames() const;

        // QAbstractItemModel interface
};

#endif // LOGINMETHODSMODEL_H
