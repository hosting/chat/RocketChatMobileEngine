#ifndef RESTSPOTLIGHTREQUEST_H
#define RESTSPOTLIGHTREQUEST_H

#include "getrequest.h"
#include "restpaths.h"

class RestSpotlightRequest : public GetRequest
{
    public:
        explicit RestSpotlightRequest( const QString &pTerm );
        RestSpotlightRequest( const QString &pTerm, const QString &pType );
        RestSpotlightRequest( const QString &pTerm, const QString &pType, const RestRequestCallback &pSuccess );

        RestSpotlightRequest( const QString &pTerm, const RestRequestCallback &pSuccess );
};

#endif // RESTSPOTLIGHTREQUEST_H
