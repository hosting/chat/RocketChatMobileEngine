#ifndef ANDROIDCOOKIES_H
#define ANDROIDCOOKIES_H

#include <QAndroidJniEnvironment>
#include <QAndroidJniObject>

class AndroidCookies
{
    public:
        AndroidCookies();
        static void clearAllCookies();
};

#endif // ANDROIDCOOKIES_H
