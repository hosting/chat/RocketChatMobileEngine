/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#ifndef ABSTRACTBASEREPOSITORY_H
#define ABSTRACTBASEREPOSITORY_H

#include <QObject>
#include <QMap>
#include <QSharedPointer>
#include <iterator>
using namespace std;
template<typename T> class AbstractBaseRepository
{
    public:
        bool add( const QString &pId, const QSharedPointer<T> & );
        void remove( const QString &pId );
        void set( const QString &pId, const QSharedPointer<T> & );
        bool contains( const QString &pId ) const;
        QSharedPointer<T> get( const QString &pId );
        const QSharedPointer<T> &get( const QString &pId ) const;
        QMap<QString, T> list();
        const QMap<QString, T> &list() const;
        const QHash<QString, QSharedPointer<T> > &getElements() const;
        int size();
        /*  T& operator [](QString pArgument){
              return *mElements[pArgument];
          }
          const T operator[](QString pArgument) const{
              return mElements[pArgument];
          }*/
        bool isEmpty() const;
        void clear( void );
    protected:
        QHash<QString, QSharedPointer<T> > mElements;
};
#include "abstractbaserepository.cpp"
#endif // ABSTRACTBASEREPOSITORY_H


