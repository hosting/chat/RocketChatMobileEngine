#ifndef ROCKETCHATGETROOMBYNAMEANDTYPE_H
#define ROCKETCHATGETROOMBYNAMEANDTYPE_H

#include "ddpmethodrequest.h"

class RocketChatGetRoomByNameAndType: public DDPMethodRequest
{
public:
    RocketChatGetRoomByNameAndType(const QString &pName, const QString &pType);
    RocketChatGetRoomByNameAndType(const QString &pName, const QString &pType, const DdpCallback &pSuccess );

};

#endif // ROCKETCHATGETROOMBYNAMEANDTYPE_H
