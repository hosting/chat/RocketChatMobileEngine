#ifndef ROCKETCHATLEAVEROOM_H
#define ROCKETCHATLEAVEROOM_H

#include "ddpmethodrequest.h"

class RocketChatLeaveRoomRequest: public DDPMethodRequest
{
    public:
        explicit RocketChatLeaveRoomRequest( const QString &pChanneId );
};

#endif // ROCKETCHATLEAVEROOM_H
