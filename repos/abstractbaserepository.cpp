/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#include <exception>

#include "abstractbaserepository.h"

template <typename T>
bool AbstractBaseRepository<T>::add( const QString &pId, const QSharedPointer<T> &pElement )
{
    if ( !mElements.contains( pId ) ) {
        mElements[pId] = pElement;
        return true;
    }

    return false;
}
template <typename T>
void AbstractBaseRepository<T>::remove( const QString &pId )
{
    mElements.remove( pId );
}

template <typename T>
void AbstractBaseRepository<T>::set( const QString &pId, const QSharedPointer<T> &pElement )
{
    mElements[pId] = pElement;
}
template <typename T>
bool AbstractBaseRepository<T>::contains( const QString &pId ) const
{
    return mElements.contains( pId );
}
template <typename T>
QSharedPointer<T> AbstractBaseRepository<T>::get( const QString &pId )
{
    if ( mElements.contains( pId ) ) {
        return mElements[pId];
    }

    throw new std::logic_error( "Cannot find element with id:" + pId.toStdString() );
}

template <typename T>
const QHash<QString, QSharedPointer<T> > &AbstractBaseRepository<T>::getElements() const
{
    return mElements;
}

template<typename T>
int AbstractBaseRepository<T>::size()
{
    return mElements.size();
}

template <typename T>
bool AbstractBaseRepository<T>::isEmpty() const
{
    return mElements.empty();
}
template <typename T>
void AbstractBaseRepository<T>::clear( void )
{
    mElements.clear();
}
