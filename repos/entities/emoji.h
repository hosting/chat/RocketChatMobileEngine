/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/


#ifndef EMOJIS_H
#define EMOJIS_H
#include <QString>
#include <QFileInfo>
#include <QVariantMap>
#include "tempfile.h"

class Emoji : public TempFile
{
    public:
        Emoji( QString name, QString extension, QString category );
        Emoji( QString name, QString category, QString file, QString html );
        Emoji( QString name, QString category, QString file, QString html, QString unicode );
        Emoji( QString name, QString category, QString file, QString html, QString unicode, int order );

        const QString &getIdentifier() const;
        QVariantMap toQVariantMap();

        operator QVariantMap();

        const QString &getHtml() const;
        void setHtml( const QString &value );

        const QString &getExtension() const;

        const QString &getName() const;
        void setName( const QString &name );

        const QString &getUnicodeChar() const;
        void setUnicodeChar( const QString &pUnicodeChar );

        int getOrder() const;
        void setOrder( int pOrder );

        const QString &getCategory() const;
        void setCategory( const QString &category );

    protected:
        QString mIdentifier;
        QString mName;
        QString mCategory;
        QString mExtension;
        QString mhtml;
        QString mUnicodeChar;
        int mOrder = 0;
};

#endif // EMOJIS_H
