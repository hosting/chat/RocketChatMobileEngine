/********************************************************************************************
 *                                                                                          *
 * Copyright (C) 2017 Armin Felder, Dennis Beier                                            *
 * This file is part of RocketChatMobileEngine <https://git.fairkom.net/chat/fairchat>.     *
 *                                                                                          *
 * RocketChatMobileEngine is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by                     *
 * the Free Software Foundation, either version 3 of the License, or                        *
 * (at your option) any later version.                                                      *
 *                                                                                          *
 * RocketChatMobileEngine is distributed in the hope that it will be useful,                *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                           *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                            *
 * GNU General Public License for more details.                                             *
 *                                                                                          *
 * You should have received a copy of the GNU General Public License                        *
 * along with RocketChatMobileEngine. If not, see <http://www.gnu.org/licenses/>.           *
 *                                                                                          *
 ********************************************************************************************/



#ifndef LOADMISSEDMESSAGESERVICEREQUEST_H
#define LOADMISSEDMESSAGESERVICEREQUEST_H
#include <QString>
#include <QSharedPointer>
#include <functional>

#include "repos/entities/rocketchatmessage.h"
typedef QList<QSharedPointer<RocketChatMessage>> MessageList;
typedef QSharedPointer<RocketChatMessage> ChatMessage;

class LoadMissedMessageServiceRequest
{
    public:
        LoadMissedMessageServiceRequest( const QString &pChannelId, const MessageList &pMessages );

        std::function<void ( QMultiMap<QString, QSharedPointer<RocketChatMessage> > *pMessages )> getSuccess() const;
        void setSuccess( const std::function<void ( QMultiMap<QString, QSharedPointer<RocketChatMessage> > *pMessages )> &pValue );

        QString getChannelId() const;

        MessageList getMessages() const;

    protected:
        std::function<void ( QMultiMap<QString, QSharedPointer<RocketChatMessage> > *pMessages )> mSuccess = nullptr;
        QString mChannelId;
        MessageList mMessages;
};

#endif // LOADMISSEDMESSAGESERVICEREQUEST_H
